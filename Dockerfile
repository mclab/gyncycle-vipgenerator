FROM mclab/jmodelica:2.4

MAINTAINER sinisi@di.uniroma1.it

RUN apt-get update && apt-get -y install\
    build-essential zlib1g-dev\
    ssh gcc gfortran wget cmake\
    libgsl-dev openmpi-bin libopenmpi-dev\
    libblas-dev liblapack-dev\
    libsqlite3-dev sqlite3

RUN wget https://github.com/google/or-tools/releases/download/v7.5/or-tools_ubuntu-18.04_v7.5.7466.tar.gz

RUN tar -xf or-tools_ubuntu-18.04_v7.5.7466.tar.gz
RUN mv or-tools_Ubuntu-18.04-64bit_v7.5.7466 or-tools

ENV JMODELICA_HOME /opt/jmodelica/
ENV IPOPT_HOME /opt/ipopt
ENV SUNDIALS_HOME /opt/jmodelica/ThirdParty/Sundials
ENV PYTHONPATH :/opt/jmodelica/Python/::$PYTHONPATH
ENV LD_LIBRARY_PATH :/opt/ipopt/lib/:/opt/jmodelica/ThirdParty/Sundials/lib:/opt/jmodelica/ThirdParty/CasADi/lib:$LD_LIBRARY_PATH
ENV SEPARATE_PROCESS_JVM /usr/lib/jvm/java-11-openjdk-amd64

COPY . tool
WORKDIR tool
RUN mkdir build
WORKDIR build
RUN cmake -DFIND_ALPHA_TAU=ON -DCMAKE_LIBRARY_PATH=/root/work/or-tools/lib -DCMAKE_INCLUDE_PATH=/root/work/or-tools/include -DMCLAB_LIB_WARNINGS=OFF -DMCLAB_SUPPRESS_EXT_LIB_WARN=OFF -DMCLAB_WARNINGS=OFF ..
WORKDIR ..
RUN make -C build

WORKDIR data/model
RUN python compilefmu.py GynCycle.Model GynCycle
WORKDIR ../../


