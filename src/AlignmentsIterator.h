/*
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

typedef void AlignmentsIterator_C;

AlignmentsIterator_C *
AlignmentsIterator_new(Patient const *def, unsigned int distance,
                       unsigned int sampling_freq, double tolerance,
                       unsigned int nbSteps, Range_Double *alpha,
                       Range_Double *tau, Range_Double *timeRange);

void AlignmentsIterator_free(AlignmentsIterator_C **a);

void AlignmentsIterator_start(AlignmentsIterator_C *a, Patient const *vp);

bool AlignmentsIterator_hasNext(AlignmentsIterator_C *a);

void AlignmentsIterator_next(AlignmentsIterator_C *a, double *alpha,
                             double *tau);
