/*
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

extern "C" {
#include <MCLabUtils.h>
#include "Patient.h"
#include "TimeEvolution.h"
#include "AlignmentsIterator.h"
}

#include <ortools/constraint_solver/constraint_solveri.h>

#define NULL_VALUE -1

#define DEBUG "AlignmentsIterator"

using namespace operations_research;

class AlignmentsIterator {
public:
  void start(Patient const *vp);
  bool hasNext();
  void next(double &alpha, double &tau);
  AlignmentsIterator(Patient const *def, unsigned int distance,
                     unsigned int sampling_freq, double tolerance,
                     unsigned int nbSteps, Range_Double *alpha,
                     Range_Double *tau, Range_Double *timeRange);
  ~AlignmentsIterator();

private:
  void computeNextSolution();
  void findSpecies();
  void computeExtrema(Patient const *vp);
  int makeVars(Patient const *vp);
  std::vector<Constraint *> makeConstraints();
  void reset();
  int computeAlphaTau(double &alpha, double &tau);
  bool checkAlphaTau(double alpha, double tau, double defValue, double vpValue);

  unsigned int distance;
  unsigned int sampling_freq;
  unsigned int nbSteps;
  double tolerance;
  Range_Double *tau;
  Range_Double *alpha;
  double timeRangeMin;
  double timeRangeMax;

  Patient const *def;
  unsigned int speciesIdx;

  // def maximum and minimum time points
  Array *defArgExtrema;
  Array *defArgMaximaIdxs;
  Array *defArgMinimaIdxs;

  // work array
  Array *vpArgExtrema;
  Array *vpArgMaximaIdxs;
  Array *vpArgMinimaIdxs;

  // discretise maxima and minima
  Range_Double *defRange;
  Range_Double *vpRange;

  Solver *solver;
  std::vector<IntVar *> vars;
  bool isSearchStarted;

  // collect all solutions
  std::unordered_set<std::pair<double, double>> solutions;
  std::pair<double, double> nextAlphaTau;
  bool m_hasNext;
};


AlignmentsIterator::AlignmentsIterator(Patient const *def,
                                       unsigned int distance,
                                       unsigned int sampling_freq,
                                       double tolerance, unsigned int nbSteps,
                                       Range_Double *alpha, Range_Double *tau, Range_Double *timeRange)
    : def(def), distance(distance), sampling_freq(sampling_freq),
      tolerance(tolerance), nbSteps(nbSteps) {
  this->alpha =
      Range_Double_new(Range_Double_min(alpha), Range_Double_max(alpha),
                       Range_Double_step(alpha));
  this->tau = Range_Double_new(Range_Double_min(tau), Range_Double_max(tau),
                               Range_Double_step(tau));
  this->timeRangeMin = Range_Double_min(timeRange);
  this->timeRangeMax = Range_Double_max(timeRange);
  defArgExtrema = NULL;
  defArgMaximaIdxs = NULL;
  defArgMinimaIdxs = NULL;
  vpArgExtrema = NULL;
  vpArgMaximaIdxs = NULL;
  vpArgMinimaIdxs = NULL;
  defRange = NULL;
  vpRange = NULL;
  solver = NULL;
  isSearchStarted = false;
  m_hasNext = false;
  findSpecies();
  if (Debug_isEnabled(DEBUG, 1))
    std::cerr << "[AlignmentsIterator] Species " << speciesIdx
              << " is the candidate for the alignments." << std::endl;
}

AlignmentsIterator::~AlignmentsIterator(void) {
  reset();
  Range_Double_free(&alpha);
  Range_Double_free(&tau);
  Array_free(&defArgExtrema);
  Array_free(&defArgMaximaIdxs);
  Array_free(&defArgMinimaIdxs);
  Range_Double_free(&vpRange);
}

void AlignmentsIterator::findSpecies() {
  int nbSpecies = Patient_numSpecies(def);
  Array *extrema = NULL;
  Array *maxima = NULL;
  Array *minima = NULL;
  auto minLen = INFINITY;
  for (auto i = 1; i <= nbSpecies; i++) {
    extrema = TimeEvolution_argExtrema(Patient_evolution(def, i), &minima,
                                       &maxima, distance, sampling_freq, timeRangeMin, timeRangeMax, false);
    if (Array_length(extrema) < minLen && Array_length(extrema) > 1 &&
        Array_length(minima) > 0 && Array_length(maxima) > 0) {
      // choose the species with less extremum time points but at least 2 (one
      // maximum and one minimum) in order to compute alpha and tau
      speciesIdx = i;
      if (defArgExtrema != NULL) Array_free(&defArgExtrema);
      defArgExtrema = extrema;
      if (defArgMaximaIdxs != NULL) Array_free(&defArgMaximaIdxs);
      defArgMaximaIdxs = maxima;
      if (defArgMinimaIdxs != NULL) Array_free(&defArgMinimaIdxs);
      defArgMinimaIdxs = minima;
      minLen = Array_length(extrema);
      /* Array_fprint(extrema, stderr); */
      if (Array_length(extrema) == 2) break;
    } else {
      Array_free(&extrema);
      Array_free(&maxima);
      Array_free(&minima);
    }
  }
  if (nbSteps > 1) {
    auto stepSize = (timeRangeMax - timeRangeMin) / (nbSteps - 1);
    defRange = Range_Double_new(timeRangeMin, timeRangeMax, stepSize);
  }
}

void AlignmentsIterator::computeExtrema(Patient const *vp) {
  // Compute extrema for selected species only
  vpArgExtrema = TimeEvolution_argExtrema(Patient_evolution(vp, speciesIdx),
                                          &vpArgMinimaIdxs, &vpArgMaximaIdxs,
                                          distance, sampling_freq, NAN, NAN, false);
  /* Array_fprint(vpArgExtrema, stderr); */
  if (nbSteps > 1) {
    auto min = TimeEvolution_min(Patient_evolution(vp, speciesIdx));
    auto max = TimeEvolution_max(Patient_evolution(vp, speciesIdx));
    auto stepSize = (max - min) / (nbSteps - 1);
    vpRange = Range_Double_new(min, max, stepSize);
  }
}

int AlignmentsIterator::makeVars(Patient const *vp) {
  TimeEvolution *def_evol = Patient_evolution(def, speciesIdx);
  TimeEvolution *vp_evol = Patient_evolution(vp, speciesIdx);
  Array *extrema = defArgExtrema;
  /* Array_fprint(defArgMaximaIdxs, stderr); */
  /* Array_fprint(defArgMinimaIdxs, stderr); */
  auto MIdx = 0;
  auto mIdx = 0;
  auto M = 0;
  auto m = 0;
  for (auto j = 0; j < Array_length(extrema); j++) {
    std::vector<int> domainValues;
    Array *domain = NULL;

    double def_extremum_tp = NAN;
    Array_get(extrema, j, &def_extremum_tp);
    double def_extremum = TimeEvolution_value(
        def_evol, TimeEvolution_lastSampleBefore(def_evol, def_extremum_tp));

    if (MIdx < Array_length(defArgMaximaIdxs))
      Array_get(defArgMaximaIdxs, MIdx, &M);
    if (j == M) {
      MIdx++;
      domain = vpArgMaximaIdxs;
    } else {
      if (mIdx < Array_length(defArgMinimaIdxs))
        Array_get(defArgMinimaIdxs, mIdx, &m);
      if (j == m) {
        mIdx++;
        domain = vpArgMinimaIdxs;
      } else {
        std::cerr << "[AlignmentsIterator] Error in making domain."
                  << std::endl;
        abort();
      }
    }
    // 1. Maxima/minima of the reference curve can only be assigned to
    // maxima/minima.
    auto def_step_idx = 0;
    if (defRange != NULL)
      def_step_idx = Range_Double_closest_index(defRange, def_extremum);
    int domainIdx = 0;
    double domainValue = 0;
    for (auto k = 0; k < Array_length(domain); k++) {
      Array_get(domain, k, &domainIdx);
      if (vpRange != NULL) {
        Array_get(vpArgExtrema, domainIdx, &domainValue);
        double value = TimeEvolution_value(
            vp_evol, TimeEvolution_lastSampleBefore(vp_evol, domainValue));
        auto value_idx = Range_Double_closest_index(vpRange, value);
        if (value_idx == def_step_idx) {
          domainValues.push_back(domainIdx);
        }
      } else {
        domainValues.push_back(domainIdx);
      }
    }
    if (domainValues.size() > 0) {
      domainValues.push_back(NULL_VALUE); // NULL
      std::stringstream varName_stream;
      varName_stream << "S" << speciesIdx << "_" << j;
      auto varName = varName_stream.str();
      auto var = solver->MakeIntVar(domainValues, varName);
      if (Debug_isEnabled(DEBUG, 1))
        std::cerr << "[AlignmentsIterator] Make var:'" << var << "'"
                  << std::endl;
      vars.push_back(var);
    } else {
      if (Debug_isEnabled(DEBUG, 1))
        std::cerr << "[AlignmentsIterator] Error: domain size of S" << speciesIdx
                << "_" << j << " is 0. No alignments will be found."
                << std::endl;
      return 1;
    }
  }
  return 0;
}

std::vector<Constraint *> AlignmentsIterator::makeConstraints() {
  std::vector<Constraint *> constraints;
  IntVar *nullConst = solver->MakeIntConst(NULL_VALUE, "NULL");
  for (auto i = 0; i < vars.size(); i++) {
    for (auto j = i + 1; j < vars.size(); j++) {
      // vars[i] != nullConst && vars[j] !=
      // nullConst --> vars[i] <= vars[j]
      constraints.push_back(solver->MakeEquality(
          solver->MakeMax(
              solver->MakeMax(solver->MakeIsEqualVar(vars[i], nullConst),
                              solver->MakeIsEqualVar(vars[j], nullConst)),
              solver->MakeIsLessOrEqualVar(vars[i], vars[j])),
          1));
    }
  }
  return constraints;
}

void AlignmentsIterator::start(Patient const *vp) {
  reset();
  computeExtrema(vp);
  solver = new Solver("AlignmentsIterator");
  auto err = makeVars(vp);
  if (err) {
    return;
  }
  std::vector<Constraint *> constraints = makeConstraints();
  for (Constraint *c : constraints) {
    solver->AddConstraint(c);
    if (Debug_isEnabled(DEBUG, 1))
      std::cerr << "[AlignmentsIterator] Make constraint:'" << c << "'"
                << std::endl;
  }
  DecisionBuilder *const db = solver->MakePhase(
      vars, Solver::CHOOSE_FIRST_UNBOUND, Solver::ASSIGN_MIN_VALUE);
  solver->NewSearch(db);
  isSearchStarted = true;
  computeNextSolution();
}

void AlignmentsIterator::reset() {
  if (vpArgExtrema != NULL) Array_free(&vpArgExtrema);
  if (vpArgMaximaIdxs != NULL) Array_free(&vpArgMaximaIdxs);
  if (vpArgMinimaIdxs != NULL) Array_free(&vpArgMinimaIdxs);

  if (vpRange != NULL) Range_Double_free(&vpRange);

  if (solver != NULL) {
    if (isSearchStarted) solver->EndSearch();
    isSearchStarted = false;
    delete solver;
    solver = NULL;
  }
  vars.clear();

  solutions.clear();
  nextAlphaTau = {NAN, NAN};
  m_hasNext = false;
}

bool AlignmentsIterator::hasNext() { return m_hasNext; }

void AlignmentsIterator::computeNextSolution() {
  if (Debug_isEnabled(DEBUG, 1))
    std::cerr << "[AlignmentsIterator] Compute next solution" << std::endl;
  // compute next solution
  bool accept = false;
  while (!accept && solver->NextSolution()) {
    bool sat = true;
    // compute alpha and tau
    double _alpha = NAN;
    double _tau = NAN;
    int err = computeAlphaTau(_alpha, _tau);
    if (err) continue;
    if (_tau < Range_Double_min(tau) || _tau > Range_Double_max(tau))
      sat = false;
    if (_alpha < Range_Double_min(alpha) || _alpha > Range_Double_max(alpha))
      sat = false;
    // for each mapping check if alpha tau are ok
    for (auto v = 0; v < vars.size() && sat; v++) {
      auto idx = vars[v]->Value();
      double defValue = 0.0;
      Array_get(defArgExtrema, v, &defValue);
      if (idx == -1) {
        continue;
      }
      double vpValue = 0.0;
      Array_get(vpArgExtrema, idx, &vpValue);
      sat = checkAlphaTau(_alpha, _tau, defValue, vpValue);
    }
    if (sat) {
      solutions.insert({_alpha, _tau});
      nextAlphaTau = {_alpha, _tau};
      accept = true;
    }
  }
  if (accept == false)
    m_hasNext = false;
  else
    m_hasNext = true;
}

void AlignmentsIterator::next(double &alpha, double &tau) {
  alpha = this->nextAlphaTau.first;
  tau = this->nextAlphaTau.second;
  computeNextSolution();
}

bool AlignmentsIterator::checkAlphaTau(double alpha, double tau,
                                       double defValue, double vpValue) {
  double diff = Numbers_abs_double(vpValue - (alpha * defValue + tau));
  bool ret = Numbers_approxLE(diff, tolerance);
  /* if (Debug_isEnabled(DEBUG, 1)) { */
  /*   std::cerr << "[AlignmentsIterator] alpha: " << alpha << " * defValue:" <<
   * defValue << " + tau:" << tau << "=" << alpha * defValue + tau << std::endl;
   */
  /*   std::cerr << "[AlignmentsIterator] vpValue: " << vpValue << std::endl; */
  /*   std::cerr << "[AlignmentsIterator] diff: " << fabs(defValue - vpValue) <<
   * std::endl; */
  /* } */
  return ret;
}

int AlignmentsIterator::computeAlphaTau(double &alpha, double &tau) {
  int first = 0;
  int last = vars.size() - 1;
  while (first < vars.size()) {
    if (vars[first]->Value() != NULL_VALUE) break;
    ++first;
  }
  if (first >= vars.size()) return 1;
  while (last > 0) {
    if (vars[last]->Value() != NULL_VALUE) break;
    --last;
  }
  if (last <= 0) return 1;
  if (first == last) return 1;
  int vpIdx_first = vars[first]->Value();
  int vpIdx_last = vars[last]->Value();

  double defValue_first = NAN;
  double defValue_last = NAN;
  double vpValue_first = NAN;
  double vpValue_last = NAN;

  Array_get(defArgExtrema, first, &defValue_first);
  Array_get(defArgExtrema, last, &defValue_last);
  Array_get(vpArgExtrema, vpIdx_first, &vpValue_first);
  Array_get(vpArgExtrema, vpIdx_last, &vpValue_last);

  /*
   * vpValue_first = alpha * defValue_first + tau
   * vpValue_last = alpha * defValue_last + tau
   *    ==>
   *        vpValue_first = alpha * defValue_first + tau
   *        tau = vpValue_last - alpha * defValue_last
   *          ==>
   *              vpValue_first = alpha * defValue_first + vpValue_last - alpha
   * * defValue_last tau = vpValue_last - alpha * defValue_last
   *                ==>
   *                    vpValue_first - vpValue_last = alpha * (defValue_first -
   * defValue_last) tau = vpValue_last - alpha * defValue_last
   *                      ==>
   *                                   (vpValue_first - vpValue_last)
   *                         alpha = ----------------------------------
   *                                  (defValue_first - defValue_last)
   *
   *                         tau = vpValue_last - alpha * defValue_last
   */
  alpha = (vpValue_first - vpValue_last) / (defValue_first - defValue_last);
  tau = vpValue_last - alpha * defValue_last;

  alpha = Range_Double_closest_value(this->alpha, alpha);
  tau = Range_Double_closest_value(this->tau, tau);

  if (solutions.count({alpha, tau}) > 0) return 1;
  return 0;
}

// C proxy
AlignmentsIterator_C *
AlignmentsIterator_new(Patient const *def, unsigned int distance,
                       unsigned int sampling_freq, double tolerance,
                       unsigned int nbSteps, Range_Double *alpha,
                       Range_Double *tau, Range_Double *timeRange) {
  return (AlignmentsIterator_C *) new AlignmentsIterator(
      def, distance, sampling_freq, tolerance, nbSteps, alpha, tau, timeRange);
}

void AlignmentsIterator_free(AlignmentsIterator_C **aP) {
  Debug_assert(DEBUG_ALWAYS, aP != NULL, "aP == NULL\n");
  delete (AlignmentsIterator *) *aP;
}

void AlignmentsIterator_start(AlignmentsIterator_C *a, Patient const *vp) {
  ((AlignmentsIterator *) a)->start(vp);
}

bool AlignmentsIterator_hasNext(AlignmentsIterator_C *a) {
  return ((AlignmentsIterator *) a)->hasNext();
}

void AlignmentsIterator_next(AlignmentsIterator_C *a, double *alpha,
                             double *tau) {
  ((AlignmentsIterator *) a)->next(*alpha, *tau);
}
