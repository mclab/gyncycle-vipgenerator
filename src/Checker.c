/*
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <MCLabUtils.h>
#include <RealVector.h>

#include <config.h>


#include "ParameterSpace.h"
#include "Patient.h"
#include "Injections.h"
#include "Simulator.h"
#include "TimeEvolution_Transformed.h"
#include "SignalMetrics.h"

#include "CheckerOutcome.h"
#include "CheckerResult.h"
#include "CheckerBounds.h"
#include "Checker.h"

#ifdef FIND_ALPHA_TAU
#include "AlignmentsIterator.h"
#endif

#define DEBUG "Checker"
#define DEBUG_VERBOSE "Checker Verbose"

#define KEYMAXLEN 128

#define Checker_isOutOfBounds(value, b)            \
  (Numbers_approxL(value, CheckerBounds_min(b)) || \
   Numbers_approxG(value, CheckerBounds_max(b)))

static double
Checker_normalisedEnergyDifference(SignalMetrics const *def, SignalMetrics const *metrics);

static double Checker_normalisedAverageDifference(SignalMetrics const *def,
                                                  SignalMetrics const *metrics);
static double
Checker_normalisedSampleVarianceDifference(SignalMetrics const *def,
                                           SignalMetrics const *metrics);
static double Checker_normalisedZeroLagCrossCorr(SignalMetrics const *def,
                                                 SignalMetrics const *metrics);
static void Checker_fillDefaultTETCache(Checker *checker);
static void Checker_clearDefaultTETCache(Checker *checker);
static void Checker_clearDefaultMetricsCache(Checker *checker);
static void Checker_fillDefaultMetricsCache(Checker *checker);
static bool Checker_checkBA(Checker *checker, Patient *patient, double alpha, double tau);

bool Checker_checkBounds(Checker *checker, RealVector const *v);
void Checker_initAlphaTau(Checker *checker, Patient *patient);
int Checker_nextAlphaTau(Checker *checker, double *tau_ptr, double *alpha_ptr);
Patient *Checker_simulate(Checker const *checker, RealVector const *v, double t, char const *virtualPatientFileName);
CheckerResult *Checker_run(Checker *checker, RealVector const *v, char const *virtualPatientFileName);
CheckerResult *Checker_run_given_tau(Checker *checker,
                                            RealVector const *v,
                                            char const *virtualPatientFileName,
                                            double tau);
CheckerResult *Checker_run_given_alpha_tau(Checker *checker,
                                            RealVector const *v,
                                            char const *virtualPatientFileName,
                                            double tau, double alpha);
bool Checker_checkBA(Checker *checker, Patient *patient, double alpha, double tau);



struct Checker {
  ParameterSpace const *pSpace;
#ifdef FIND_ALPHA_TAU
  AlignmentsIterator_C *findAlphaTau;
#endif

  double transientEnd;
  Range_Double *alphaRange;
  Range_Double *tauRange;
#ifndef FIND_ALPHA_TAU
  Range_Double_Iterator *alphaIterator;
  Range_Double_Iterator *tauIterator;
#endif
  Range_Double *timeRange;
  Array *normCrossCorrBounds;
  Array *normEnergyDiffBounds;
  Array *speciesBounds;
  double simulation_horizon;
  unsigned int numSpecies;
  HashSet *species;
  RealVector *def;
  Patient *patientDefault;
  char *injections_path;
  Simulator const *simulator;

  // cache
  Array *defaultMetricsCache; // Array of SignalMetrics
  Array *defaultTETCache;     // Array of TimeEvolution_Transformed

  Array *normCrossCorr;
  /* Array *normAvgDiff; */
  Array *normEnergyDiff;
  /* Array *normSampleVarDiff; */
  Array *speciesMin;
  Array *speciesMax;

  double cached_alpha;
};

static void fillDefaultTETCache(Checker *checker);
static void clearDefaultTETCache(Checker *checker);
static void fillDefaultMetricsCache(Checker *checker);
static void clearDefaultMetricsCache(Checker *checker);

unsigned int Checker_numSpecies(Checker const *checker) {
  return checker->numSpecies;
}

Patient const *Checker_patientDefault(Checker const *checker) {
  return checker->patientDefault;
}

Range_Double const *Checker_timeRange(Checker const *checker) {
  return checker->timeRange;
}

Checker *Checker_new(ParameterSpace const *pSpace, Simulator const *simulator, char const *injections_path, Properties const *checkerOptions) {
  Debug_assert(DEBUG_ALWAYS, pSpace != NULL, "pSpace == NULL\n");
  Debug_assert(DEBUG_ALWAYS, simulator != NULL, "simulator == NULL\n");
  Debug_assert(DEBUG_ALWAYS, checkerOptions != NULL, "checkerOptions == NULL\n");
  Checker *new = calloc(1, sizeof(Checker));
  new->pSpace = pSpace;
  new->def = RealVector_newFromArray(ParameterSpace_getDef(pSpace));

  char const *str = NULL;
  int err = Properties_getString(checkerOptions, "alpha", &str);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'alpha' missing in checkerOptions\n");
  new->cached_alpha = 0.0;
  new->alphaRange = Range_Double_new_fromString(str);
#ifndef FIND_ALPHA_TAU
  new->alphaIterator = Range_Double_Iterator_new_flipping(new->alphaRange);
#endif
  str = NULL;
  err = Properties_getString(checkerOptions, "tau", &str);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'tau' missing in checkerOptions\n");
  new->tauRange = Range_Double_new_fromString(str);
#ifndef FIND_ALPHA_TAU
  new->tauIterator =
      Range_Double_Iterator_new(new->tauRange); // tau is always > 0
#endif
  err = Properties_getDouble(checkerOptions, "transientEnd",
                             &new->transientEnd);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'transientEnd' missing in checkerOptions\n");
  Debug_assert(DEBUG_ALWAYS, new->transientEnd >= 0,
               "transientEnd could not be < 0\n");
#ifdef FIND_ALPHA_TAU
  unsigned int distance = 0;
  err = Properties_getUInt(checkerOptions, "extrema/distance", &distance);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'extrema/distance' missing in checkerOptions\n");
  unsigned int sampling_freq = 0;
  err = Properties_getUInt(checkerOptions, "extrema/sampling_freq", &sampling_freq);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'extrema/sampling_freq' missing in checkerOptions\n");
  double tolerance = NAN;
  err = Properties_getDouble(checkerOptions, "extrema/tolerance", &tolerance);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'extrema/tolerance' missing in checkerOptions\n");
  unsigned int nbSteps = 0;
  err = Properties_getUInt(checkerOptions, "extrema/nbSteps", &nbSteps);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'extrema/nbSteps' missing in checkerOptions\n");
#endif
  // Check time ranges
  str = NULL;
  err = Properties_getString(checkerOptions, "timeRange", &str);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "'timeRange' missing in checkerOptions\n");
  new->timeRange = Range_Double_new_fromString(str);

  Debug_assert(DEBUG_ALWAYS, Range_Double_step(new->timeRange) > 0,
               "timeSlotLength <= 0\n");
  Debug_assert(DEBUG_ALWAYS, Range_Double_max(new->timeRange) > 0,
               "timeRange.max <= 0\n");
  Debug_assert(DEBUG_ALWAYS,
               Numbers_approxEQ(Range_Double_min(new->timeRange), 0),
               "timeRange.min != 0\n");

  err = Properties_getUInt(checkerOptions, "numSpecies", &new->numSpecies);
  Debug_assert(DEBUG_ALWAYS, err == 0, "'numSpecies' missing in checkerOptions\n");

  new->species = NULL;
  char const *speciesIdxs_str = NULL;
  err = Properties_getString(checkerOptions, "speciesIdxs", &speciesIdxs_str);
  if (err == 0) {
   new->species = HashSet_new();
   Array *tokens = Array_new(100, sizeof(char *), 1, NULL, NULL);
   char *speciesIdxs_str_clone = StringUtils_clone(speciesIdxs_str);
   StringUtils_tokenize(speciesIdxs_str_clone, ",", tokens);
   for (unsigned int i = 0; i < Array_length(tokens); i++) {
     unsigned int speciesIdx = 0;
     char *speciesIdx_str = NULL;
     Array_get(tokens, i, &speciesIdx_str);
     StringUtils_toUInt(speciesIdx_str, &speciesIdx);
     HashSet_add(new->species, &speciesIdx, sizeof(unsigned int));
   }
   free(speciesIdxs_str_clone);
   Array_free(&tokens);
  }

  new->normCrossCorrBounds =
      Array_new(new->numSpecies, sizeof(CheckerBounds *), 1, NULL, NULL);
  new->normEnergyDiffBounds =
      Array_new(new->numSpecies, sizeof(CheckerBounds *), 1, NULL, NULL);
  new->speciesBounds =
      Array_new(new->numSpecies, sizeof(CheckerBounds *), 1, NULL, NULL);
  for (unsigned int i = 1; i <= new->numSpecies; i++) {
    char boundsStr[KEYMAXLEN] = {0};
    sprintf(boundsStr, "%d/normCrossCorr", i);
    str = NULL;
    err = Properties_getString(checkerOptions, boundsStr, &str);
    Debug_assert(DEBUG_ALWAYS, err == 0, "'%s' is missing in checkerOptions\n",
                 boundsStr);
    CheckerBounds *bounds = CheckerBounds_newFromString(str);
    Debug_assert(DEBUG_ALWAYS,
                 CheckerBounds_min(bounds) >= 0 &&
                     CheckerBounds_max(bounds) <= 1,
                 "normCrossCorr not in [0, 1]\n");
    Array_add(new->normCrossCorrBounds, &bounds);
    sprintf(boundsStr, "%d/normEnergyDiff", i);
    str = NULL;
    err = Properties_getString(checkerOptions, boundsStr, &str);
    Debug_assert(DEBUG_ALWAYS, err == 0, "'%s' is missing in checkerOptions\n",
                 boundsStr);
    bounds = CheckerBounds_newFromString(str);
    Debug_assert(
        DEBUG_ALWAYS,
        CheckerBounds_min(bounds) <= 0 && CheckerBounds_max(bounds) >= -1,
        "normEnergyDiff.min = %g not in [0, -1]\n", CheckerBounds_min(bounds));
    Debug_assert(
        DEBUG_ALWAYS,
        CheckerBounds_max(bounds) >= 0 && CheckerBounds_max(bounds) <= 1,
        "normEnergyDiff.max = %g not in [0, 1]\n", CheckerBounds_max(bounds));
    Array_add(new->normEnergyDiffBounds, &bounds);
    sprintf(boundsStr, "%d/species", i);
    str = NULL;
    err = Properties_getString(checkerOptions, boundsStr, &str);
    Debug_assert(DEBUG_ALWAYS, err == 0, "'%s' is missing in checkerOptions\n",
                 boundsStr);
    bounds = CheckerBounds_newFromString(str);
    Array_add(new->speciesBounds, &bounds);
  }

  new->simulation_horizon =
      Range_Double_max(new->alphaRange) *
      Range_Double_max(new->timeRange) + Range_Double_max(new->tauRange) + new->transientEnd;

  new->patientDefault =
      Patient_newWithTransientPeriod(new->numSpecies, new->transientEnd);

  new->injections_path = injections_path != NULL ? StringUtils_clone(injections_path) : NULL;
  double defaultSimulationTime =
      Range_Double_max(new->timeRange) + new->transientEnd;

  new->simulator = simulator;
  Injections *inj = NULL;
  if (new->injections_path != NULL) {
    inj = Injections_newFromFile(injections_path);
    Injections_shift(inj, new->transientEnd);
  }
  err = Simulator_run(new->simulator, new->def, NULL, new->simulation_horizon,
                      Range_Double_step(new->timeRange), inj, new->patientDefault);
  Debug_assert(DEBUG_ALWAYS, err == 0,
               "Error in simulating default parameter values\n");
  if (inj != NULL) Injections_free(&inj);

#ifdef FIND_ALPHA_TAU
  new->findAlphaTau =
      AlignmentsIterator_new(new->patientDefault, distance, sampling_freq, tolerance, nbSteps,
                             new->alphaRange, new->tauRange, new->timeRange);
#endif

  new->defaultTETCache = Array_new(
      new->numSpecies, sizeof(TimeEvolution_Transformed *), 1, NULL, NULL);
  Checker_fillDefaultTETCache(new);

  new->defaultMetricsCache =
      Array_new(new->numSpecies, sizeof(SignalMetrics *), 1, NULL, NULL);
  Checker_fillDefaultMetricsCache(new);

  double zero = 0.0;
  new->normCrossCorr = Array_newDouble(new->numSpecies, new->numSpecies);
  /* new->normAvgDiff = Array_newDouble(new->numSpecies, new->numSpecies); */
  new->normEnergyDiff = Array_newDouble(new->numSpecies, new->numSpecies);
  /* new->normSampleVarDiff = Array_newDouble(new->numSpecies, new->numSpecies); */
  new->speciesMin = Array_newDouble(new->numSpecies, new->numSpecies);
  new->speciesMax = Array_newDouble(new->numSpecies, new->numSpecies);
  Array_add_n_times(new->normCrossCorr, &zero, new->numSpecies);
  /* Array_add_n_times(new->normAvgDiff, &zero, new->numSpecies); */
  Array_add_n_times(new->normEnergyDiff, &zero, new->numSpecies);
  /* Array_add_n_times(new->normSampleVarDiff, &zero, new->numSpecies); */
  Array_add_n_times(new->speciesMin, &zero, new->numSpecies);
  Array_add_n_times(new->speciesMax, &zero, new->numSpecies);
  return new;
}


void Checker_free(Checker **thisP) {
  Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
  Checker *checker = *thisP;
  if (checker == NULL) return;
  RealVector_free(&checker->def);
  Range_Double_free(&checker->alphaRange);
  Range_Double_free(&checker->tauRange);
  Range_Double_free(&checker->timeRange);
  CheckerBounds *bounds = NULL;
  for (unsigned int i = 0; i < checker->numSpecies; i++) {
    Array_get(checker->normCrossCorrBounds, i, &bounds);
    CheckerBounds_free(&bounds);
    Array_get(checker->normEnergyDiffBounds, i, &bounds);
    CheckerBounds_free(&bounds);
    Array_get(checker->speciesBounds, i, &bounds);
    CheckerBounds_free(&bounds);
  }
  Array_free(&checker->normCrossCorrBounds);
  Array_free(&checker->normEnergyDiffBounds);
  Array_free(&checker->speciesBounds);

  Patient_free(&checker->patientDefault);

  Checker_clearDefaultTETCache(checker);
  Array_free(&checker->defaultTETCache);
  Checker_clearDefaultMetricsCache(checker);
  Array_free(&checker->defaultMetricsCache);

  Array_free(&checker->normCrossCorr);
  /* Array_free(&checker->normAvgDiff); */
  Array_free(&checker->normEnergyDiff);
  /* Array_free(&checker->normSampleVarDiff); */
  Array_free(&checker->speciesMin);
  Array_free(&checker->speciesMax);

  if (checker->injections_path) free(&checker->injections_path);

  if (checker->species != NULL) HashSet_free(&checker->species);
#ifndef FIND_ALPHA_TAU
  Range_Double_Iterator_free(&checker->alphaIterator);
  Range_Double_Iterator_free(&checker->tauIterator);
#else
  AlignmentsIterator_free(&checker->findAlphaTau);
#endif
  free(*thisP);
}

static void initWorkArrays(Checker *checker) {
  double zero = NAN;
  for (unsigned int i = 0; i < checker->numSpecies; i++) {
    Array_set(checker->normCrossCorr, i, &zero);
    /* Array_set(checker->normAvgDiff, i, &zero); */
    Array_set(checker->normEnergyDiff, i, &zero);
    /* Array_set(checker->normSampleVarDiff, i, &zero); */
    Array_set(checker->speciesMax, i, &zero);
    Array_set(checker->speciesMin, i, &zero);
  }
}

Patient *Checker_simulate(Checker const *checker, RealVector const *v, double t, char const *virtualPatientFileName) {
  Patient *patient = Patient_newWithTransientPeriod(checker->numSpecies, checker->transientEnd);
  Injections *inj = NULL;
  if (checker->injections_path != NULL) {
    inj = Injections_newFromFile(checker->injections_path);
    Injections_shift(inj, checker->transientEnd + t);
  }
  int err = Simulator_run(checker->simulator, v, virtualPatientFileName,
                          checker->simulation_horizon,
                          Range_Double_step(checker->timeRange),
                          inj, patient);
  if (inj != NULL) Injections_free(&inj);
  if (err != 0) {
    Patient_free(&patient);
    return NULL;
  }
  return patient;
}

void Checker_initAlphaTau(Checker *checker, Patient *patient) {
#ifdef FIND_ALPHA_TAU
  AlignmentsIterator_start(checker->findAlphaTau, patient);
#else
  patient; // to suppress warning
  Range_Double_Iterator_rewind(checker->alphaIterator);
  Range_Double_Iterator_rewind(checker->tauIterator);
  Range_Double_Iterator_next(checker->alphaIterator, &checker->cached_alpha);
#endif
}

int Checker_nextAlphaTau(Checker *checker, double *tau_ptr, double *alpha_ptr) {
#ifdef FIND_ALPHA_TAU
  if (AlignmentsIterator_hasNext(checker->findAlphaTau)) {
    AlignmentsIterator_next(checker->findAlphaTau, alpha_ptr, tau_ptr);
    Debug_out(DEBUG, "next alpha = %g, tau = %g\n", *alpha_ptr, *tau_ptr);
    return 1;
  }
#else
  if (Range_Double_Iterator_hasNext(checker->tauIterator)) {
    Range_Double_Iterator_next(checker->tauIterator, tau_ptr);
    *alpha_ptr = checker->cached_alpha;
    Debug_out(DEBUG, "next alpha = %g, tau = %g\n", *alpha_ptr, *tau_ptr);
    return 1;
  } else {
    if (Range_Double_Iterator_hasNext(checker->alphaIterator)) {
      Range_Double_Iterator_next(checker->alphaIterator, alpha_ptr);
      checker->cached_alpha = *alpha_ptr;
      Range_Double_Iterator_rewind(checker->tauIterator);
      Range_Double_Iterator_next(checker->tauIterator, tau_ptr);
      Debug_out(DEBUG, "next alpha = %g, tau = %g\n", *alpha_ptr, *tau_ptr);
      return 1;
    }
  }
#endif
  return 0;
}

bool Checker_checkBounds(Checker *checker, RealVector const *v) {
  CheckerOutcome outcome = CHECKER_OUTCOME_NOT_ADM;
  Debug_perform(DEBUG, {
    fprintf(stderr, "[%g", RealVector_get(v, 0));
    for (unsigned int i = 1; i < RealVector_dim(v); i++) {
      fprintf(stderr, ",%g", RealVector_get(v, i));
    }
    fprintf(stderr, "]\n");
  });
  if (!ParameterSpace_belongsTo(checker->pSpace, v)) {
    Debug_out(DEBUG, "Does not belong to space defined by input bounds.\n");
    return false;
  }
  return true;
}

CheckerResult *_Checker_run(Checker * checker, RealVector const *v, char const *virtualPatientFileName, double tau) {
  CheckerOutcome outcome = CHECKER_OUTCOME_NOT_ADM;
  Debug_perform(DEBUG, {
    fprintf(stderr, "[%g", RealVector_get(v, 0));
    for (unsigned int i = 1; i < RealVector_dim(v); i++) {
      fprintf(stderr, ",%g", RealVector_get(v, i));
    }
    fprintf(stderr, "]\n");
  });
  if (!ParameterSpace_belongsTo(checker->pSpace, v)) {
    Debug_out(DEBUG, "Does not belong to space defined by input bounds.\n");
    return CheckerResult_new(NAN, outcome, NULL);
  }
  Debug_out(DEBUG, "Start Simulation\n");
  Patient *patient = Checker_simulate(checker, v, isnan(tau) ? 0 : tau, virtualPatientFileName);
  Debug_out(DEBUG, "End Simulation\n");
  if (patient == NULL) {
    Debug_out(DEBUG, "Error during simulation.\n");
    return CheckerResult_new(NAN, CHECKER_OUTCOME_ERR, NULL);
  }
  Debug_out(DEBUG, "Start Similarity Evaluation\n");
#ifdef FIND_ALPHA_TAU
  AlignmentsIterator_start(checker->findAlphaTau, patient);
#endif
  int done = 0;
  double _alpha = NAN;
  double _tau = NAN;
#ifndef FIND_ALPHA_TAU
  Range_Double_Iterator *alphaIterator = checker->alphaIterator;
  Range_Double_Iterator *tauIterator = checker->tauIterator;
  Range_Double_Iterator_rewind(alphaIterator);
#endif

#ifdef FIND_ALPHA_TAU
  while (!done && AlignmentsIterator_hasNext(checker->findAlphaTau)) {
    AlignmentsIterator_next(checker->findAlphaTau, &_alpha, &_tau);
    Debug_out(DEBUG, "trying alpha = %g, tau = %g\n", _alpha, _tau);
#else
  while (!done && Range_Double_Iterator_next(alphaIterator, &_alpha)) {
    Debug_out(DEBUG_VERBOSE, "trying alpha = %g\n", _alpha);
    Range_Double_Iterator_rewind(tauIterator);
    while (!done && Range_Double_Iterator_next(tauIterator, &_tau)) {
#endif
    if (!(isnan(tau) || Numbers_approxEQ(tau, _tau))) { continue; }

    bool isAdmissible = Checker_checkBA(checker, patient, _alpha, _tau);
    if (isAdmissible) {
      Debug_out(DEBUG, "Admissible with time-scale %g and time-shift %g.\n", _alpha, _tau);
      outcome = CHECKER_OUTCOME_ADM;
      done = 1;
    }
#ifdef FIND_ALPHA_TAU
  } // for each (alpha, tau)
#else
    } // for each alpha
  }   // for each tau
#endif
  Debug_out(DEBUG, "End Similarity Evaluation\n");
  return CheckerResult_new(_tau, outcome, patient);
}

CheckerResult *Checker_run_given_tau(Checker *checker, RealVector const *v, char const *virtualPatientFileName, double tau) {
  return _Checker_run(checker, v, virtualPatientFileName, tau);
}


CheckerResult *Checker_run_given_alpha_tau(Checker *checker, RealVector const *v, char const *virtualPatientFileName, double tau, double alpha) {
  CheckerOutcome outcome = CHECKER_OUTCOME_NOT_ADM;
  Debug_perform(DEBUG, {
    fprintf(stderr, "[%g", RealVector_get(v, 0));
    for (unsigned int i = 1; i < RealVector_dim(v); i++) {
      fprintf(stderr, ",%g", RealVector_get(v, i));
    }
    fprintf(stderr, "]\n");
  });
  if (!ParameterSpace_belongsTo(checker->pSpace, v)) {
    Debug_out(DEBUG, "Does not belong to space defined by input bounds.\n");
    return CheckerResult_new(NAN, outcome, NULL);
  }
  Debug_out(DEBUG, "Start Simulation\n");
  Patient *patient =
      Checker_simulate(checker, v, isnan(tau) ? 0 : tau, virtualPatientFileName);
  Debug_out(DEBUG, "End Simulation\n");
  if (patient == NULL) {
    Debug_out(DEBUG, "Error during simulation.\n");
    return CheckerResult_new(NAN, CHECKER_OUTCOME_ERR, NULL);
  }
  Debug_out(DEBUG, "Start Similarity Evaluation\n");
  bool isAdmissible = Checker_checkBA(checker, patient, alpha, tau);
  if (isAdmissible) {
    Debug_out(DEBUG, "Admissible with time-scale %g and time-shift %g.\n", alpha, tau);
    outcome = CHECKER_OUTCOME_ADM;
  }
  Debug_out(DEBUG, "End Similarity Evaluation\n");
  return CheckerResult_new(tau, outcome, patient);
}

CheckerResult *Checker_run(Checker *checker, RealVector const *v, char const *virtualPatientFileName) {
  return _Checker_run(checker, v, virtualPatientFileName, NAN);
}

bool Checker_checkBA(Checker *checker, Patient *patient, double alpha, double tau) {
  initWorkArrays(checker);
  Array *speciesBounds = checker->speciesBounds;
  Array *normCrossCorrBounds = checker->normCrossCorrBounds;
  Array *normEnergyDiffBounds = checker->normEnergyDiffBounds;
  unsigned int numSpecies = checker->numSpecies;
  TimeEvolution_Transformed *tetDefault = NULL;
  SignalMetrics *defaultMetrics = NULL;
  CheckerBounds *bounds = NULL;
  bool isAdmissible = true;

  SignalMetrics *metrics = SignalMetrics_new();
  TimeEvolution_Transformed *tet = NULL;
  for (unsigned int speciesIdx = 1; isAdmissible && speciesIdx <= numSpecies; speciesIdx++) {
    if (checker->species != NULL &&
        !HashSet_contains(checker->species, &speciesIdx, sizeof(unsigned int)))
      continue;
    Debug_out(DEBUG_VERBOSE,
              "(%d) Trying alpha = %g, "
              "tau = %g\n",
              speciesIdx, alpha, tau);

    tetDefault = NULL;
    Array_get(checker->defaultTETCache, speciesIdx - 1, &tetDefault);
    Debug_assert(DEBUG_ALWAYS, tetDefault != NULL, "tetDefault == NULL\n");
    tet = TimeEvolution_Transformed_new(
        Patient_evolution(patient, (int) speciesIdx), alpha, tau);

    defaultMetrics = NULL;
    Array_get(checker->defaultMetricsCache, speciesIdx - 1, &defaultMetrics);
    Debug_assert(DEBUG_ALWAYS, defaultMetrics != NULL,
                  "defaultMetrics == NULL\n");
    SignalMetrics_compute(metrics, tetDefault, tet, checker->timeRange);

    Array_get(speciesBounds, speciesIdx - 1, &bounds);
    if (Checker_isOutOfBounds(SignalMetrics_min(metrics), bounds) ||
        Checker_isOutOfBounds(SignalMetrics_max(metrics), bounds)) {
      Debug_out(DEBUG_VERBOSE, "Bounds FAIL %g,%g in [%g,%g]\n", SignalMetrics_min(metrics), SignalMetrics_max(metrics), CheckerBounds_min(bounds), CheckerBounds_max(bounds));
      isAdmissible = false;
    } else {
      Debug_out(DEBUG_VERBOSE, "Bounds PASS %g,%g in [%g,%g]\n", SignalMetrics_min(metrics), SignalMetrics_max(metrics), CheckerBounds_min(bounds), CheckerBounds_max(bounds));
      Array_set(checker->speciesMin, speciesIdx - 1,
                &SignalMetrics_min(metrics));
      Array_set(checker->speciesMax, speciesIdx - 1,
                &SignalMetrics_max(metrics));
    }

    // 1. Normalised Energy Difference
    if (isAdmissible) {
      Array_get(normEnergyDiffBounds, (unsigned int) speciesIdx - 1, &bounds);
      double normEnergyDiff =
          Checker_normalisedEnergyDifference(defaultMetrics, metrics);
      if (Checker_isOutOfBounds(normEnergyDiff, bounds)) {
        Debug_out(DEBUG_VERBOSE,
                  "Normalised Energy Difference FAIL %g in [%g,%g]\n",
                  normEnergyDiff, CheckerBounds_min(bounds),
                  CheckerBounds_max(bounds));
        isAdmissible = false;
      } else {
        Debug_out(DEBUG_VERBOSE, "Normalised Energy Difference PASS %g in [%g,%g]\n", normEnergyDiff, CheckerBounds_min(bounds), CheckerBounds_max(bounds));
        Array_set(checker->normEnergyDiff, speciesIdx - 1, &normEnergyDiff);
      }
    }
    // 2. Normalised Cross Corr
    if (isAdmissible) {
      Array_get(normCrossCorrBounds, (unsigned int) speciesIdx - 1, &bounds);
      double normZeroLagCrossCorr =
          Checker_normalisedZeroLagCrossCorr(defaultMetrics, metrics);
      if (Checker_isOutOfBounds(normZeroLagCrossCorr, bounds)) {
        Debug_out(DEBUG_VERBOSE,
                  "Normalised Zero Lag Cross Correlation FAIL %g in [%g,%g]\n", normZeroLagCrossCorr, CheckerBounds_min(bounds), CheckerBounds_max(bounds));
        isAdmissible = false;
      } else {
        Debug_out(DEBUG_VERBOSE,
                  "Normalised Zero Lag Cross Correlation PASS %g in [%g,%g]\n", normZeroLagCrossCorr, CheckerBounds_min(bounds), CheckerBounds_max(bounds));
        Array_set(checker->normCrossCorr, speciesIdx - 1,
                  &normZeroLagCrossCorr);
      }
    }
    TimeEvolution_Transformed_free(&tet);
  } // for each speciesIdx
  SignalMetrics_free(&metrics);
  return isAdmissible;
}

static double
Checker_normalisedEnergyDifference(SignalMetrics const *def,
                                    SignalMetrics const *metrics) {
  /* fprintf(stderr, "E_def: %.17g E: %.17g ==> KPI: %.17g\n", SignalMetrics_energy(def), SignalMetrics_energy(metrics), Numbers_relativeError(SignalMetrics_energy(def), SignalMetrics_energy(metrics))); */
  return Numbers_relativeError(SignalMetrics_energy(def),
                               SignalMetrics_energy(metrics));
}

static double
Checker_normalisedAverageDifference(SignalMetrics const *def,
                                    SignalMetrics const *metrics) {
  return Numbers_relativeError(SignalMetrics_avg(def),
                               SignalMetrics_avg(metrics));
}

static double
Checker_normalisedSampleVarianceDifference(SignalMetrics const *def,
                                           SignalMetrics const *metrics) {
  return Numbers_relativeError(SignalMetrics_sampleVar(def),
                               SignalMetrics_sampleVar(metrics));
}

static double Checker_normalisedZeroLagCrossCorr(SignalMetrics const *def,
                                                 SignalMetrics const *metrics) {
  return SignalMetrics_zeroLagCrossCorr(metrics) /
         (SignalMetrics_L2(def) * SignalMetrics_L2(metrics));
}

static void Checker_fillDefaultTETCache(Checker *checker) {
  for (int speciesIdx = 1; speciesIdx <= (int) checker->numSpecies;
       speciesIdx++) {
    TimeEvolution_Transformed *tet = TimeEvolution_Transformed_new(
        Patient_evolution(checker->patientDefault, speciesIdx), 1,
        0); // alpha = 1, tau = 0
    Array_add(checker->defaultTETCache, &tet);
  }
}

static void Checker_clearDefaultTETCache(Checker *checker) {
  unsigned int len = Array_length(checker->defaultTETCache);
  TimeEvolution_Transformed *tet = NULL;
  for (unsigned int i = 0; i < len; i++) {
    Array_get(checker->defaultTETCache, i, &tet);
    TimeEvolution_Transformed_free(&tet);
  }
  Array_clear(checker->defaultTETCache);
}

static void Checker_clearDefaultMetricsCache(Checker *checker) {
  for (unsigned int i = 0; i < Array_length(checker->defaultMetricsCache);
       i++) {
    SignalMetrics *s = NULL;
    Array_get(checker->defaultMetricsCache, i, &s);
    SignalMetrics_free(&s);
  }
  Array_clear(checker->defaultMetricsCache);
}

static void Checker_fillDefaultMetricsCache(Checker *checker) {
  for (unsigned int i = 0; i < (unsigned int) checker->numSpecies; i++) {
    TimeEvolution_Transformed *tet = NULL;
    Array_get(checker->defaultTETCache, i, &tet);
    Debug_assert(DEBUG_ALWAYS, tet != NULL, "tet == NULL\n");
    SignalMetrics *s = SignalMetrics_new();
    SignalMetrics_compute(s, tet, tet, checker->timeRange);
    Array_add(checker->defaultMetricsCache, &s);
  }
}


Array *Checker_admissibility_check(Array *checkers, RealVector *v,
                                  const char *virtualPatientFileNameTmp,
                                  CheckerOutcome *admissibility_check_outcome_ptr,
                                  double *tau_ptr) {
  double alpha = NAN;
  Checker *checker_0 = NULL;
  Array_get(checkers, 0, &checker_0);
  bool bounds_check = Checker_checkBounds(checker_0, v);
  if (!bounds_check) {
    *admissibility_check_outcome_ptr = CHECKER_OUTCOME_NOT_ADM;
    return NULL;
  }
  Patient *p = Checker_simulate(checker_0, v, 0, virtualPatientFileNameTmp);
  if (p == NULL) {
    *admissibility_check_outcome_ptr = CHECKER_OUTCOME_ERR;
    Patient_free(&p);
    return NULL;
  }
  Checker_initAlphaTau(checker_0, p);
  Array *results = Array_new(Array_length(checkers), sizeof(CheckerResult *), 1, NULL, NULL);
  CheckerResult *tonull = NULL;
  Array_add_n_times(results, &tonull, Array_length(checkers));
  Checker *checker = NULL;
  *admissibility_check_outcome_ptr = CHECKER_OUTCOME_NOT_ADM;
  CheckerResult *res = NULL;
  while (*admissibility_check_outcome_ptr != CHECKER_OUTCOME_ADM && Checker_nextAlphaTau(checker_0, tau_ptr, &alpha)) {
    for (unsigned int i = 1; i < Array_length(results); i++) {
        Array_get(results, i, &res);
        CheckerResult_free(&res);
        Array_set(results, i , &tonull);
    }
    if (Checker_checkBA(checker_0, p, alpha, *tau_ptr)) *admissibility_check_outcome_ptr = CHECKER_OUTCOME_ADM;
    for (unsigned int i = 1; *admissibility_check_outcome_ptr == CHECKER_OUTCOME_ADM && i < Array_length(checkers); i++) {
      Array_get(checkers, i, &checker);
      res = Checker_run_given_alpha_tau(checker, v, virtualPatientFileNameTmp, *tau_ptr, alpha);
      switch (CheckerResult_outcome(res)) {
        case CHECKER_OUTCOME_ADM:
          *tau_ptr = CheckerResult_tau(res);
          *admissibility_check_outcome_ptr = CHECKER_OUTCOME_ADM;
          Array_set(results, i, &res);
          break;
        case CHECKER_OUTCOME_NOT_ADM:
          *admissibility_check_outcome_ptr = CHECKER_OUTCOME_NOT_ADM;
          CheckerResult_free(&res);
          break;
        case CHECKER_OUTCOME_ERR:
          *admissibility_check_outcome_ptr = CHECKER_OUTCOME_ERR;
          CheckerResult_free(&res);
          break;
        default:
          fprintf(stderr, "This should not happen!\n");
          break;
      }
    }
  }
  if (*admissibility_check_outcome_ptr == CHECKER_OUTCOME_ADM) {
    res = CheckerResult_new(*tau_ptr, CHECKER_OUTCOME_ADM, p);
    Array_set(results, 0, &res);
  } else {
    Patient_free(&p);
  }
  if (*admissibility_check_outcome_ptr != CHECKER_OUTCOME_ADM) {
    for (unsigned int i = 0; i < Array_length(results); i++) {
      CheckerResult *f = NULL;
      Array_get(results, i, &f);
      CheckerResult_free(&f);
    }
    Array_free(&results);
    results = NULL;
  }
  return results;
}

