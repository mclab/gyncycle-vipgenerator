/*
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <math.h>
#include <MCLabUtils.h>
#include <RectRealVectorSpace.h>

#include "Sampler.h"
#include "RealVectorSet.h"

#define DEBUG "Sampler"
#define PROB_DECIMAL_PLACES (8)

struct Sampler {
  RndGen *rndGen;
  RectRealVectorSpace *space;
  RealVectorSet *set;
  RealVector *v;
  HashTable *mapping;
  RndGenDistribution *distribution;
  HashTable *distributionMapping;
};

static double Sampler_powerLaw(unsigned long x);

Sampler *Sampler_new(RectRealVectorSpace *space, RealVectorSet *set,
                     RndGen *rndGen, Properties const *options) {
  Debug_assert(DEBUG_ALWAYS, space != NULL, "space == NULL\n");
  Debug_assert(DEBUG_ALWAYS, set != NULL, "set == NULL\n");
  Debug_assert(DEBUG_ALWAYS, rndGen != NULL, "rndGen == NULL\n");
  char const *mappingPath = NULL;
  int err = Properties_getString(options, "mappingPath", &mappingPath);
  Sampler *sampler = calloc(1, sizeof(Sampler));
  sampler->space = space;
  sampler->rndGen = rndGen;
  sampler->set = set;
  sampler->v = RealVector_new(RectRealVectorSpace_dim(space));
  sampler->mapping = HashTable_new();
  Properties *mappingProp = Properties_newFromFile(mappingPath);
  Properties_Iterator *iter = Properties_Iterator_new(mappingProp);
  Properties_Entry const *entry = NULL;
  Properties const *content = NULL;
  char const *key = NULL;
  char const *value = NULL;
  int idx = 0;
  while ((entry = Properties_Iterator_next(iter)) != NULL) {
    key = Properties_Entry_property(entry);
    StringUtils_toInt(key, &idx);
    content = Properties_Content_value(Properties_Entry_value(entry));
    int speciesIdx = -1;
    int parameterIdx = -1;
    Properties_getInt(content, "s", &speciesIdx);
    Properties_getInt(content, "p", &parameterIdx);
    Array *components = NULL;
    if (HashTable_get(sampler->mapping, &speciesIdx, sizeof(int), (void **) &components) == NULL) {
      components = Array_newInt(10, 10);
    }
    Array_add(components, &idx);
    HashTable_put(sampler->mapping, &speciesIdx, sizeof(int), components);
  }
  Properties_Iterator_free(&iter);
  Properties_free(&mappingProp);
  sampler->distribution = RndGenDistribution_new_fromFunction(sampler->rndGen, Sampler_powerLaw, 1, HashTable_size(sampler->mapping), PROB_DECIMAL_PLACES);
  Debug_out(DEBUG, "Expected Value: %g\n", RndGenDistribution_expectedValue(sampler->distribution));
  idx = 0;
  sampler->distributionMapping = HashTable_new();
  HashTableIterator *it = HashTableIterator_new(sampler->mapping);
  HashTable_Entry *e = NULL;
  while (NULL != (e = HashTableIterator_next(it))) {
    HashTable_put(sampler->distributionMapping, &idx, sizeof(int), (void *) HashTable_Entry_key(e));
    idx++;
  }
  HashTableIterator_free(&it);
  return sampler;
}

void Sampler_free(Sampler **thisP) {
  if (*thisP == NULL) return;
  Sampler *this = *thisP;
  RealVector_free(&this->v);
  RndGenDistribution_free(&this->distribution);
  HashTableIterator *it = HashTableIterator_new(this->mapping);
  HashTable_Entry *e = NULL;
  Array *value = NULL;
  while (NULL != (e = HashTableIterator_next(it))) {
    value = HashTable_Entry_value(e);
    Array_free(&value);
  }
  HashTableIterator_free(&it);
  HashTable_free(&this->mapping);
  HashTable_free(&this->distributionMapping);
  free(this);
  this = NULL;
}

static double Sampler_powerLaw(unsigned long x) {
  Debug_assert(DEBUG_ALL, x > 0, "x = %lu <= 0\n", x);
  return pow(x, -1.4);
}

void Sampler_sample(Sampler *sampler, RealVector *res) {
  Debug_assert(DEBUG_ALWAYS, sampler != NULL, "sampler == NULL\n");
  Debug_assert(DEBUG_ALWAYS, res != NULL, "res == NULL\n");
  RealVectorSet_random(sampler->set, sampler->rndGen, sampler->v);
  int numSpeciesToChange = (int) RndGenDistribution_next(sampler->distribution);
  Debug_out(DEBUG, "# Species to change: %d\n", numSpeciesToChange);
  Array *componentsToChange = Array_newInt(RectRealVectorSpace_dim(sampler->space), 10);
  HashSet *speciesToChange = HashSet_new();
  for (int i = 0; i < numSpeciesToChange; i++) {
    int *speciesIdxPtr;
    int rndIdx = (int) RndGen_nextUL(sampler->rndGen, 0, HashTable_size(sampler->distributionMapping)-1);
    if (!HashSet_contains(speciesToChange, &rndIdx, sizeof(int))) HashSet_add(speciesToChange, &rndIdx, sizeof(int));
    else {i-=1; continue;}
    HashTable_get(sampler->distributionMapping, &rndIdx, sizeof(int), (void **) &speciesIdxPtr);
    Array *components = NULL;
    void *err = HashTable_get(sampler->mapping, speciesIdxPtr, sizeof(int), (void **) &components);
    Debug_assert(DEBUG_ALWAYS, err != NULL, "err == NULL\n");
    for (unsigned int j = 0; j < Array_length(components); j++) {
      int component = -1;
      Array_get(components, j, &component);
      Array_add(componentsToChange, &component);
    }
    Debug_out(DEBUG, "species: %d, components: %u\n", *speciesIdxPtr, Array_length(components));
  }
  /* Debug_perform(DEBUG, Array_fprint(componentsToChange, stderr)); */
  RectRealVectorSpace_randomComponents(sampler->space, sampler->rndGen, sampler->v, res, componentsToChange);
  Array_free(&componentsToChange);
  HashSet_free(&speciesToChange);
  Debug_perform(DEBUG, fprintf(stderr, "[");
                fprintf(stderr, "%g", RealVector_get(res, 0));
                for (unsigned int i = 1; i < RealVector_dim(res); i++)
                    fprintf(stderr, ", %g", RealVector_get(res, i));
                fprintf(stderr, "]\n"););
}

void Sampler_sampleSeq(Sampler *sampler, Array *samples) {
  Debug_assert(DEBUG_ALWAYS, sampler != NULL, "sampler == NULL\n");
  Debug_assert(DEBUG_ALWAYS, samples != NULL, "samples == NULL\n");

  RealVector *res = NULL;
  for (unsigned int i = 0; i < Array_length(samples); i++) {
    Array_get(samples, i, &res);
    Sampler_sample(sampler, res);
  }
}
