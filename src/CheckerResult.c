/*
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include <MCLabUtils.h>

#include "Patient.h"
#include "CheckerOutcome.h"
#include "CheckerResult.h"

struct CheckerResult {
  double tau;
  Patient *patient;
  CheckerOutcome outcome;
};

CheckerResult *
CheckerResult_new(double tau, CheckerOutcome outcome, Patient *patient) {
  CheckerResult *result = calloc(1, sizeof(CheckerResult));
  result->tau = tau;
  result->outcome = outcome;
  result->patient = patient;
  return result;
}

CheckerOutcome CheckerResult_outcome(CheckerResult const *result) {
  Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");
  return result->outcome;
}

double CheckerResult_tau(CheckerResult const *result) {
  Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");
  return result->tau;
}


Patient const *CheckerResult_patient(CheckerResult const *result) {
  Debug_assert(DEBUG_ALWAYS, result != NULL, "result == NULL\n");
  return result->patient;
}

void CheckerResult_free(CheckerResult **resultP) {
  Debug_assert(DEBUG_ALWAYS, resultP != NULL, "resultP == NULL\n");
  if (*resultP == NULL) return;
  CheckerResult *result = *resultP;
  if (result->patient != NULL) Patient_free(&result->patient);
  free(*resultP);
}
