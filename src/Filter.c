/*
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_fft_real.h>

#include <MCLabUtils.h>

#include "TimeEvolution.h"
#include "TimeEvolution_Transformed.h"
#include "Patient.h"
#include "CheckerOutcome.h"
#include "CheckerResult.h"
#include "Fingerprint.h"
#include "FilterResult.h"

#include "Filter.h"

#define DEBUG "Filter"
#define DEBUG_FILTER_FINGERPRINT "Filter Fingerprint"
#define DEBUG_FILTER_FINGERPRINT_DISCRETIZE "Filter Fingerprint Discretize"
#define DEBUG_FILTER_FINGERPRINT_NORM "Filter Fingerprint Norm"
#define DEBUG_FILTER_FINGERPRINT_DISTANCE "Filter Fingerprint distance"
#define DEBUG_FILTER_MAGNITUDE_COMPUTATION "Filter Magnitude Computation"
#define DEBUG_FILTER_FINGERPRINT_PERIOD "Filter Fingerprint Period"
#define DEBUG_FILTER_FEATURES "Filter Features"
#define DEBUG_FILTER_STEPS "Filter Steps"

struct Filter {
  unsigned int numSpecies;
  Array *features;
  Array *perc_def_step;
  Array *timeRanges;
  unsigned int layers;
  unsigned int inputs;

  unsigned int numExternalFactors;
  unsigned int fingerprintLen;

  Range_Double *periodRange;
  unsigned int periodSpeciesIdx;

  Array * /*Patient */ def;
  Array *denominators;
  double minStep;
  Array *normEnergyDiffBounds;

  Array *speciesIdxs;
};

static Array *computeSquaredMagnitude(Filter const *f, Patient const *p, double tau, unsigned int input);
static Array *computeDenominators(Filter const *f, Patient const *p, double tau, unsigned int input);
static double computeStep(Filter const *f, unsigned int layer);
static double normalize(double squared_magnitude, double denominator);
static int32_t quantize(double value, double step, double *value_discr);

Filter *Filter_new(unsigned int numSpecies, Array /*Patient*/ *def, Properties const *filterOptions) {
  Debug_assert(DEBUG_ALWAYS, filterOptions != NULL, "filterOptions == NULL\n");
  Filter *f = calloc(1, sizeof(Filter));
  f->numSpecies = numSpecies;
  int err = 0;
  char const *perc_def_step_str = NULL;
  err = Properties_getString(filterOptions, "perc default step", &perc_def_step_str);
  char *perc_def_step_str_clone = StringUtils_clone(perc_def_step_str);
  f->perc_def_step = Array_newDouble(1,1);
  if (err == 0) {
    Array *tokens = Array_new(100, sizeof(char *), 1, NULL, NULL);
    StringUtils_tokenize(perc_def_step_str_clone, ",", tokens);
    for (unsigned int i = 0; i < Array_length(tokens); i++) {
      double perc_def_step_i = 0;
      char *perc_def_step_str_i = NULL;
      Array_get(tokens, i, &perc_def_step_str_i);
      StringUtils_toDouble(perc_def_step_str_i, &perc_def_step_i);
      Array_add(f->perc_def_step, &perc_def_step_i);
    }
    Array_free(&tokens);
  }
  free(perc_def_step_str_clone);
  f->layers = Array_length(f->perc_def_step);
  err = Properties_getDouble(filterOptions, "minStep", &f->minStep);

  f->timeRanges = Array_new(Array_length(def), sizeof(Range_Double *), 1, NULL, NULL);
  char const *timeRanges_str = NULL;
  err = Properties_getString(filterOptions, "timeRanges", &timeRanges_str);
  char *timeRanges_str_clone = StringUtils_clone(timeRanges_str);
  f->timeRanges = Array_new(1,sizeof(Range_Double *), 1, NULL, NULL);
  if (err == 0) {
    Array *tokens = Array_new(100, sizeof(char *), 1, NULL, NULL);
    StringUtils_tokenize(timeRanges_str_clone, ";", tokens);
    for (unsigned int i = 0; i < Array_length(tokens); i++) {
      Range_Double *timeRange = NULL;
      char *timeRanges_str_i = NULL;
      Array_get(tokens, i, &timeRanges_str_i);
      timeRange = Range_Double_new_fromString(timeRanges_str_i);
      Array_add(f->timeRanges, &timeRange);
    }
    Array_free(&tokens);
  }
  free(timeRanges_str_clone);
  f->inputs = Array_length(f->timeRanges);

  f->periodSpeciesIdx = 0;
  f->numExternalFactors = 0;
  err = Properties_getUInt(filterOptions, "num external factors", &f->numExternalFactors);
  if (f->numExternalFactors > 0) {
    char const *periodRange_str = NULL;
    err = Properties_getString(filterOptions, "periodRange", &periodRange_str);
    Debug_assert(DEBUG_ALWAYS, err == 0, "'periodRange' missing in filterOptions\n");
    f->periodRange = Range_Double_new_fromString(periodRange_str);
    err = Properties_getUInt(filterOptions, "periodSpeciesIdx", &f->periodSpeciesIdx);
    Debug_assert(DEBUG_ALL, err == 0, "'periodSpeciesIdx' missing in filterOptions\n");
  } else {
    f->periodRange = NULL;
  }

  char const *speciesIdxs_str = NULL;
  err = Properties_getString(filterOptions, "speciesIdxs", &speciesIdxs_str);
  char *speciesIdxs_str_clone = StringUtils_clone(speciesIdxs_str);
  f->speciesIdxs = Array_new(f->numSpecies, sizeof(unsigned int), 1, NULL, NULL);
  if (err == 0) {
   Array *tokens = Array_new(100, sizeof(char *), 1, NULL, NULL);
   StringUtils_tokenize(speciesIdxs_str_clone, ",", tokens);
   for (unsigned int i = 0; i < Array_length(tokens); i++) {
     unsigned int speciesIdx = 0;
     char *speciesIdx_str = NULL;
     Array_get(tokens, i, &speciesIdx_str);
     StringUtils_toUInt(speciesIdx_str, &speciesIdx);
     Array_add(f->speciesIdxs, &speciesIdx);
   }
   Array_free(&tokens);
   f->numSpecies = Array_length(f->speciesIdxs);
  } else {
    for (unsigned int i = 1; i <= f->numSpecies; i++) {
      Array_add(f->speciesIdxs, &i);
    }
  }
  free(speciesIdxs_str_clone);

  f->normEnergyDiffBounds = Array_newDouble(f->numSpecies, 1);
  for (unsigned int i = 0; i < f->numSpecies; i++) {
    unsigned int s = 0;
    Array_get(f->speciesIdxs, i, &s);
    char *str = NULL;
    asprintf(&str, "%d/normEnergyDiff", s);
    double value = NAN;
    err = Properties_getDouble(filterOptions, str, &value);
    Debug_assert(DEBUG_ALWAYS, err == 0, "'%s' is missing in filterOptions\n", str);
    Array_add(f->normEnergyDiffBounds, &value);
    free(str);
  }

  const char *features_str = NULL;
  err = Properties_getString(filterOptions, "features", &features_str);
  f->features = Array_new(f->inputs, sizeof(unsigned int), 1, NULL, NULL);
  if (err == 0) {
    Array *tokens = Array_newString(10, 10);
    StringUtils_tokenize((char *) features_str, ",", tokens);
    char *features_i_str = NULL;
    for (unsigned int i = 0; i < Array_length(tokens); i++) {
      Array_get(tokens, i, &features_i_str);
      unsigned int features_i = 0;
      StringUtils_toUInt(features_i_str, &features_i);
      if (features_i == 0) {
        Range_Double *timeRange = NULL;
        Array_get(f->timeRanges, i, &timeRange);
        features_i = (unsigned int) (Range_Double_size(timeRange) / 2.0) + 1;
        Debug_out(DEBUG_FILTER_FEATURES, "features[%u] = %u\n", i, features_i);
      }
      Array_add(f->features, &features_i);
    }
    Array_free(&tokens);
  }

  f->def = Array_clone(def);
  f->denominators = Array_new(f->inputs, sizeof(Array *), 1, NULL, NULL);
  for (unsigned int i = 0; i < f->inputs; i++) {
    Patient *def_i = NULL;
    Array_get(f->def, i, &def_i);
    Array *denominators_i = computeDenominators(f, def_i, 0.0, i);
    Array_add(f->denominators, &denominators_i);
  }
  return f;
}

static Array *computeDenominators(Filter const *f, Patient const *p, double tau, unsigned int input) {
  Array *denominators = Array_newDouble(f->numSpecies, 1);
  Range_Double *timeRange = NULL;
  Array_get(f->timeRanges, input, &timeRange);
  double timeRange_size = Range_Double_size(timeRange);
  Range_Double_Iterator *it = Range_Double_Iterator_new(timeRange);
  TimeEvolution_Transformed *te = NULL;
  unsigned int speciesIdx = 0;
  Debug_out(DEBUG_FILTER_FINGERPRINT, "===== Input %u\n", input);
  double theta = NAN;
  for (unsigned int i = 0; i < f->numSpecies; i++) {
    Array_get(f->speciesIdxs, i, &speciesIdx);
    Array_get(f->normEnergyDiffBounds, i, &theta);
    Debug_out(DEBUG_FILTER_FINGERPRINT, "===== Species %u\n", speciesIdx);
    te = TimeEvolution_Transformed_new(Patient_evolution(p, (int) speciesIdx),
                                       1.0, tau);
    double t = NAN;
    double energy = 0.0;
    while (Range_Double_Iterator_next(it, &t)) {
      int tIdx = TimeEvolution_Transformed_lastSampleBefore(te, t);
      Debug_assert(DEBUG_ALWAYS, tIdx >= 0, "tIdx < 0\n");
      double value = TimeEvolution_Transformed_value(te, tIdx);
      energy += value * value;
    }
    Range_Double_Iterator_rewind(it);
    TimeEvolution_Transformed_free(&te);
    double denominator = energy;
    Debug_out(DEBUG_FILTER_FINGERPRINT, "energy: %.17g, denominator = %.17g\n", energy, denominator);
    Array_add(denominators, &denominator);
  }
  Debug_out(DEBUG_FILTER_FINGERPRINT, "================================\n");
  Range_Double_Iterator_free(&it);
  return denominators;
}

static double computeStep(Filter const *f, unsigned int layer) {
  double perc_def_step = NAN;
  Array_get(f->perc_def_step, layer, &perc_def_step);
  return f->minStep * perc_def_step;
}

static Array *computeSquaredMagnitude(Filter const *f, Patient const *p, double tau, unsigned int input) {
  Array *squared_magnitude_p=
      Array_new(f->numSpecies, sizeof(Array *), 1, NULL, NULL);
  Range_Double *timeRange = NULL;
  Array_get(f->timeRanges, input, &timeRange);
  size_t n = (size_t) Range_Double_size(timeRange);
  gsl_fft_real_workspace *fft_work = gsl_fft_real_workspace_alloc(n);
  TimeEvolution_Transformed *te = NULL;
  Range_Double_Iterator *it = Range_Double_Iterator_new(timeRange);
  unsigned int speciesIdx = 0;
  Debug_out(DEBUG_FILTER_FINGERPRINT, "===== Input %u\n", input);
  for (unsigned int i = 0; i < f->numSpecies; i++) {
    Array_get(f->speciesIdxs, i, &speciesIdx);
    Debug_out(DEBUG_FILTER_FINGERPRINT, "===== Species %u\n", speciesIdx);
    te = TimeEvolution_Transformed_new(Patient_evolution(p, (int) speciesIdx),
                                       1.0, tau);
    double t = NAN;
    Array *data = Array_newDouble((unsigned int) n, 1);
    while (Range_Double_Iterator_next(it, &t)) {
      int tIdx = TimeEvolution_Transformed_lastSampleBefore(te, t);
      Debug_assert(DEBUG_ALWAYS, tIdx >= 0, "tIdx < 0\n");
      double value = TimeEvolution_Transformed_value(te, tIdx);
      Array_add(data, &value);
    }
    Range_Double_Iterator_rewind(it);
    TimeEvolution_Transformed_free(&te);
    double *data_C_array = (double *) Array_as_C_array(data);
    gsl_fft_real_wavetable *real = gsl_fft_real_wavetable_alloc(n);
    int err = gsl_fft_real_transform(data_C_array, 1, n, real, fft_work);
    gsl_fft_real_wavetable_free(real);
    Debug_assert(DEBUG_ALWAYS, err == 0, "Error in computing DFT\n");

    /*
    es. n = 6
    squared_magnitude[0] = sqrt(data[0]^2)/n
    squared_magnitude[1] = 2 * sqrt(data[1]^2 + data[2]^2)/n
    squared_magnitude[2] uses  2 * sqrt(data[3]^2 + data[4]^2)/n
    squared_magnitude[3] uses  sqrt(data[5]^2)/n

    es. n = 5
    squared_magnitude[0] uses sqrt(data[0]^2)/n
    squared_magnitude[1] uses 2 * sqrt(data[1]^2 + data[2]^2)/n
    squared_magnitude[2] uses 2 * sqrt(data[3]^2 + data[4]^2)/n */
    Array *squared_magnitude_p_s = Array_newDouble((unsigned int) floor(n/2) + 1, 1);
    double nan = NAN;
    Array_add_n_times(squared_magnitude_p_s, &nan, (unsigned int) floor(n/2) + 1);
    double *squared_magnitude_p_s_as_C_array = (double *) Array_as_C_array(squared_magnitude_p_s);
    unsigned int idx = 0;
    double vreal = data_C_array[idx];
    double vimag = 0.0;
    gsl_complex z;
    GSL_SET_COMPLEX(&z, vreal, vimag);
    squared_magnitude_p_s_as_C_array[idx] = gsl_complex_abs2(z) / (double) n;
    Debug_out(DEBUG_FILTER_MAGNITUDE_COMPUTATION, "|X[%u]|^2/%lu = %.17g, X[%u].real = %.17g, X[%u].imag = %.17g\n", idx, n, squared_magnitude_p_s_as_C_array[idx], idx, vreal, idx, vimag);
    for (idx = 1; idx < n - idx; idx++) {
      vreal = data_C_array[(2 * idx - 1)];
      vimag = data_C_array[(2 * idx)];
      GSL_SET_COMPLEX(&z, vreal, vimag);
      squared_magnitude_p_s_as_C_array[idx] = 2 * gsl_complex_abs2(z) / (double) n;
      Debug_out(DEBUG_FILTER_MAGNITUDE_COMPUTATION, "2*|X[%u]|^2/%zu = |X[%u]|^2/%zu + |X[%lu]|^2/%zu = %.17g, X[%u].real = %.17g, X[%u].imag = %.17g, X[%lu].real = %.17g, X[%lu].imag = %.17g\n", idx, n, idx, n, n-idx, n, squared_magnitude_p_s_as_C_array[idx], idx, vreal, idx, vimag, n-idx, vreal, n-idx, -vimag);
    }
    if (idx == n - idx) { // if n is even
      vreal = data_C_array[(n - 1)];
      vimag = 0.0;
      GSL_SET_COMPLEX(&z, vreal, vimag);
      squared_magnitude_p_s_as_C_array[idx] = gsl_complex_abs2(z) / (double) n;
      Debug_out(DEBUG_FILTER_MAGNITUDE_COMPUTATION, "|X[%u]|^2/%lu = %.17g, X[%u].real = %.17g, X[%u].imag = %.17g\n", idx, n, squared_magnitude_p_s_as_C_array[idx], idx, vreal, idx, vimag);
    }
    Array_add(squared_magnitude_p, &squared_magnitude_p_s);
    Array_free(&data);
  }
  Range_Double_Iterator_free(&it);
  Debug_out(DEBUG_FILTER_FINGERPRINT, "================================\n");
  gsl_fft_real_workspace_free(fft_work);
  return squared_magnitude_p;
}

void Filter_free(Filter **fP) {
  Debug_assert(DEBUG_ALWAYS, fP != NULL, "fP == NULL");
  if (*fP == NULL) return;
  Filter *f = *fP;
  Array_free(&f->timeRanges);
  Array_free(&f->perc_def_step);
  for (unsigned int i = 0; i < f->inputs; i++) {
    Array *den_i = NULL;
    Array_get(f->denominators, i, &den_i);
    Array_free(&den_i);
  }
  Array_free(&f->denominators);
  Array_free(&f->speciesIdxs);
  Array_free(&f->normEnergyDiffBounds);
  Range_Double_free(&f->periodRange);
  free(*fP);
}

unsigned int Filter_layers(Filter const *f) {
  Debug_assert(DEBUG_ALWAYS, f != NULL, "f == NULL\n");
  return (unsigned int) f->layers;
}

static int32_t quantize(double value, double step, double *value_discr) {
  int32_t quantum_idx =  (int32_t) floor(value / step);
  *value_discr = quantum_idx * step;
  Debug_out(DEBUG_FILTER_FINGERPRINT_DISCRETIZE,
            "value: %.17g, step: %.17g, quantum = %d, value_discr: %.17g\n",
            value, step, quantum_idx, *value_discr);
  return quantum_idx;
}

static double normalize(double squared_magnitude, double denominator) {
  double norm_squared_magnitude = squared_magnitude / denominator;
  Debug_out(DEBUG_FILTER_FINGERPRINT_NORM,
            "squared_magnitude: %.17g, denominator: %.17g, norm_squared_magnitude = %.17g\n",
            squared_magnitude, denominator, norm_squared_magnitude);
  // Debug_assert(DEBUG_ALWAYS, norm_squared_magnitude <= 1.8, "norm_squared_magnitude == %.17g >> 1.8\n", norm_squared_magnitude);
  return norm_squared_magnitude;
}


FilterResult *Filter_filter(const Filter *f, const Array *v, const Array *checker_results) {
  Debug_assert(DEBUG_ALWAYS, f != NULL, "f == NULL\n");
  Debug_assert(DEBUG_ALWAYS, checker_results != NULL, "p == NULL\n");

  unsigned int layers = Filter_layers(f);
  Array *fingerprints = Array_new(layers, sizeof(Fingerprint *), 1, NULL, NULL);
  unsigned int numExternalFactors = f->numExternalFactors;
  for (unsigned int i = 0; i < layers; i++) {
    Fingerprint *s =
        Fingerprint_new(f->numSpecies, f->inputs, numExternalFactors, f->features);
    Array_add(fingerprints, &s);
  }
  Array *distances = Array_newDouble(layers, 1);
  double zero = 0.0;
  Array_add_n_times(distances, &zero, layers);
  Debug_assert(DEBUG_ALWAYS, numExternalFactors <= 0 || v != NULL, "v == NULL\n");

  // compute DFT
  Patient const *p = NULL;
  Array *squared_magnitude_u = NULL;
  Array *squared_magnitude = Array_new(f->inputs, sizeof(Array *), 1, NULL, NULL);
  int32_t periodValue = 0;
  for (unsigned int i = 0; i < f->inputs; i++) {
    CheckerResult *res = NULL;
    Array_get(checker_results, i, &res);
    p = CheckerResult_patient(res);
    squared_magnitude_u = computeSquaredMagnitude(f, p, CheckerResult_tau(res), i);
    Array_add(squared_magnitude, &squared_magnitude_u);
    if (f->numExternalFactors > 0 && i == 0) {
      TimeEvolution_Transformed *evol = TimeEvolution_Transformed_new(Patient_evolution(p, (int) f->periodSpeciesIdx), 1, CheckerResult_tau(res));
      double argmax1 = TimeEvolution_Transformed_findArgMaxWithinInterval(evol, 0, Range_Double_max(f->periodRange)/2.0 + 1.0);
      double argmax2 = TimeEvolution_Transformed_findArgMaxWithinInterval(evol, argmax1 + 3.0, argmax1 + 3.0 + Range_Double_max(f->periodRange) + 1.0);
      periodValue = (int32_t) Range_Double_closest_value(f->periodRange, argmax2 - argmax1);
      TimeEvolution_Transformed_free(&evol);
      Debug_out(DEBUG_FILTER_FINGERPRINT_PERIOD, "===== Computed Period %d\n", periodValue);
    }
  }
  // compute fingerprints
  squared_magnitude_u = NULL;
  Array *squared_magnitude_u_s = NULL;
  Array *denominator_u = NULL;
  double denominator_u_s = NAN;
  unsigned int speciesIdx = 0;
  unsigned int features_u = 0;
  for (unsigned int u = 0; u < f->inputs; u++) {
    Array_get(f->features, u, &features_u);
    Debug_out(DEBUG_FILTER_FINGERPRINT, "===== Input %u Features %u\n", u, features_u);
    Array_get(squared_magnitude, u, &squared_magnitude_u);
    Array_get(f->denominators, u, &denominator_u);
    for (unsigned int s = 0; s < f->numSpecies; s++) {
      Array_get(f->speciesIdxs, s, &speciesIdx);
      Debug_out(DEBUG_FILTER_FINGERPRINT, "===== Species %u\n", speciesIdx);
      Array_get(squared_magnitude_u, s, &squared_magnitude_u_s);
      Array_get(denominator_u, s, &denominator_u_s);
      Fingerprint *fin = NULL;
      double distance = NAN;
      for (unsigned int l = 0; l < layers; l++) {
        Debug_out(DEBUG_FILTER_FINGERPRINT, "======= Layer %u\n", l);
        Array_get(fingerprints, l, &fin);
        Array_get(distances, l, &distance);
        if (numExternalFactors > 0) {
          for (unsigned int efIdx = 0; efIdx < numExternalFactors; efIdx++) {
            Fingerprint_setExternalFactors(fin, efIdx, periodValue);
          }
        }
        double value = NAN;
        double value_discr = NAN;
        int32_t fp_values[features_u];
        double distance_s_l = 0.0;
        for (unsigned int feature = 0; feature < features_u; feature++) {
          Array_get(squared_magnitude_u_s, feature, &value);
          double step = computeStep(f, l);
          value = normalize(value, denominator_u_s);
          fp_values[feature] = quantize(value, step, &value_discr);
          distance_s_l += (value - value_discr) * (value - value_discr);
        } // for each feature
        distance += distance_s_l;
        Array_set(distances, l, &distance);
        Fingerprint_setFeatures(fin, s + 1, u, fp_values);
      } // for each layer
    }   // for each each species
  } // for each input
  for (unsigned int i = 0; i < Array_length(distances); i++) {
    double distance = NAN;
    Array_get(distances, i, &distance);
    distance = sqrt(distance);
    Debug_out(DEBUG_FILTER_FINGERPRINT_DISTANCE, "======= Layer %u, Distance %.17g\n", i, distance);
    Array_set(distances, i, &distance);
  }
  for (unsigned int i = 0; i < Array_length(squared_magnitude); i++) {
    Array *a = NULL;
    Array_get(squared_magnitude, i, &a);
    for (unsigned int j = 0; j < Array_length(a); j++) {
      Array *b = NULL;
      Array_get(a, j, &b);
      Array_free(&b);
    }
    Array_free(&a);
  }
  Array_free(&squared_magnitude);
  Debug_out(DEBUG_FILTER_FINGERPRINT, "================================\n");
  return FilterResult_new(fingerprints, distances);
}
