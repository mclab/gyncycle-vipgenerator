/*
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include <config.h>
#include <MCLabUtils.h>
#include <ViPGenerator.h>

#define DEBUG_THIS "main"

int main(int argc, char *argv[]) {
  Debug_setDefaultOut(0);
  Debug_enableOut("Worker");
  Debug_enableOut("ParameterSpace");
  Debug_enableOut("MC2");
  Debug_enableOut("Checker");
  Debug_enableOut("Simulator");
  return ViPGenerator_run(argc, argv);
}
