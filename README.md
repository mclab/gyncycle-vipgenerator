# GynCycle-ViPGenerator

GynCycle-ViPGenerator is a tool for generating population of virtual patients for the GynCycle model.

For a complete and detailed documentation of ViPGenerator please read [ViPGenerator README](https://bitbucket.org/mclab/vipgenerator).

## Build

### Requirements

GynCycle-ViPGenerator depends on the following requirements:

* [CMake](https://cmake.org)

* [MCLab CMake Modules](https://bitbucket.org/mclab/cmake)

* [MCLabUtils](https://bitbucket.org/mclab/mclabutils)

* [RealVectorSpace](https://bitbucket.org/mclab/realvectorspace)

* [MultiProcess](https://bitbucket.org/mclab/multiprocess)

* [SQLite3](https://sqlite3.org)

* [JModelica](https://jmodelica.org)

* [ORTools](https://bitbucket.org/mclab/cmake)

### Compile

To build GynCycle-ViPGenerator execute the following instructions:
```
git submodule update --init --recursive
mkdir build && cd build
cmake -DCMAKE_LIBRARY_PATH=<ortools-dir>/lib -DCMAKE_INCLUDE_PATH=<ortools-dir>/include/  -DFIND_ALPHA_TAU=ON ..
make
```

### Compile GynCycle to FMU

To obtain an FMU of the GynCycle model, that is needed as input for the ViPGenerator, execute the following instructions:
```
cd data/model
python2.7 compilefmu.py GynCycle.Model GynCycle
```

## Run

To run GynCycle-ViPGenerator tool execute the following command:
```
mpiexec -np <num parallel processes> ./build/GynCycle-ViPGenerator --config data/gyncycle.conf.prop.txt
```

## Docker

This repo provides a Docker Image to run the tool.
To build the docker image execute the following command:

```
docker build -t gyncycle-vipgenerator .
```

Once the docker image is built, you can run a docker container by executing the following command:
```
docker run --rm -it -v"${PWD}"/output:/root/work/tool/output gyncycle-vipgenerator bash
```

In the docker container, you can run directly the tool as specified in the above sections.


## Recommended Citation

S. Sinisi, V. Alimguzhin, T. Mancini, E. Tronci, B. Leeners, Complete populations of virtual patients for
in silico clinical trials, Bioinformatics,
[https://doi.org/10.1093/bioinformatics/btaa1026](https://doi.org/10.1093/bioinformatics/btaa1026)


## Support

If you have any problems or questions, please feel free to contact us.

## Authors

* [Stefano Sinisi](sinisi@di.uniroma1.it)
* [Vadim Alimguzhin](alimguzhin@di.uniroma1.it)

## License

GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published
by the Free Software Foundation.

GynCycle-ViPGenerator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GynCycle-ViPGenerator.
If not, see <https://www.gnu.org/licenses/>.
