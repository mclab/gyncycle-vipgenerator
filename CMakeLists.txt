#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.3)

project(GynCycle-ViPGenerator VERSION 0.1.0)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules)

include(MCLabTool)

enable_language(Fortran)

set(SRCS
  src/Checker.c
  src/CheckerResult.c
  src/Filter.c
  src/Sampler.c
)


set(FIND_ALPHA_TAU OFF CACHE BOOL "Use ORTOOLS to find alpha-tau...")

if(FIND_ALPHA_TAU)
  mclab_add_executable(${PROJECT_NAME} ${SRCS} src/AlignmentsIterator.cpp src/main.c)
  mclab_cxx_standard(${PROJECT_NAME} 14)
else(FIND_ALPHA_TAU)
  mclab_add_executable(${PROJECT_NAME} ${SRCS} src/main.c)
endif(FIND_ALPHA_TAU)

mclab_link_library(${PROJECT_NAME} RealVectorSpace)
mclab_link_library(${PROJECT_NAME} MCLabUtils)
mclab_link_library(${PROJECT_NAME} ViPGenerator)
mclab_find_and_link_library(${PROJECT_NAME} SQLITE3 REQUIRED)
mclab_find_and_link_library(${PROJECT_NAME} GSL REQUIRED)
mclab_find_and_link_library(${PROJECT_NAME} ZLIB REQUIRED)

if(FIND_ALPHA_TAU)
  mclab_find_and_link_library(${PROJECT_NAME} ORTOOLS REQUIRED)
endif(FIND_ALPHA_TAU)

mclab_mpi(${PROJECT_NAME})
mclab_link_library(${PROJECT_NAME} MultiProcess)

mclab_generate_config_header()

mclab_check_deps()

