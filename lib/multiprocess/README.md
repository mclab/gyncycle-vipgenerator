# MultiProcess #

MultiProcess is a C library implementing simple APIs for [MPI](https://www.open-mpi.org).

The package is versioned using the [semantic versioning policy](http://semver.org).

## Building prerequisites ##

MCLabUtils Library depends on the following:

* [CMake](https://cmake.org)
* [MCLab CMake modules](https://bitbucket.org/mclab/cmake)
* [MCLabUtils](https://bitbucket.org/mclab/mclabutils)

## For users ##

Make sure that your CMake module path contains path to [MCLab CMake modules](https://bitbucket.org/mclab/cmake).

Use the following CMake macro to link your library/tool to MultiProcess:

```
mclab_link_library(MultiProcess)
```

## For developers ##

Use [MCLab libraries devenv](https://bitbucket.org/mclab/devenv) when working on the library.

## Contributors ##
* [Toni Mancini](https://bitbucket.org/tmancini/)

## License

MultiProcess is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published
by the Free Software Foundation.

MultiProcess is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MultiProcess.
If not, see <https://www.gnu.org/licenses/>.

