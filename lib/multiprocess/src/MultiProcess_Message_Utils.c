/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>

#include <Debug.h>
#include <BasicTypeValue.h>

#include <MultiProcess_Message_Utils.h>


void MultiProcess_Message_Utils_encodeHeader(long* header, int struct_id, long obj_id, BasicType elemType, long content_length, long content_msg_id) {

	Debug_assert(DEBUG_ALWAYS, header != NULL, "header == NULL\n");
	header[0] = (long)struct_id;
	header[1] = (long)obj_id;
	header[2] = (long)elemType;
	header[3] = (long)content_length;
	header[4] = (long)content_msg_id;
}

void MultiProcess_Message_Utils_resetHeader(long* header) {
	Debug_assert(DEBUG_ALWAYS, header != NULL, "header == NULL\n");
	header[0] = -1;
	header[1] = -1;
	header[2] = -1;
	header[3] = -1;
	header[4] = -1;
}

int MultiProcess_Message_Utils_header_structID(const long* const header) {
	Debug_assert(DEBUG_ALWAYS, header != NULL, "header == NULL\n");
	return (int)header[0];
}
long MultiProcess_Message_Utils_header_objID(const long* const header) {
	Debug_assert(DEBUG_ALWAYS, header != NULL, "header == NULL\n");
	return header[1];
}
BasicType MultiProcess_Message_Utils_header_contentElemType(const long* const header) {
	Debug_assert(DEBUG_ALWAYS, header != NULL, "header == NULL\n");
	return (BasicType)header[2];
}
int MultiProcess_Message_Utils_header_contentLength(const long* const header) {
	Debug_assert(DEBUG_ALWAYS, header != NULL, "header == NULL\n");
	return (int)header[3];
}
long MultiProcess_Message_Utils_header_contentMsgID(const long* const header) {
	Debug_assert(DEBUG_ALWAYS, header != NULL, "header == NULL\n");
	return header[4];
}

void MultiProcess_Message_Utils_header_printf(const long* const header, FILE* f) {
	fprintf(f, "Header: [struct_id=(int)%ld=%d, obj_id=%ld, elemType=%ld, "
		"content_length=%ld, content_msg_id=%ld]\n",
		header[0], (int)header[0],
		header[1], header[2], header[3], header[4]
	);
}
