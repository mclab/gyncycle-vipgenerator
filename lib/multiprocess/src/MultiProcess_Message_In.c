/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <mpi.h>

#include <Debug.h>
#include <Array.h>
#include <BasicTypeValue.h>

#include <MultiProcess_Utils.h>
#include <MultiProcess_Message_Utils.h>
#include <MultiProcess_Message_In.h>


#define DEBUG "MultiProcess_Message_In"

typedef enum { WAITING_FOR_HDR, WAITING_FOR_MSG, DONE } MessageStatus;

struct MultiProcess_Message_In {
	long header[MultiProcess_Message_HEADER_LENGTH];

	Array* msg;
	void* msg_C_array;

	int req_sender_id; 	// sender_id, as requested by receiver (might be 'any')
	int sender_id; 		// actual sender id
	int req_msg_tag; 	// tag, as requested by receiver (might be 'any')
	int msg_tag;		// actual msg tag
	MessageStatus status;

	MPI_Request transmissionRequestHeader;
	MPI_Request transmissionRequestMessage;
};

void _MultiProcess_Message_In_waitForHeader(MultiProcess_Message_In* const this);

const Array* MultiProcess_Message_In_msg(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	MultiProcess_Message_In_waitFor(this);
	return this->msg;
}
int MultiProcess_Message_In_requestedTag(const MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	return this->req_msg_tag;
}
int MultiProcess_Message_In_requestedSender(const MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	return this->req_sender_id;
}

int MultiProcess_Message_In_structID(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	_MultiProcess_Message_In_waitForHeader(this);
	Debug_assert(DEBUG_ALL, this->status != WAITING_FOR_HDR,
		"this->status == WAITING_FOR_HDR\n");
	return MultiProcess_Message_Utils_header_structID(this->header);
}
long MultiProcess_Message_In_objID(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	_MultiProcess_Message_In_waitForHeader(this);
	Debug_assert(DEBUG_ALL, this->status != WAITING_FOR_HDR,
		"this->status == WAITING_FOR_HDR\n");
	return MultiProcess_Message_Utils_header_objID(this->header);
}
int MultiProcess_Message_In_tag(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	_MultiProcess_Message_In_waitForHeader(this);
	Debug_assert(DEBUG_ALL, this->status != WAITING_FOR_HDR, "this->status == WAITING_FOR_HDR\n");
	return this->msg_tag;
}
int MultiProcess_Message_In_sender(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	_MultiProcess_Message_In_waitForHeader(this);
	Debug_assert(DEBUG_ALL, this->status != WAITING_FOR_HDR, "this->status == WAITING_FOR_HDR\n");
	return this->sender_id;
}


void MultiProcess_Message_In_fprint(MultiProcess_Message_In* const this, FILE* f) {
	fprintf(f, 	"MultiProcess_Message_In (status = %s):\n"
				" - allowed sender: %d\n"
				" - actual sender:  %d\n"
				" - allowed tag:    %d\n"
				" - actual tag:     %d\n"
				" - struct_id:      %d\n"
				" - obj id:         %ld\n",
		( (this->status == WAITING_FOR_HDR) ? "WAITING_FOR_HDR" :
			( (this->status == WAITING_FOR_MSG) ? "WAITING_FOR_MSG" :
				( (this->status == DONE) ? "DONE" : "UNKNOWN!!!" )
			)
		),
		this->req_sender_id,
		this->sender_id,
		this->req_msg_tag,
		this->msg_tag,
		MultiProcess_Message_Utils_header_structID(this->header),
		MultiProcess_Message_Utils_header_objID(this->header)
	);
}

void MultiProcess_Message_In_free(MultiProcess_Message_In** thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	MultiProcess_Message_In* this = *thisP;
	if (this == NULL) return;
	Array_free(&this->msg);
	free(this);
	*thisP = NULL;
}


/**
	WARNING: in case of multi-thread programs, this function must be called in a critical section.

	Returns a pointer to a new MultiProcess_Message_In object, representing a message recv request.
	Returns NULL in case of IO error.
*/
MultiProcess_Message_In* MultiProcess_Message_In_new(int req_sender_id, int req_msg_tag) {

	MultiProcess_Message_In* new = calloc(1, sizeof(MultiProcess_Message_In));
	Debug_assert(DEBUG, new != NULL, "new == NULL\n");

	Debug_out(DEBUG, "MultiProcess_Message_In_new(req_sender_id=%d, "
		"req_msg_tag=%d): start\n",
		req_sender_id, req_msg_tag);

	MultiProcess_Message_Utils_resetHeader(new->header);

	new->req_sender_id = req_sender_id;
	new->sender_id = -1;
	new->req_msg_tag = req_msg_tag;
	new->msg_tag = -1;
	new->msg = NULL;
	new->msg_C_array = NULL;

	new->status = 0;



	/** 1. Post non-blocking recv to receive header message with:
		- length of second (content) msg
		- elemType
		- unique msg id
	*/
	int return_code = MPI_Irecv(new->header,
		MultiProcess_Message_HEADER_LENGTH, MPI_LONG,
		req_sender_id, req_msg_tag, MPI_COMM_WORLD, &new->transmissionRequestHeader);

	new->status = WAITING_FOR_HDR;

	if (return_code != MPI_SUCCESS) {
		MultiProcess_Message_In_free(&new);
		return NULL;
	}
	else return new;
}

void _MultiProcess_Message_In_processHeaderArrived(MultiProcess_Message_In* const this, MPI_Status* mpi_status) {

		Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
		Debug_assert(DEBUG, this->status == WAITING_FOR_HDR,
			"this->status = %d != WAITING_FOR_HDR\n", this->status);
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_processHeaderArrived() start\n",
			MultiProcess_Utils_myID());

		BasicType elemType =
		 	MultiProcess_Message_Utils_header_contentElemType(this->header);
		long content_msg_id =
		 	MultiProcess_Message_Utils_header_contentMsgID(this->header);
		int msg_len = MultiProcess_Message_Utils_header_contentLength(this->header);

		this->msg_tag = mpi_status->MPI_TAG;
		this->sender_id = mpi_status->MPI_SOURCE;

		Debug_out(DEBUG, "_MultiProcess_Message_In_processHeaderArrived(): "
			"Received [msg_tag=%d, sender_id=%d] with header: \n",
			this->msg_tag, this->sender_id);
		Debug_perform(DEBUG,
			MultiProcess_Message_Utils_header_printf(this->header, stderr)
		);
		/*
			[elemType:%d, content_msg_id=%ld, "
			"msg_len=%d]. msg now has metadata [msg_tag=%d, sender_id=%d]:\n",
			elemType, content_msg_id, msg_len, this->msg_tag, this->sender_id);
		*/
		Debug_out(DEBUG, "_MultiProcess_Message_In_processHeaderArrived(): "
			"Msg now has the following metadata:\n");
		Debug_perform(DEBUG, MultiProcess_Message_In_fprint(this, stderr));

		if (msg_len == 0) {
			Debug_out(DEBUG, "[Process %d] "
				" _MultiProcess_Message_In_processHeaderArrived(): "
				"msg_len == 0 --> status DONE\n",
				MultiProcess_Utils_myID());
			this->status = DONE;
		} else {

			// prepare for msg reception
			Debug_assert(DEBUG_ALWAYS, this->msg_C_array == NULL,
				"this->msg_C_array != NULL\n");
			this->msg_C_array = calloc(msg_len, BasicType_sizeof(elemType));
			Debug_assert(DEBUG, this->msg_C_array != NULL, "this->msg_C_array == NULL\n");

			Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_processHeaderArrived(): "
				" posting recv request for message\n", MultiProcess_Utils_myID());
			int return_code = MPI_Irecv(this->msg_C_array, msg_len,
			 	MultiProcess_Utils_BasicType_to_MPI_Datatype(elemType),
				this->sender_id, content_msg_id, MPI_COMM_WORLD,
				&this->transmissionRequestMessage);
			Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_processHeaderArrived(): "
				"recv request for message posted\n", MultiProcess_Utils_myID());
			Debug_assert(DEBUG_ALWAYS, return_code == MPI_SUCCESS, "MPI Error %d\n",
			 	return_code);
			Debug_assert(DEBUG_ALWAYS, return_code == MPI_SUCCESS,
				"return_code == %d != MPI_SUCCESS\n", return_code);
			this->status = WAITING_FOR_MSG;
		} // end case msg_len != 0
}

int _MultiProcess_Message_In_isHeaderArrived(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived() start\n",
		MultiProcess_Utils_myID());

	if (this->status != WAITING_FOR_HDR) {
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived(): "
		"msg status is %d != WAITING_FOR_HDR. Returning 1. End.\n",
			MultiProcess_Utils_myID(), this->status);
		return 1;
	}

	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived(): "
		"Status WAITING_FOR_HDR: start checking header\n", MultiProcess_Utils_myID());
	// check whether header arrived
	int headerTransmitted = 0;
	MPI_Status mpi_status = {.MPI_TAG=0 };
	MPI_Test(&this->transmissionRequestHeader, &headerTransmitted, &mpi_status);
	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived(): "
		"check for header done: MPI_test returned %d\n",
		MultiProcess_Utils_myID(),
	 	headerTransmitted);
	if (headerTransmitted) {
		_MultiProcess_Message_In_processHeaderArrived(this, &mpi_status);
		return 1;
	}
	else return 0;
}

void _MultiProcess_Message_In_waitForHeader(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_waitForHeader() start\n",
		MultiProcess_Utils_myID());

	if (this->status != WAITING_FOR_HDR) {
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_waitForHeader(): "
		"msg status is %d != WAITING_FOR_HDR. Returning. End.\n",
			MultiProcess_Utils_myID(), this->status);
		return;
	}

	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_waitForHeader(): "
		"Status WAITING_FOR_HDR: start checking header\n", MultiProcess_Utils_myID());
	// check whether header arrived
	MPI_Status mpi_status = {.MPI_TAG=0 };
	MPI_Wait(&this->transmissionRequestHeader, &mpi_status);
	_MultiProcess_Message_In_processHeaderArrived(this, &mpi_status);
}



void _MultiProcess_Message_In_processContentMsgArrived(MultiProcess_Message_In* const this/*, MPI_Status* mpi_status*/) {

		Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
		Debug_assert(DEBUG, this->status == WAITING_FOR_MSG,
			"this->status = %d != WAITING_FOR_MSG\n", this->status);
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_"
			"processContentMsgArrived() start\n",
			MultiProcess_Utils_myID());

		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_"
			"processContentMsgArrived(): start wrapping up msg\n",
			MultiProcess_Utils_myID());

		BasicType elemType =
			MultiProcess_Message_Utils_header_contentElemType(this->header);
		int msg_len = MultiProcess_Message_Utils_header_contentLength(this->header);

		Debug_assert(DEBUG_ALL, this->msg_C_array != NULL, "this->msg_C_array == NULL\n");
		this->msg = Array_new_wrap(msg_len, BasicType_sizeof(elemType),
						this->msg_C_array, msg_len, NULL, NULL);
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_"
			"processContentMsgArrived(): wrapping up msg done\n",
			MultiProcess_Utils_myID());
		this->msg_C_array = NULL;
		this->status = DONE;

}


int _MultiProcess_Message_In_isContentMsgArrived(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isContentMsgArrived() start\n",
		MultiProcess_Utils_myID());

	if (this->status == WAITING_FOR_HDR) {
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived(): "
		"msg status is WAITING_FOR_HDR. Returning 0. End.\n",
			MultiProcess_Utils_myID());
		return 0;
	}
	if (this->status == DONE) {
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived(): "
		"msg status is DONE. Returning 1. End.\n",
			MultiProcess_Utils_myID());
		return 1;
	}

	Debug_assert(DEBUG, this->status == WAITING_FOR_MSG,
		"this->status == %d != WAITING_FOR_MSG\n", this->status);

	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived(): "
		"Status WAITING_FOR_MSG: start checking msg\n", MultiProcess_Utils_myID());

	int msgTransmitted = 0;
	MPI_Status mpi_status = {.MPI_TAG=0 };
	MPI_Test(&this->transmissionRequestMessage, &msgTransmitted, &mpi_status);
	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_isHeaderArrived(): "
		"check for content msg done: MPI_test returned %d\n",
		MultiProcess_Utils_myID(),
	 	msgTransmitted);
	if (msgTransmitted) {
		_MultiProcess_Message_In_processContentMsgArrived(this/*, &mpi_status*/);
		return 1;
	}
	else return 0;
}

void MultiProcess_Message_In_waitFor(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_waitForContentMsg() start\n",
		MultiProcess_Utils_myID());

	if (this->status == WAITING_FOR_HDR) {
		_MultiProcess_Message_In_waitForHeader(this);
	}
	if (this->status == DONE) {
		Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_waitForHeader(): "
		"msg status is DONE. Returning. End.\n",
			MultiProcess_Utils_myID());
		return;
	}

	Debug_assert(DEBUG, this->status == WAITING_FOR_MSG, "[Process %d] "
		"this->status = %d != WAITING_FOR_MSG\n",
		MultiProcess_Utils_myID(), this->status);

	Debug_out(DEBUG, "[Process %d] _MultiProcess_Message_In_waitForContentMsg(): "
		"Status WAITING_FOR_MSG: start checking content msg\n", MultiProcess_Utils_myID());
	// check whether content msg arrived
	MPI_Status mpi_status = {.MPI_TAG=0 };
	MPI_Wait(&this->transmissionRequestMessage, &mpi_status);
	_MultiProcess_Message_In_processContentMsgArrived(this/*, &mpi_status*/);
}



int MultiProcess_Message_In_isReceived(MultiProcess_Message_In* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");

	Debug_out(DEBUG, "[Process %d] MultiProcess_Message_In_isReceived() start\n",
	 	MultiProcess_Utils_myID());

	if (this->status == DONE) {
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_In_isReceived() end. "
			"Status: DONE --> result = 1\n", MultiProcess_Utils_myID());
		return 1;
	}

	if (this->status == WAITING_FOR_HDR) {
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_In_isReceived(): "
			"Status: WAITING_FOR_HDR.\n",
			MultiProcess_Utils_myID());

		if (!_MultiProcess_Message_In_isHeaderArrived(this)) return 0;
	}

	if (this->status == WAITING_FOR_MSG) {
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_In_isReceived(): "
			"Status: WAITING_FOR_HDR.\n",
			MultiProcess_Utils_myID());

		if (!_MultiProcess_Message_In_isContentMsgArrived(this)) return 0;
	}
	return (this->status == DONE);
}






