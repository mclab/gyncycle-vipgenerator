#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <Debug.h>
#include <Array.h>
#include <BasicTypeValue.h>
#include <RndGen.h>

#include <MultiProcess_Utils.h>
#include <MultiProcess_Message_Out.h>
#include <MultiProcess_Message_In.h>

#include <mpi.h>

#define MASTER 0

#define M2S 1
#define S2M 2

void master(int this_id, int nb_processes, const char* host_name);
void slave(int this_id, int nb_processes, const char* host_name);


static int STRING_STRUCT_ID = 10;


static RndGen* rnd;

void waitAbit() {
	int seconds = (int)RndGen_nextUL(rnd, 2, 8);
	sleep(seconds);
}


int main(int argc, char** args) {
	
	Debug_setDefaultOut(0);
	//Debug_enable("MultiProcess_Message_Out");
	//Debug_enable("MultiProcess_Message_In");	
	Debug_enable("MultiProcess_Utils");
	
	rnd = RndGen_new();
	
	MultiProcess_Utils_Start(&argc, &args);
	
	Debug_assert(DEBUG_ALWAYS, MultiProcess_Utils_nbProcesses() >= 2, 
		"nb_processes = %d < 2\n", MultiProcess_Utils_nbProcesses());
	
	//MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

	sleep(8);

	int max_string_len = 100;
	char* string = calloc(max_string_len, sizeof(char));
	
	if (MultiProcess_Utils_myID() == MASTER) {
		// MASTER
		master(MultiProcess_Utils_myID(), MultiProcess_Utils_nbProcesses(), MultiProcess_Utils_myHostName());
	}
	
	else {
		// SLAVE this_id
		slave(MultiProcess_Utils_myID(), MultiProcess_Utils_nbProcesses(), MultiProcess_Utils_myHostName());
	}	
		
		
	MultiProcess_Utils_End();
	return 0;
}






void master(int this_id, int nb_processes, const char* host_name) {
	Debug_assert(DEBUG_ALL, this_id == 0, "this_id != 0\n");
	int max_string_len = 100;	

	fprintf(stderr, "[process %d on '%s'] Hello! I am process %d out of %d.\n", 
		this_id, host_name, this_id, nb_processes);

	fprintf(stderr, "[MASTER] ====== Starting SEND phase =======\n");

	Array* m2s_msgs = Array_new(nb_processes, sizeof(MultiProcess_Message_Out*), 
		nb_processes, NULL, NULL);

	for(int recipient = MASTER+1; recipient < nb_processes; recipient++) {
		// build message for 'recipient'
		char* string = calloc(max_string_len, sizeof(char));
		sprintf(string, "Hello %d", recipient);
		int msg_len = strlen(string)+1;
		Array* msg = Array_new_wrap(msg_len, sizeof(char), string, msg_len, NULL, NULL);
		
		MultiProcess_Message_Out* m2s = MultiProcess_Message_Out_new(M2S, STRING_STRUCT_ID, 0, 
			msg, BASIC_TYPE_CHAR, recipient);			
		Array_add(m2s_msgs, &m2s);
		fprintf(stderr, "[MASTER] send requested for message to slave %d\n", recipient);
	}

	int all_sent = 0;
	do {
		fprintf(stderr, "[MASTER] sleeping a little bit\n");
		waitAbit();
		all_sent = 1;
		MultiProcess_Message_Out* m2s = NULL;			
		for(int i=0; i < Array_length(m2s_msgs); i++) {
			fprintf(stderr, "[MASTER] getting array elem\n");
			Array_get(m2s_msgs, i, &m2s);
			fprintf(stderr, "[MASTER] getting array elem --> done\n");			
			int this_sent = MultiProcess_Message_Out_isSent(m2s);
			fprintf(stderr, "[MASTER] message to slave %d sent? %s\n", i+1,
				(this_sent ? "YES" : "NO")
			);
			if (!this_sent) all_sent = 0;
		}
	} while (!all_sent);
	fprintf(stderr, "[MASTER] messages have been sent to all slaves\n");
		
	fprintf(stderr, "[MASTER]  ====== Starting RECEIVE phase ====== \n"); 
	Array* s2m_msgs = Array_new(nb_processes, sizeof(MultiProcess_Message_In*), 
		nb_processes, NULL, NULL);		
	for(int source = MASTER+1; source < nb_processes; source++) {
		MultiProcess_Message_In* s2m = MultiProcess_Message_In_new(source, S2M);
		fprintf(stderr, "[MASTER] recv requested for message from slave %d\n", source);
		Array_add(s2m_msgs, &s2m);
	}
	
	int all_recv = 0;
	do {
		fprintf(stderr, "[MASTER] sleeping a little bit\n");
		waitAbit();
		all_recv = 1;
		MultiProcess_Message_In* s2m = NULL;			
		for(int i=0; i < Array_length(s2m_msgs); i++) {
			Array_get(s2m_msgs, i, &s2m);
			int this_recv = MultiProcess_Message_In_isReceived(s2m);
			fprintf(stderr, "[MASTER] message from slave %d received? %s\n", i+1,
				(this_recv ? "YES" : "NO")
			);
			if (!this_recv) all_recv = 0;
		}
	} while (!all_recv);
	fprintf(stderr, "[MASTER] the following messages have been received from all slaves:\n");
		
	for(int i=0; i < Array_length(s2m_msgs); i++) {
		MultiProcess_Message_In* s2m = NULL;
		Array_get(s2m_msgs, i, &s2m);
		fprintf(stderr, " - From slave %d (msg tag: %d, sender: %d): '%s'\n", i+1, 
			MultiProcess_Message_In_tag(s2m),
			MultiProcess_Message_In_sender(s2m),			
			Array_as_C_array(MultiProcess_Message_In_msg(s2m))
		);
	}
	fprintf(stderr, "[MASTER] done.\n");	
}







void slave(int this_id, int nb_processes, const char* host_name) {
	Debug_assert(DEBUG_ALL, this_id > 0, "this_id <= 0\n");
	Debug_assert(DEBUG_ALL, this_id < nb_processes, "this_id >= %d = nb_processes\n", nb_processes);
	
	int max_string_len = 100;
	
	
	fprintf(stderr, "[process %d on '%s'] Hello! I am process %d out of %d.\n", 
		this_id, host_name, this_id, nb_processes);

	fprintf(stderr, "[SLAVE %d] ====== Starting RECEIVE phase =======\n",
	 		this_id); 

	MultiProcess_Message_In* m2s = MultiProcess_Message_In_new(MASTER, M2S);
	fprintf(stderr, "[SLAVE %d] recv requested for message from master\n", this_id);
	
	int recv = 0;
	do {
		fprintf(stderr, "[SLAVE %d] sleeping a little bit\n", this_id);
		waitAbit();
		recv = MultiProcess_Message_In_isReceived(m2s);
		fprintf(stderr, "[SLAVE %d] message from master not yet received\n", this_id);
	} while (!recv);
	fprintf(stderr, "[SLAVE %d] message from master received (tag: %d, sender: %d): '%s'\n", 
		MultiProcess_Message_In_tag(m2s),
		MultiProcess_Message_In_sender(m2s),
		this_id,
		Array_as_C_array(MultiProcess_Message_In_msg(m2s))
	);
	fprintf(stderr, "[SLAVE %d]  ====== Starting SEND phase ====== \n", this_id); 

	char* string = calloc(max_string_len, sizeof(char));
	sprintf(string, "Reply from %d", this_id);
	int msg_len = strlen(string)+1;
	Array* msg = Array_new_wrap(msg_len, sizeof(char), string, msg_len, NULL, NULL);
	MultiProcess_Message_Out* s2m = MultiProcess_Message_Out_new(S2M, STRING_STRUCT_ID, 1, msg, BASIC_TYPE_CHAR, MASTER);			

	fprintf(stderr, "[SLAVE %d] send requested for message to master\n", this_id);

	int sent = 0;
	do {
		fprintf(stderr, "[SLAVE %d] sleeping a little bit\n", this_id);
		waitAbit();
		sent = 1;
		sent = MultiProcess_Message_Out_isSent(s2m);
		fprintf(stderr, "[SLAVE %d] message to master sent? %s\n", this_id,
			(sent ? "YES" : "NO")
		);
	} while (!sent);	
	fprintf(stderr, "[SLAVE %d] Done\n", this_id);
}

