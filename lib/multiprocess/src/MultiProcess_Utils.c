/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <mpi.h>

#include <Debug.h>
#include <BasicTypeValue.h>
#include <HashTable.h>
#include <Array.h>

#include <MultiProcess_Utils.h>
#include <MultiProcess_Message_Out.h>
#include <MultiProcess_Message_In.h>

#define MAX_HOST_NAME_LEN 1025

static int initialized = 0;
static int my_id = -1;
static int nb_processes = 0;
static char host_name[MAX_HOST_NAME_LEN] = {0};
static HashTable* toolkit = NULL;

typedef struct {
	int struct_id;
	const Array* (*serialize_f)(void* const _this);
	void* (*new_fromSerialized_f)(const Array* const serialized);
	BasicType arrayElemType;
} StructTransmissionToolkit;


MPI_Datatype MultiProcess_Utils_BasicType_to_MPI_Datatype(BasicType t) {
	switch(t) {
		case BASIC_TYPE_INT : return MPI_INT;
		case BASIC_TYPE_LONG : return MPI_LONG;
		case BASIC_TYPE_ULONG : return MPI_UNSIGNED_LONG;
		case BASIC_TYPE_ULONGLONG : return MPI_UNSIGNED_LONG_LONG;
		case BASIC_TYPE_DOUBLE : return MPI_DOUBLE;
		case BASIC_TYPE_CHAR : return MPI_CHAR;
	}
	Debug_assert(DEBUG_ALWAYS, 0, "Unknown BasicType '%d'\n", t);
	return MPI_INT;
}


int MultiProcess_Utils_myID() {
	Debug_assert(DEBUG_ALWAYS, initialized, "Not initialized\n");
	return my_id;
}
const char* MultiProcess_Utils_myHostName() {
	Debug_assert(DEBUG_ALWAYS, initialized, "Not initialized\n");
	return host_name;
}

int MultiProcess_Utils_nbProcesses() {
	Debug_assert(DEBUG_ALWAYS, initialized, "Not initialized\n");
	return nb_processes;
}



StructTransmissionToolkit* StructTransmissionToolkit_new(
	int struct_id,
	const Array* (*serialize_f)(void* const _this),
	void* (*new_fromSerialized_f)(const Array* const serialized),
	BasicType arrayElemType
) {
	StructTransmissionToolkit* new = calloc(1, sizeof(StructTransmissionToolkit));
	Debug_assert(DEBUG_ALWAYS, new != NULL, "new == NULL\n");
	new->struct_id = struct_id;
	new->serialize_f = serialize_f;
	new->new_fromSerialized_f = new_fromSerialized_f;
	new->arrayElemType = arrayElemType;
	return new;
}
void StructTransmissionToolkit_free(StructTransmissionToolkit** thisP) {
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL\n");
	StructTransmissionToolkit* this = *thisP;
	if (this == NULL) return;
	free(this);
	*thisP = NULL;
}


void MultiProcess_Utils_registerTransmissionEnabledStruct(
	int struct_id,
	const Array* (*serialize_f)(void* const _this),
	void* (*new_fromSerialized_f)(const Array* const serialized),
	BasicType arrayElemType
) {
	Debug_assert(DEBUG_ALWAYS, initialized, "You must first invoke MultiProcess_Utils_Start()\n");
	StructTransmissionToolkit* structToolkit = NULL;
	HashTable_get(toolkit, &struct_id, sizeof(int), (void**)&structToolkit);
	Debug_assert(DEBUG_ALWAYS, structToolkit == NULL,
		"Struct with id %d has already been registered\n", struct_id);

	structToolkit =
		StructTransmissionToolkit_new(struct_id, serialize_f, new_fromSerialized_f, arrayElemType);

	HashTable_put(toolkit, &struct_id, sizeof(int), structToolkit);
}

MultiProcess_Message_Out* MultiProcess_Utils_new_Obj_Message_Out(
	int msg_tag,
	int struct_id, long obj_id,
	void* const obj,
	int recipient_id) {
		Debug_assert(DEBUG_ALWAYS, initialized, "You must first invoke MultiProcess_Utils_Start()\n");
		StructTransmissionToolkit* structToolkit = NULL;
		HashTable_get(toolkit, &struct_id, sizeof(int), (void**)&structToolkit);
		Debug_assert(DEBUG_ALWAYS, structToolkit != NULL, "struct with id %d not registered\n",
		 	struct_id);
		const Array* serialized_obj = structToolkit->serialize_f(obj);
		return MultiProcess_Message_Out_new(msg_tag, struct_id, obj_id, serialized_obj,
			structToolkit->arrayElemType, recipient_id);
}


void* MultiProcess_Utils_new_Obj_from_Message_In(MultiProcess_Message_In* const msg_in) {
	Debug_assert(DEBUG_ALWAYS, msg_in != NULL, "msg_in == NULL\n");
	MultiProcess_Message_In_waitFor(msg_in);

	Debug_assert(DEBUG_ALWAYS, initialized, "You must first invoke MultiProcess_Utils_Start()\n");
	int struct_id = MultiProcess_Message_In_structID(msg_in);

	StructTransmissionToolkit* structToolkit = NULL;
	HashTable_get(toolkit, &struct_id, sizeof(int), (void**)&structToolkit);
	if (structToolkit == NULL) {
		Debug_out(DEBUG_ALWAYS, "struct with id %d not registered. "
			"Process id = %d, msg_in =\n",
	 		struct_id, MultiProcess_Utils_myID());
		Debug_perform(DEBUG_ALWAYS, MultiProcess_Message_In_fprint(msg_in, stderr));
		Debug_assert(DEBUG_ALWAYS, 0, "structToolkit == NULL\n");
	}


	const Array* const serialized_obj = MultiProcess_Message_In_msg(msg_in);
	return structToolkit->new_fromSerialized_f(serialized_obj);
}

void* MultiProcess_Utils_new_Obj(int sender_id, int req_msg_tag) {
	MultiProcess_Message_In* msg_in = MultiProcess_Message_In_new(sender_id, req_msg_tag);
	void* result = MultiProcess_Utils_new_Obj_from_Message_In(msg_in);
	MultiProcess_Message_In_free(&msg_in);
	return result;
}


void MultiProcess_Utils_Barrier() {
	Debug_assert(DEBUG_ALWAYS, initialized, "Not initialized\n");
  MPI_Barrier(MPI_COMM_WORLD);
}

void MultiProcess_Utils_Start(int* argc_p, char*** args_p) {
	MPI_Init(argc_p, args_p);
	MPI_Comm_rank( MPI_COMM_WORLD, &my_id);
	MPI_Comm_size( MPI_COMM_WORLD, &nb_processes);
	int host_name_len = 0;
	MPI_Get_processor_name(host_name, &host_name_len);
	toolkit = HashTable_new(sizeof(int));
	initialized = 1;
}

void MultiProcess_Utils_End() {
	Debug_assert(DEBUG_ALWAYS, initialized, "Not initialized\n");
	MPI_Finalize();

	HashTableIterator* it = HashTableIterator_new(toolkit);
	HashTable_Entry* entry = NULL;
	while ( (entry = HashTableIterator_next(it)) != NULL ) {
		StructTransmissionToolkit* structToolkit = HashTable_Entry_value(entry);
		StructTransmissionToolkit_free(&structToolkit);
	}
	HashTableIterator_free(&it);
	HashTable_free(&toolkit);
	initialized = 0;
}

