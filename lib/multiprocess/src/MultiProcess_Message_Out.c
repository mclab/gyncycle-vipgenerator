/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <mpi.h>

#include <Debug.h>
#include <Array.h>
#include <BasicTypeValue.h>

#include <MultiProcess_Utils.h>
#include <MultiProcess_Message_Utils.h>

#include <MultiProcess_Message_Out.h>


#define DEBUG "MultiProcess_Message_Out"


typedef enum { SEND_REQ_POSTED, DONE } MessageStatus;

struct MultiProcess_Message_Out {
	long header[MultiProcess_Message_HEADER_LENGTH];
	const Array* msg; // Array of header->elemType, can be null

	int recipient_id;
	int msg_tag;
	MessageStatus status;

	MPI_Request transmissionRequestHeader;
	MPI_Request transmissionRequestMessage;
};

const Array* MultiProcess_Message_Out_msg(const MultiProcess_Message_Out* const this) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	return this->msg;
}

int MultiProcess_Message_Out_recipient(const MultiProcess_Message_Out* const this) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	return this->recipient_id;
}

int MultiProcess_Message_Out_structID(const MultiProcess_Message_Out* const this) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	return MultiProcess_Message_Utils_header_structID(this->header);
}

long MultiProcess_Message_Out_objID(const MultiProcess_Message_Out* const this) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	return MultiProcess_Message_Utils_header_objID(this->header);
}
int MultiProcess_Message_Out_tag(const MultiProcess_Message_Out* const this) {
	Debug_assert(DEBUG, this != NULL, "this == NULL\n");
	return this->msg_tag;
}



void MultiProcess_Message_Out_fprint(const MultiProcess_Message_Out* const this, FILE* f) {
	fprintf(f, 	"MultiProcess_Message_Out (status = %s):\n"
				" - recipient: %d\n"
				" - tag:       %d\n"
				" - struct_id: %d\n"
				" - obj id:    %ld\n",
		( (this->status == SEND_REQ_POSTED) ? "SEND_REQ_POSTED" :
			( (this->status == DONE) ? "DONE" : "UNKNOWN!!!" )
		),
		MultiProcess_Message_Out_recipient(this),
		MultiProcess_Message_Out_tag(this),
		MultiProcess_Message_Out_structID(this),
		MultiProcess_Message_Out_objID(this)
	);
}


void MultiProcess_Message_Out_free(MultiProcess_Message_Out** thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	MultiProcess_Message_Out* this = *thisP;
	if (this == NULL) return;
	free(this);
	*thisP = NULL;
}

#define MAX_UNIQUE_ID (32767+1) // 32767 is guaranteed by the standard
#define MIN_UNIQUE_ID ((MAX_UNIQUE_ID / 2) + 1)
static int next_unique_id = MIN_UNIQUE_ID;
static int _next_unique_id() {
	int result = next_unique_id;
	if (next_unique_id < MAX_UNIQUE_ID) next_unique_id++;
	else next_unique_id = MIN_UNIQUE_ID;
	return result;
}

static int _isUserTagGood(int tag) {
	return tag >= 0 && tag < MIN_UNIQUE_ID;
}


MultiProcess_Message_Out* MultiProcess_Message_Out_new(int msg_tag, int struct_id, long obj_id, const Array* const msg, BasicType elemType, int recipient_id) {

	Debug_assert(DEBUG, _isUserTagGood(msg_tag), "msg tag %d reserved for unique ids\n", msg_tag);

	Debug_out(DEBUG, "MultiProcess_Message_Out_new(sender=%d, msg_tag=%d, "
		"struct_id=%d, obj_id=%ld, recipient_id=%d)\n",
		MultiProcess_Utils_myID(), msg_tag, struct_id, obj_id, recipient_id);

	MultiProcess_Message_Out* new = calloc(1, sizeof(MultiProcess_Message_Out));
	Debug_assert(DEBUG, new != NULL, "new == NULL\n");


	long msg_len = (msg == NULL ? 0 : Array_length(msg));
	long msg_id = _next_unique_id();
	MultiProcess_Message_Utils_encodeHeader(new->header, struct_id, obj_id, elemType, msg_len, msg_id);
	new->msg = msg; // can be null
	new->recipient_id = recipient_id;
	new->msg_tag = msg_tag;

	Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_new(): sending header\n",
		MultiProcess_Utils_myID());
	Debug_perform(DEBUG,
		MultiProcess_Message_Utils_header_printf(new->header, stderr)
	);
	int return_code = MPI_Isend(new->header, MultiProcess_Message_HEADER_LENGTH,
		MPI_LONG, new->recipient_id, msg_tag,
		MPI_COMM_WORLD, &new->transmissionRequestHeader);
	Debug_assert(DEBUG_ALWAYS, return_code == MPI_SUCCESS, "MPI Error %d while sending header\n",
	 	return_code);
	if (return_code != MPI_SUCCESS) {
		MultiProcess_Message_Out_free(&new);
		return NULL;
	}

	if (msg_len > 0) {
		// 2. Send content message (with unique id 'msg_id')
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_new(): sending message\n",
			MultiProcess_Utils_myID());
		return_code = MPI_Isend(
			(void*)Array_as_C_array(new->msg), (int) msg_len,
			MultiProcess_Utils_BasicType_to_MPI_Datatype(elemType),
			recipient_id, (int) msg_id, MPI_COMM_WORLD, &new->transmissionRequestMessage
		);
		Debug_assert(DEBUG_ALWAYS, return_code == MPI_SUCCESS, "MPI Error %d while sending "
			"content message\n", return_code);
		if (return_code != MPI_SUCCESS) {
			MultiProcess_Message_Out_free(&new);
			return NULL;
		}
	}

	new->status = SEND_REQ_POSTED;
	return new;
}

void MultiProcess_Message_Out_waitFor(MultiProcess_Message_Out* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	if (this->status == DONE) return;

	int msg_len = MultiProcess_Message_Utils_header_contentLength(this->header);
	if (msg_len == 0) {
		MPI_Wait(&this->transmissionRequestHeader, MPI_STATUS_IGNORE);
	} else {
		MPI_Wait(&this->transmissionRequestMessage, MPI_STATUS_IGNORE);
	}
}


int MultiProcess_Message_Out_isSent(MultiProcess_Message_Out* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent() start\n", MultiProcess_Utils_myID());

	if (this->status == DONE) {
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent() end. "
			"Status DONE --> result = 1\n", MultiProcess_Utils_myID());
		return 1;
	}
	Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent() status SEND_REQ_POSTED\n",
		MultiProcess_Utils_myID());

	Debug_assert(DEBUG_ALWAYS, this->status == SEND_REQ_POSTED,
		"this->status = %d != SEND_REQ_POSTED\n", this->status);

	int headerTransmitted = 0;
	Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent(): start checking for header\n",
	 	MultiProcess_Utils_myID());
	MPI_Test(&this->transmissionRequestHeader, &headerTransmitted, MPI_STATUS_IGNORE);
	Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent(): check for header done: %d\n",
	 	MultiProcess_Utils_myID(), headerTransmitted);

	if (!headerTransmitted) {
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent() end. "
			"!headerTransmitted --> result = 0\n", MultiProcess_Utils_myID());
		return 0;
	}

	int msg_len = MultiProcess_Message_Utils_header_contentLength(this->header);

	int result = 0;

	if (msg_len == 0) {
		result = 1;
	} else {
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent(): start checking for message\n",
		 	MultiProcess_Utils_myID());
		MPI_Test(&this->transmissionRequestMessage, &result, MPI_STATUS_IGNORE);
		Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent(): check for message done: %d\n",
		 	MultiProcess_Utils_myID(), result);
	}

	if (result) {
		this->status = DONE;
	}
	Debug_out(DEBUG, "[Process %d] MultiProcess_Message_Out_isSent() end. "
		"result = %d\n", MultiProcess_Utils_myID(), result);
	return result;
}



