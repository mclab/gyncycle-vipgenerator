/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __MultiProcess_Message_Utils_h__
#define __MultiProcess_Message_Utils_h__

#include <BasicTypeValue.h>

#define MultiProcess_Message_HEADER_LENGTH 5

void MultiProcess_Message_Utils_encodeHeader(long* header, int struct_id, long obj_id, BasicType elemType,
	long content_length, long content_msg_id);

void MultiProcess_Message_Utils_resetHeader(long* header);

int MultiProcess_Message_Utils_header_structID(const long* const header);
long MultiProcess_Message_Utils_header_objID(const long* const header);

BasicType MultiProcess_Message_Utils_header_contentElemType(const long* const header);

int MultiProcess_Message_Utils_header_contentLength(const long* const header);

long MultiProcess_Message_Utils_header_contentMsgID(const long* const header);

void MultiProcess_Message_Utils_header_printf(const long* const header, FILE* f);

#endif
