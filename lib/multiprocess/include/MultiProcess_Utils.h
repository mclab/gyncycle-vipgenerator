/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __MultiProcess_Utils_h__
#define __MultiProcess_Utils_h__

#include <mpi.h>
#include <BasicTypeValue.h>
#include <Array.h>
#include <MultiProcess_Message_Out.h>
#include <MultiProcess_Message_In.h>


#define MultiProcess_Utils_ANY_TAG (MPI_ANY_TAG)

MPI_Datatype MultiProcess_Utils_BasicType_to_MPI_Datatype(BasicType t);

void MultiProcess_Utils_Start(int* argc_p, char*** args_p);
void MultiProcess_Utils_Barrier();
void MultiProcess_Utils_End();
int MultiProcess_Utils_myID();
const char* MultiProcess_Utils_myHostName();
int MultiProcess_Utils_nbProcesses();



void MultiProcess_Utils_registerTransmissionEnabledStruct(
	int struct_id,
	const Array* (*serialize_f)(void* const _this),
	void* (*new_fromSerialized_f)(const Array* const serialized),
	BasicType arrayElemType
);

MultiProcess_Message_Out* MultiProcess_Utils_new_Obj_Message_Out(
	int msg_tag,
	int struct_id, long obj_id,
	void* const obj,
	int recipient_id
);

/** Reads msg_in and creates an object from it */
void* MultiProcess_Utils_new_Obj_from_Message_In(MultiProcess_Message_In* const msg_in);

/** Receives a new object from sender_id, with a message of tag req_msg_tab */
void* MultiProcess_Utils_new_Obj(int sender_id, int req_msg_tag);

#endif
