/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __MultiProcess_Message_In_h__
#define __MultiProcess_Message_In_h__

#include <stdio.h>

typedef struct MultiProcess_Message_In MultiProcess_Message_In;

const Array* MultiProcess_Message_In_msg(MultiProcess_Message_In* const _this);
int MultiProcess_Message_In_requestedTag(const MultiProcess_Message_In* const _this);
int MultiProcess_Message_In_requestedSender(const MultiProcess_Message_In* const _this);
int MultiProcess_Message_In_structID(MultiProcess_Message_In* const _this);
long MultiProcess_Message_In_objID(MultiProcess_Message_In* const _this);
int MultiProcess_Message_In_tag( MultiProcess_Message_In* const _this);
int MultiProcess_Message_In_sender(MultiProcess_Message_In* const _this);

void MultiProcess_Message_In_free(MultiProcess_Message_In** thisP);

/**
	WARNING: in case of multi-thread programs, this function must be called in a critical section
*/
MultiProcess_Message_In* MultiProcess_Message_In_new(int sender_id, int req_msg_tag);

void MultiProcess_Message_In_waitFor(MultiProcess_Message_In* const _this);

int MultiProcess_Message_In_isReceived(MultiProcess_Message_In* const _this);

void MultiProcess_Message_In_fprint(MultiProcess_Message_In* const this, FILE* f);

#endif
