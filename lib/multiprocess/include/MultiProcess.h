/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __MULTIPROCESS__
#define __MULTIPROCESS__

#include <mpi.h>

#include <MCLabUtils.h>

#include <MultiProcess_Message_In.h>
#include <MultiProcess_Message_Out.h>
#include <MultiProcess_Message_Utils.h>

#endif
