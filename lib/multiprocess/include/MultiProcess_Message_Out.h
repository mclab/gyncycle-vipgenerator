/*#    This file is part of MultiProcess
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MultiProcess is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MultiProcess is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MultiProcess.
#    If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __MultiProcess_Message_Out_H__
#define __MultiProcess_Message_Out_H__

#include <Array.h>
#include <BasicTypeValue.h>


typedef struct MultiProcess_Message_Out MultiProcess_Message_Out;


const Array* MultiProcess_Message_Out_msg(const MultiProcess_Message_Out* const _this);
int MultiProcess_Message_Out_recipient(const MultiProcess_Message_Out* const _this);

int MultiProcess_Message_Out_structID(const MultiProcess_Message_Out* const _this);
long MultiProcess_Message_Out_objID(const MultiProcess_Message_Out* const _this);
int MultiProcess_Message_Out_tag(const MultiProcess_Message_Out* const _this);

void MultiProcess_Message_Out_free(MultiProcess_Message_Out** thisP);

void MultiProcess_Message_Out_fprint(const MultiProcess_Message_Out* const _this, FILE* f);

/**
	WARNING: in case of multi-thread programs, this function must be called in a critical section
*/
MultiProcess_Message_Out* MultiProcess_Message_Out_new(int msg_tag, int struct_id, long obj_id, const Array* const msg, BasicType elemType, int recipient_id);


int MultiProcess_Message_Out_isSent(MultiProcess_Message_Out* const _this);

void MultiProcess_Message_Out_waitFor(MultiProcess_Message_Out* const _this);

#endif

