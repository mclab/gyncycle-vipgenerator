/*
#    This file is part of RealVectorSpace
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    RealVectorSpace is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    RealVectorSpace is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RealVectorSpace.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <RealVector.h>

struct RealVector {
	unsigned int dim;
	Array* values; // of double
};

const double* RealVector_as_C_array(const RealVector* const this) {
	return (const double*)Array_as_C_array(this->values);
}

Array const *RealVector_as_Array(const RealVector* const this) {
	return (Array const *) this->values;
}

RealVector* RealVector_new(unsigned int dim) {
	RealVector* new = calloc(1, sizeof(RealVector));
	Debug_assert(DEBUG_ALL, new != NULL, "new == NULL");
	new->dim = dim;
	new->values = Array_newDouble(dim, dim);
	double zero = 0;
	for(unsigned int i=0; i<dim; i++) {
		Array_add(new->values, &zero);
	}
	return new;
}

// coords is an Array of double
RealVector* RealVector_newFromArray(Array const *coords) {
	unsigned int dim = Array_length(coords);
	RealVector* new = RealVector_new(dim);
	double value = 0;
	for(unsigned int i=0; i<dim; i++) {
		Array_get(coords, i, &value);
		RealVector_set(new, i, value);
	}
	return new;
}

unsigned int RealVector_dim(const RealVector* const this) {
	return this->dim;
}

double RealVector_get(const RealVector* const this, unsigned int i) {
	double result = 0;
	Array_get(this->values, i, &result);
	return result;
}
void RealVector_set(const RealVector* this, unsigned int i, double value) {
	Debug_assert(DEBUG_ALL, i < this->dim, "i == %d >= %d = dim\n", i, this->dim);
	Array_set(this->values, i, &value);
}


void RealVector_copy(const RealVector* const this, RealVector* dest) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALL, dest != NULL, "result == NULL\n");
	Debug_assert(DEBUG_ALL, this->dim == dest->dim, "this->dim = %d != result->dim = %d\n",
			this->dim, dest->dim);
	Array_copy(this->values, dest->values);
}

RealVector* RealVector_clone(const RealVector* const this) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	RealVector* new = calloc(1, sizeof(RealVector));
	Debug_assert(DEBUG_ALL, new != NULL, "new == NULL");
	new->dim = this->dim;
	new->values = Array_clone(this->values);
	return new;
}



void RealVector_free(RealVector** thisP) {
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL\n");
	RealVector* this = *thisP;
	Array_free( & (this->values) );
	free(this);
	*thisP = NULL;
}

int RealVector_equals(const RealVector* const this, const RealVector* const other) {
	if (this == NULL) return other == NULL;
	if (other == NULL) return 0;
	return this->dim == other->dim && Array_equals(this->values, other->values);
}

void RealVector_fprint(const RealVector* const this, FILE* f) {
	Array_fprint(this->values, f);
}







double RealVector_euclideanDistance(RealVector* this, RealVector* other) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
	Debug_assert(DEBUG_ALL, other != NULL, "other == NULL");
	Debug_assert(DEBUG_ALL, this->dim == this->dim, "this->dim != other->dim");

	double result = 0;
	for (unsigned int i = 0; i < this->dim; i++) {
		result += pow( RealVector_get(other, i) - RealVector_get(this, i), 2 );
	}
	return sqrt(result);
}


