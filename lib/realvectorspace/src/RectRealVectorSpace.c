/*
#    This file is part of RealVectorSpace
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    RealVectorSpace is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    RealVectorSpace is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RealVectorSpace.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <RealVector.h>
#include <RectRealVectorSpace.h>


struct RectRealVectorSpace {
	unsigned int dim;
	RealVector* lower;
	RealVector* upper;
	Array* nonEmptyDimensions; // of int
	Array* discrSteps; // of int, can be NULL
	void* env; // environment data
	void (*free_env)(void**);
};

bool RectRealVectorSpace_belongsTo(RectRealVectorSpace const *space, RealVector const *v) {
  Debug_assert(DEBUG_ALWAYS, space != NULL, "space == NULL\n");
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  Debug_assert(DEBUG_ALWAYS, RealVector_dim(v) == space->dim, "Dim of v must be equal to %u\n", space->dim);
  for (unsigned int i = 0; i < space->dim; i++) {
    double l = RealVector_get(space->lower, i);
    double u = RealVector_get(space->upper, i);
    double v_i = RealVector_get(v, i);
    if (Numbers_approxG(v_i, u) || Numbers_approxL(v_i, l)) return false;
  }
  return true;
}


#define DEBUG_RVS_DISCR "RectRealVectorSpace discr"
int RectRealVectorSpace_discretisedRealVectorIndexes(RectRealVectorSpace const *space, RealVector const *v, Array *a) {
  Debug_assert(DEBUG_ALWAYS, space != NULL, "space == NULL\n");
  Debug_assert(DEBUG_ALWAYS, v != NULL, "v == NULL\n");
  Debug_assert(DEBUG_ALWAYS, RealVector_dim(v) == space->dim, "Dim of v must be equal to %u\n", space->dim);
  Debug_assert(DEBUG_ALWAYS, RealVector_dim(v) == Array_length(a), "Dim of v %u must be equal to len of a %u\n", space->dim, Array_length(a));
  if (space->discrSteps == NULL) return -1;
  unsigned int dim = (unsigned int) space->dim;
  int index = 0;
  double u = NAN;
  double l = NAN;
  int n = -1;
  double s = NAN;
  double val = NAN;
  for (unsigned int i = 0; i < dim; i++) {
    val = RealVector_get(v, i);
    l = RealVector_get(space->lower, i);
    u = RealVector_get(space->upper , i);
    if ( Numbers_approxLE(val, l)) {
      index = 0;
      Debug_out(DEBUG_RVS_DISCR, "Value: %g in [%g, %g]. Nearest index is %d.\n", val, l, u, index);
    } else {
      Array_get(space->discrSteps, i, &n);
      s = (u - l)/(n-1);
      index = (int) round((val - l)/s);
      if (index > (n-1)) index = n - 1;
      Debug_out(DEBUG_RVS_DISCR, "Value: %g in [%g, %g] with no of steps = %d and step size = %g. Nearest index is %d.\n", val, l, u, n, s, index);
    }
    Array_set(a, i, &index);
  }
  return 0;
}

unsigned int RectRealVectorSpace_dim(RectRealVectorSpace* this) {
	return this->dim;
}

#define DEBUG_RVS_NEW "RectRealVectorSpace_new"
RectRealVectorSpace* RectRealVectorSpace_new(RealVector* lower, RealVector* upper, Array* discrSteps) {
	Debug_assert(DEBUG_ALL, lower != NULL, "lower = NULL\n");
	Debug_assert(DEBUG_ALL, upper != NULL, "upper = NULL\n");
	unsigned int dim = RealVector_dim(upper);
	Debug_assert(DEBUG_ALL, dim == RealVector_dim(lower),
		"upper and lower have a different number of dimensions: %d vs %d\n",
		dim, RealVector_dim(lower));
	Debug_assert(DEBUG_ALL, discrSteps == NULL ? 1 : dim == Array_length(discrSteps),
		"wrong number of discrSteps: %d\n", Array_length(discrSteps));
	// discrSteps can be null

	RectRealVectorSpace* new = calloc(1, sizeof(RectRealVectorSpace));
	Debug_assert(DEBUG_ALL, new != NULL, "new == NULL");

	new->dim = dim;
	Debug_out(DEBUG_RVS_NEW, "RealVectorSpace_new(): dim = %d\n", new->dim);

	new->nonEmptyDimensions = Array_newInt(dim, dim);
	new->lower = RealVector_clone(lower);
	new->upper = RealVector_clone(upper);
	new->discrSteps = NULL;
	if (discrSteps != NULL) {
		new->discrSteps = Array_clone(discrSteps);
	}

	double lower_coord = 0;
	double upper_coord = 0;
	for(unsigned int i=0; i < dim; i++) {
		lower_coord = RealVector_get(new->lower, i);
		upper_coord = RealVector_get(new->upper, i);
		if (!Numbers_approxEQ(lower_coord, upper_coord)) {
			Array_push(new->nonEmptyDimensions, &i);
			Debug_out(DEBUG_RVS_NEW, " - Domain of coord %d has size > 0\n", i);
		}
	}

	Debug_out(DEBUG_RVS_NEW, "RealVectorSpace_new(): lower = ");
	Debug_perform(DEBUG_RVS_NEW, RealVector_fprint(new->lower, stderr));
	Debug_out(DEBUG_RVS_NEW, "RealVectorSpace_new(): upper = ");
	Debug_perform(DEBUG_RVS_NEW, RealVector_fprint(new->upper, stderr));

	new->env = NULL;
	new->free_env = NULL;
	return new;
}

void RectRealVectorSpace_free(RectRealVectorSpace** thisP) {
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL");
	RectRealVectorSpace* this = *thisP;
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
	RealVector_free(& (this->lower) );
	RealVector_free(& (this->upper));
	if (this->discrSteps != NULL) Array_free(& (this->discrSteps) );
	Array_free( & (this->nonEmptyDimensions) );
	if (this->env != NULL) this->free_env(& (this->env) );
	free(this);
	*thisP = NULL;
}


#define DEBUG_RND_VECTOR "RectRealVectorSpace_randomVector"
void RectRealVectorSpace_randomVector(RectRealVectorSpace* this, RndGen* rnd, RealVector* dest) {
	Debug_out(DEBUG_RND_VECTOR, "RectRealVectorSpace_randomVector(): start");
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
	Debug_assert(DEBUG_ALL, rnd != NULL, "rnd == NULL");
	Debug_assert(DEBUG_ALL, dest != NULL, "dest == NULL");

	double value = 0;
	int steps_i = 0;
	double step_size = NAN;
	if (this->discrSteps != NULL) {
		// discretised random vector
		for(unsigned int i = 0; i < this->dim; i++) {
			Array_get(this->discrSteps, i, &steps_i);
			Debug_assert(DEBUG_ALL, steps_i > 0, "steps_i == 0\n");
			Debug_out(DEBUG_RND_VECTOR, "RectRealVectorSpace_randomVector(): starting nextDQuantized"
				"(rnd, lower[i]=%lf, upper[i]=%lf, steps[i]=%d)\n",
				RealVector_get(this->lower, i),
				RealVector_get(this->upper, i),
				steps_i
			);
      if (steps_i == 1) {
        value = RealVector_get(this->lower, i);
      } else {
        value = RndGen_nextDQuantized(rnd,
              RealVector_get(this->lower, i),
              RealVector_get(this->upper, i),
              steps_i);
      }
			Debug_out(DEBUG_RND_VECTOR, "RectRealVectorSpace_randomVector(): end nextDQuantized --> %lf\n", value);
			RealVector_set(dest, i, value);
		}
	} else {
		// non discretised random vector
		for(unsigned int i = 0; i < this->dim; i++) {
			value = RndGen_nextD(rnd,
						RealVector_get(this->lower, i),
						RealVector_get(this->upper, i) );
			RealVector_set(dest, i, value);
		}
	}
	Debug_out(DEBUG_RND_VECTOR, "RectRealVectorSpace_randomVector(): end");
}


#define DEBUG_RND_VECTOR_SINGLE "RectRealVectorSpace_randomVector_changeSingleCoordinate"
void RectRealVectorSpace_randomVector_changeSingleCoordinate(RectRealVectorSpace* this, RndGen* rnd,
	RealVector* source, RealVector* dest) {

	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
	Debug_assert(DEBUG_ALL, rnd != NULL, "rnd == NULL");
	Debug_assert(DEBUG_ALL, source != NULL, "source == NULL");
	Debug_assert(DEBUG_ALL, dest != NULL, "dest == NULL");

	Debug_assert(DEBUG_ALL, RectRealVectorSpace_dim(this) == RealVector_dim(source),
		"space has wrong number of dimensions: %u vs. %u\n",
		RectRealVectorSpace_dim(this), RealVector_dim(source) );
	Debug_assert(DEBUG_ALL, RealVector_dim(source) == RealVector_dim(dest),
		"dest has wrong number of dimensions: %u vs. %u\n",
		RealVector_dim(dest), RealVector_dim(source) );

	RealVector_copy(source, dest);

	unsigned int rndNonEmptyDimIdx = (unsigned int) RndGen_nextUL(rnd, 0, Array_length(this->nonEmptyDimensions)-1);
	unsigned int i = 0;
	Array_get(this->nonEmptyDimensions, rndNonEmptyDimIdx, &i);

	Debug_assert(DEBUG_ALL, i >= 0, "i = %d < 0", i);
	Debug_assert(DEBUG_ALL, i < RectRealVectorSpace_dim(this), "i = %u >= this->dim", i);
  double newValue = NAN;
  if (this->discrSteps != NULL) {
    int steps_i = 0;
    Array_get(this->discrSteps, (unsigned int) i, &steps_i);
    if (steps_i == 1) {
      newValue = RealVector_get(this->lower, i);
    } else {
      newValue = RndGen_nextDQuantized(rnd,
        RealVector_get(this->lower, i),
        RealVector_get(this->upper, i),
        steps_i
      );
    }
  } else {
    newValue = RndGen_nextD(rnd, RealVector_get(this->lower, i), RealVector_get(this->upper, i));
  }
	RealVector_set(dest, i, newValue);
	Debug_out(DEBUG_RND_VECTOR_SINGLE, "Value of coord %d changed to %lf\n", i, newValue);
}


typedef struct {
	Array* rndNonEmptyDimPermArray; // of int -- a random perm. of the non-empty-dimension coords of the space
	unsigned int nbNonEmptyDimensions;
	RndGen* rnd;
	RndGenDistribution* rndDistr;
} _RandomChangeFromDistribution_Env;

void _RandomChangeFromDistribution_Env_free(void** thisP_v) {
	_RandomChangeFromDistribution_Env** thisP = (_RandomChangeFromDistribution_Env**) thisP_v;
	Debug_assert(DEBUG_ALL, thisP != NULL, "thisP == NULL");
	_RandomChangeFromDistribution_Env* this = *thisP;
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
	Array_free(& (this->rndNonEmptyDimPermArray) );
	RndGenDistribution_free(& (this->rndDistr));
	free(this);
	*thisP = NULL;
}
void RectRealVectorSpace_randomChangeFromDistribution_init(
	RectRealVectorSpace* this, RndGen* rnd,
	double (*probability)(unsigned long), int decimalPlaces) {

	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALL, rnd != NULL, "rnd == NULL\n");
	Debug_assert(DEBUG_ALL, probability != NULL, "probability == NULL");
	Debug_assert(DEBUG_ALL, decimalPlaces > 0, "decimalPlaces = %d <= 0\n", decimalPlaces);

	_RandomChangeFromDistribution_Env* env =
		calloc(1, sizeof(_RandomChangeFromDistribution_Env));
	env->rnd = rnd;
	env->nbNonEmptyDimensions = Array_length(this->nonEmptyDimensions);
	env->rndNonEmptyDimPermArray =
		Array_newInt(env->nbNonEmptyDimensions, env->nbNonEmptyDimensions);
	env->rndDistr =
		RndGenDistribution_new_fromFunction(rnd, probability, 1, env->nbNonEmptyDimensions, decimalPlaces);
	Debug_assert(DEBUG_ALL, env->rndDistr != NULL, "new->rndDistr == NULL\n");
	this->env = env;
	this->free_env = _RandomChangeFromDistribution_Env_free;
}

#define DEBUG_RND_PNT_DISTR "RectRealVectorSpace_randomChangeFromDistribution"
#define DEBUG_RND_PNT_DISTR_VERBOSE "RectRealVectorSpace_randomChangeFromDistribution_Verbose"
void RectRealVectorSpace_randomChangeFromDistribution(RectRealVectorSpace* this,
		RealVector* source, RealVector* dest) {

	Debug_assert(DEBUG_ALL, source != NULL, "source == NULL\n");
	Debug_assert(DEBUG_ALL, dest  != NULL, "dest == NULL\n");
	Debug_assert(DEBUG_ALL, RealVector_dim(source) == RealVector_dim(dest),
		"source->dim != dest->dim");
	Debug_assert(DEBUG_ALL, this->env != NULL, "this->env == NULL\n");
	Debug_out(DEBUG_RND_PNT_DISTR, "Start randomRealVectorDistribution:\n");

	// 1. Copy source into dest
	RealVector_copy(source, dest);

	_RandomChangeFromDistribution_Env* env = (_RandomChangeFromDistribution_Env*)this->env;

	// 2. Copy this->nonEmptyDimensions (all varying params in asc order) into env->rndNonEmptyDimPermArray
	Array_copy(this->nonEmptyDimensions, env->rndNonEmptyDimPermArray);

	// 3. Compute how many components to change
	unsigned int nbComponentsToChange = (unsigned int) RndGenDistribution_next(env->rndDistr); // within 1..this->nonEmptyDimensions
	Debug_assert(DEBUG_ALL, nbComponentsToChange > 0, "nbComponentsToChange <= 0");
	Debug_assert(DEBUG_ALL, nbComponentsToChange <= env->nbNonEmptyDimensions,
				"nbComponentsToChange > env->nbNonEmptyDimensions");

	/* 4. Components to change in 'dest' wrt 'source' are the first nbComponentsToChange components
		of env->rndNonEmptyDimPermArray. So we stop computation of
		env->rndNonEmptyDimPermArray as soon as the first nbComponentsToChange components have been defined */
	Debug_out(DEBUG_RND_PNT_DISTR, " - Changing %d components at random\n", nbComponentsToChange);

	unsigned int i=0, j=0, tmpIdx=0, chosenCoord = 0;
	Debug_out(DEBUG_RND_PNT_DISTR, " - Create prefix rndPermArray (first %d out of %d elements)\n",
		nbComponentsToChange, env->nbNonEmptyDimensions);

	// 4.a.  For each of the first nbComponentsToChange coordinates of non-empty dimension
	for (unsigned int i = 0; i < nbComponentsToChange; i++) {

		// 4.a.1. Choose a random index j between i and rndPermArrayLength-1
		j = (unsigned int) RndGen_nextUL(env->rnd, i, env->nbNonEmptyDimensions-1);
		// 4.a.2. Swap env->rndNonEmptyDimPermArray[i] and env->rndNonEmptyDimPermArray[j]
		Debug_out(DEBUG_RND_PNT_DISTR, "    - swap rndPermArray[%d] and rndPermArray[%d]\n", i, j);
		Debug_out(DEBUG_RND_PNT_DISTR, "       - before swap: \n");
		Debug_perform(DEBUG_RND_PNT_DISTR, Array_fprint(env->rndNonEmptyDimPermArray, stderr));
		Array_swap(env->rndNonEmptyDimPermArray, i, j);
		Debug_out(DEBUG_RND_PNT_DISTR, "       - after swap: \n");
		Debug_perform(DEBUG_RND_PNT_DISTR, Array_fprint(env->rndNonEmptyDimPermArray, stderr));

		// 4.a.3. Randomly change the env->rndNonEmptyDimPermArray[i]-th element of vector dest
		Debug_out(DEBUG_RND_PNT_DISTR_VERBOSE, "     - starting 4.a.3 for the %d-th coord to change\n", i);
		Array_get(env->rndNonEmptyDimPermArray, i, &chosenCoord);
		Debug_assert(DEBUG_RND_PNT_DISTR, chosenCoord < RealVector_dim(dest), "chosenCoord >= dest->dim\n");
		Debug_out(DEBUG_RND_PNT_DISTR, "     - chosenCoord = %d (out of %d dimensions). "
			"This coord has a domain of length %lf\n",
			chosenCoord,
			RectRealVectorSpace_dim(this),
			RealVector_get(this->upper, chosenCoord) - RealVector_get(this->lower, chosenCoord));
		Debug_assert(DEBUG_RND_PNT_DISTR,
			!Numbers_approxEQ(RealVector_get(this->upper, chosenCoord), RealVector_get(this->lower, chosenCoord)),
			"this->lower[chosenIdx] == this->upper[chosenIdx]\n");
    double newValue = NAN;
    if (this->discrSteps != NULL) {
      int steps_i = 0;
      Array_get(this->discrSteps, chosenCoord, &steps_i);
      if (steps_i == 1) {
        newValue = RealVector_get(this->lower, i);
      } else {
        newValue = RndGen_nextDQuantized(
          env->rnd,
          RealVector_get(this->lower, chosenCoord),
          RealVector_get(this->upper, chosenCoord),
          steps_i
        );
      }
    } else {
      newValue = RndGen_nextD(
        env->rnd,
        RealVector_get(this->lower, chosenCoord),
        RealVector_get(this->upper, chosenCoord)
      );
    }
		RealVector_set(dest, chosenCoord, newValue);
		Debug_out(DEBUG_RND_PNT_DISTR, "     - %u-th coord %u --> new rnd value %E\n",
				i,  chosenCoord, newValue);
	}
	Debug_out(DEBUG_RND_PNT_DISTR_VERBOSE, " - new rndPermArray:\n");
	Debug_perform(DEBUG_RND_PNT_DISTR_VERBOSE, Array_fprint(env->rndNonEmptyDimPermArray,
	 	stderr));
	Debug_out(DEBUG_RND_PNT_DISTR, "End randomRealVectorDistribution.\n");
}

#define DEBUG_RND_CMPNTS "RectRealVectorSpace_randomComponents"
void RectRealVectorSpace_randomComponents(RectRealVectorSpace* this, RndGen *rnd, RealVector const *source, RealVector* dest, Array const *componentIdxsToChange) {
	Debug_assert(DEBUG_ALL, source != NULL, "source == NULL\n");
	Debug_assert(DEBUG_ALL, dest  != NULL, "dest == NULL\n");
	Debug_assert(DEBUG_ALL, RealVector_dim(source) == RealVector_dim(dest),
		"source->dim != dest->dim");

	RealVector_copy(source, dest);

	unsigned int nbComponentsToChange = Array_length(componentIdxsToChange);
	Debug_assert(DEBUG_ALL, nbComponentsToChange > 0, "nbComponentsToChange <= 0");

	int tmpIdx=0, componentIdx = 0;

	for (unsigned int i = 0; i < nbComponentsToChange; i++) {
    Array_get(componentIdxsToChange, i, &componentIdx);
    Debug_assert(DEBUG_RND_CMPNTS, !Numbers_approxEQ(RealVector_get(this->upper, (unsigned int) componentIdx), RealVector_get(this->lower, (unsigned int) componentIdx)), "this->lower[chosenIdx] == this->upper[chosenIdx]\n");
    double newValue = NAN;
    if (this->discrSteps != NULL) {
      int steps_i = 0;
      Array_get(this->discrSteps, (unsigned int) componentIdx, &steps_i);
      if (steps_i == 1) {
        newValue = RealVector_get(this->lower, i);
      } else {
        newValue = RndGen_nextDQuantized(rnd, RealVector_get(this->lower, (unsigned int) componentIdx), RealVector_get(this->upper, (unsigned int) componentIdx), steps_i);
      }
    } else {
      newValue = RndGen_nextD(
        rnd,
        RealVector_get(this->lower, (unsigned int) componentIdx),
        RealVector_get(this->upper, (unsigned int) componentIdx)
      );
    }
  RealVector_set(dest, (unsigned int) componentIdx, newValue);
 }
}


#define DEBUG_DIST_NORM "RectRealVectorSpace_normalisedEuclideanDistance"
double RectRealVectorSpace_normalisedEuclideanDistance(RectRealVectorSpace* this, RealVector* vec1, RealVector* vec2) {
	Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
	Debug_assert(DEBUG_ALL, vec1 != NULL, "vec1 == NULL");
	Debug_assert(DEBUG_ALL, vec2 != NULL, "vec2 == NULL");
	Debug_assert(DEBUG_ALL, this->dim == RealVector_dim(vec1), "this->dim != vec1->dim");
	Debug_assert(DEBUG_ALL, this->dim == RealVector_dim(vec2), "this->dim != vec2->dim");

	double result = 0;
	Debug_out(DEBUG_DIST_NORM, "Computing normalised eucl. distance vec1 <--> vec2:\n");
	unsigned int nbNonEmptyDimensions = Array_length(this->nonEmptyDimensions);
	for (unsigned int i = 0; i < nbNonEmptyDimensions; i++) {
		unsigned int x = 0;
		Array_get(this->nonEmptyDimensions, i, &x);
		double rangeLen = RealVector_get(this->upper, x) - RealVector_get(this->lower, x);
		Debug_out(DEBUG_DIST_NORM, " - %u-th non-empty coord. %u has length %lf\n", i, x, rangeLen);
		// Note: rangeLen can be 0, as some ranges can be of size 0 (i.e., fixed params!!)
		if (rangeLen > 0) {
			result += pow( ( RealVector_get(vec1, x) - RealVector_get(vec2, x) ) /rangeLen, 2);
			Debug_out(DEBUG_DIST_NORM, "  ... result now: %lf\n", result);
		}
	}
	result = sqrt(result);
	Debug_out(DEBUG_DIST_NORM, "End: result = %lf\n", result);
	return result;
}

void RectRealVectorSpace_fprint(FILE *f, RectRealVectorSpace const *this) {
  Debug_assert(DEBUG_ALL, this != NULL, "this == NULL");
  Debug_assert(DEBUG_ALL, f != NULL, "f == NULL");
  int v = -1;
  double d = NAN;
  for (unsigned int i = 0; i < this->dim; ++i) {
    fprintf(f, "  idx = %u", i);
    fprintf(f, ", lower = %g", RealVector_get(this->lower, i));
    fprintf(f, ", upper = %g", RealVector_get(this->upper, i));
    if (this->discrSteps != NULL) {
      Array_get(this->discrSteps, (unsigned int) i, &v);
      fprintf(f, ", steps = %d\n", v);
    }
  }
}
