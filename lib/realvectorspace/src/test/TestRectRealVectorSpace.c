#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <RectRealVectorSpace.h>

double powerLaw(unsigned long x) {
	return pow(x, -2);
}

int main() {
	int dim = 10;

	RealVector* lower = RealVector_new(dim);
	RealVector* upper = RealVector_new(dim);
	Array* discrSteps = Array_newInt(dim,dim);
	int steps = 11;
  double step_size = 0.6;
	for(int i=0; i<dim; i++) {
		RealVector_set(lower, i, 5);
		RealVector_set(upper, i, 1);
		Array_add(discrSteps, &steps);
	}

	RectRealVectorSpace* space = RectRealVectorSpace_new(lower, upper, discrSteps);
	RndGen* rnd = RndGen_new();

	RealVector* vec1 = RealVector_new(dim);
	RealVector* vec2 = RealVector_new(dim);

	fprintf(stderr, "\n\nTesting randomVector():");
	for(int i=0; i<10; i++) {
		RectRealVectorSpace_randomVector(space, rnd, vec1);
		fprintf(stderr, "\n - ");
		RealVector_fprint(vec1, stderr);
	}

	fprintf(stderr, "vec1: ");
	RealVector_fprint(vec1, stderr);

	fprintf(stderr, "\n\nTesting changeSingleCoordinate():");
	for(int i=0; i<10; i++) {
		RectRealVectorSpace_randomVector_changeSingleCoordinate(space, rnd, vec1, vec2);
		fprintf(stderr, "\n - ");
		RealVector_fprint(vec2, stderr);
	}

	fprintf(stderr, "\n\nTesting randomChangeFromDistribution():");
	RectRealVectorSpace_randomChangeFromDistribution_init(space, rnd, powerLaw, 8);
	for(int i=0; i<10; i++) {
		RectRealVectorSpace_randomChangeFromDistribution(space, vec1, vec2);
		fprintf(stderr, "\n - ");
		RealVector_fprint(vec2, stderr);
	}
	fprintf(stderr, "\n\nDone.\n");

	return 0;
}
