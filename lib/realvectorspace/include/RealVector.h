/*
#    This file is part of RealVectorSpace
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    RealVectorSpace is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    RealVectorSpace is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RealVectorSpace.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __REAL_VECTOR__
#define __REAL_VECTOR__

#include <MCLabUtils.h>

typedef struct RealVector RealVector;

RealVector* RealVector_new(unsigned int dim);

// Creates a new RealVector from coordinates in Array(double) coords
RealVector* RealVector_newFromArray(Array const *coords);

unsigned int RealVector_dim(const RealVector* const _this);

const double* RealVector_as_C_array(const RealVector* const _this);
Array const *RealVector_as_Array(const RealVector* const _this);
double RealVector_get(const RealVector* const _this,unsigned  int i);
void RealVector_set(const RealVector* _this, unsigned  int i, double value);

RealVector* RealVector_clone(const RealVector* const _this);
void RealVector_copy(const RealVector* const _this, RealVector* dest);
void RealVector_free(RealVector** pp);

int RealVector_equals(const RealVector* const _this, const RealVector* const other);
void RealVector_fprint(const RealVector* const _this, FILE* f);

#endif
