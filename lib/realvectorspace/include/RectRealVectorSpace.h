/*
#    This file is part of RealVectorSpace
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    RealVectorSpace is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    RealVectorSpace is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RealVectorSpace.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __RECT_REAL_VECTOR_SPACE__
#define __RECT_REAL_VECTOR_SPACE__

#include <MCLabUtils.h>

#include <RealVector.h>


typedef struct RectRealVectorSpace RectRealVectorSpace;

bool RectRealVectorSpace_belongsTo(RectRealVectorSpace const *space, RealVector const *v);
unsigned int RectRealVectorSpace_dim(RectRealVectorSpace* _this);
RectRealVectorSpace* RectRealVectorSpace_new(RealVector* lower, RealVector* upper, Array* discrSteps);

int RectRealVectorSpace_discretisedRealVectorIndexes(RectRealVectorSpace const *space, RealVector const *v, Array *a);

void RectRealVectorSpace_free(RectRealVectorSpace** thisP);
void RectRealVectorSpace_fprint(FILE *f, RectRealVectorSpace const *_this);

void RectRealVectorSpace_randomVector(RectRealVectorSpace* _this, RndGen* rnd, RealVector* dest);
void RectRealVectorSpace_randomVector_changeSingleCoordinate(RectRealVectorSpace* _this, RndGen *rnd,
	RealVector* source, RealVector* dest);

void RectRealVectorSpace_randomChangeFromDistribution_init(
	RectRealVectorSpace* this, RndGen* rnd,
	double (*probability)(unsigned long), int decimalPlaces);

void RectRealVectorSpace_randomComponents(RectRealVectorSpace* _this, RndGen *rnd, RealVector const *source, RealVector* dest, Array const *componentIdxsToChange);
void RectRealVectorSpace_randomChangeFromDistribution(RectRealVectorSpace* _this,
	RealVector* source, RealVector* dest);

double RectRealVectorSpace_normalisedEuclideanDistance(RectRealVectorSpace* _this, RealVector* vec1, RealVector* vec2);

#endif

