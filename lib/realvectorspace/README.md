# RealVectorSpace #

The package is versioned using the [semantic versioning policy](http://semver.org).

## Building prerequisites ##

RealVectorSpace depends on the following library:

* [MCLabUtils v3.0.0](https://bitbucket.org/mclab/mclabutils/downloads?tab=tags)

## Installation ##

Download most recent release from [download section](https://bitbucket.org/mclab/realvectorspace/downloads?tab=tags).

Then type the following:
```
mkdir build
cd build
cmake .. && make
```

## Contributors ##
* [Toni Mancini](https://bitbucket.org/tmancini/)

## License

RealVectorSpace is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published
by the Free Software Foundation.

RealVectorSpace is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RealVectorSpace.
If not, see <https://www.gnu.org/licenses/>.

