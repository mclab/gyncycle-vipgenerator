/*#    This file is part of MCLabUtils
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <stdarg.h>

#include <config.h>

#include <Debug.h>
#include <StringUtils.h>

#define DEBUG_FU "FileUtils"

#include <FileUtils.h>

#define STRING_MAX_LEN 5121


void FileUtils_lineByline(const char *fileName, Array *lines) {

  Debug_assert(DEBUG_ALL, fileName != NULL, "fileName == NULL");

  int lnumber = 0;
  FILE *f = fopen(fileName, "r");
  char buffer[STRING_MAX_LEN];
  Debug_out(DEBUG_FU, "Starting reading file '%s'\n", fileName);
  while (fgets(buffer, STRING_MAX_LEN, f) != NULL) {
    Debug_out(DEBUG_FU, "Line %d (%ld chars): '%s'\n", lnumber, strlen(buffer),
              buffer);
    StringUtils_trim(buffer);
    char *line = StringUtils_clone(buffer);
    Array_add(lines, &line);
    lnumber++;
  }
  fclose(f);
}

int FileUtils_listDir(const char *dirName, char *fileExtension, int depth,
                      Array *fileNames) {

  int err = 0;
  DIR *d = opendir(dirName);
  struct dirent *entry = {0};

  Debug_assert(DEBUG_ALL, d != NULL, "Cannot open directory '%s'\n", dirName);
  if (d == NULL) return -1;

  char *fullFileName = NULL;

  while ((entry = readdir(d)) != NULL) {
    Debug_out(DEBUG_FU, "Processing file '%s'\n", entry->d_name);
    int lastDotIdx = StringUtils_strpos(entry->d_name, '.');
    if (lastDotIdx == 0) {
      Debug_out(DEBUG_FU, " --> starting with '.': skipped\n");
      continue;
    }
    // skip file with a different extension
    if (!(entry->d_type & DT_DIR) &&
        (!StringUtils_endsWith(entry->d_name, fileExtension))) {
      Debug_out(DEBUG_FU,
                " --> ends with different extension from '%s': skipped\n",
                fileExtension);
      continue;
    }

    if (!(entry->d_type & DT_DIR)) {
      asprintf(&fullFileName, "%s/%s", dirName, entry->d_name);
      char *fileName = StringUtils_clone(fullFileName);
      free(fullFileName);
      Array_add(fileNames, &fileName);
    }

    if (entry->d_type & DT_DIR) {
      if (strcmp(entry->d_name, "..") != 0 && strcmp(entry->d_name, ".") != 0) {
        asprintf(&fullFileName, "%s/%s", dirName, entry->d_name);
        if (depth > 0) {
          err |= FileUtils_listDir(fullFileName, fileExtension, depth - 1, fileNames);
        }
        free(fullFileName);
      }
    }
  }
  err |= closedir(d);
  Debug_assert(DEBUG_ALL, err == 0, "Could not close '%s'\n", dirName);

  return err;
}


int FileUtils_copyFile(const char *from, const char *to) {
  int fd_to, fd_from;
  char buf[4096];
  ssize_t nread;
  int saved_errno;

  fd_from = open(from, O_RDONLY);
  if (fd_from < 0) {
    Debug_out(DEBUG_FU, "fd_from == NULL\n");
    return -1;
  }

  fd_to = creat(to, 0666);
  if (fd_to < 0) goto out_error;

  while (nread = read(fd_from, buf, sizeof buf), nread > 0) {
    char *out_ptr = buf;
    ssize_t nwritten;

    do {
      nwritten = write(fd_to, out_ptr, (size_t) nread);

      if (nwritten >= 0) {
        nread -= nwritten;
        out_ptr += nwritten;
      } else if (errno != EINTR) {
        goto out_error;
      }
    } while (nread > 0);
  }

  if (nread == 0) {
    if (close(fd_to) < 0) {
      fd_to = -1;
      goto out_error;
    }
    close(fd_from);

    /* Success! */
    return 0;
  }

out_error:
  saved_errno = errno;

  close(fd_from);
  if (fd_to >= 0) close(fd_to);

  errno = saved_errno;
  Debug_out(DEBUG_FU, "errno = %d\n", errno);
  return -1;
}

int FileUtils_isFile(const char *const path) {
  struct stat s;
  int err = stat(path, &s);
  if (err) {
    return 0;
  } else {
    return (S_ISREG(s.st_mode));
  }
}
int FileUtils_isDir(const char *const path) {
  struct stat s;
  int err = stat(path, &s);
  if (err) {
    return 0;
  } else {
    return (S_ISDIR(s.st_mode));
  }
}

const char *FileUtils_dirName(const char *const path, char *const out_dir) {
  int pos_slash = StringUtils_lastIndexOf(path, '/');
  if (pos_slash < 0) {
    // there are no slashes
    strcpy(out_dir, "./");
  } else {
    if (path[pos_slash + 1] != '\0') {
      // path does not terminate with a slash
      strncpy(out_dir, path, (unsigned long) pos_slash + 1);
      out_dir[pos_slash + 1] = '\0';
    } else {
      // path terminates with /, hence points to a directory
      int pos_prev_slash =
          StringUtils_lastIndexOf_atMost(path, '/', pos_slash - 1);
      if (pos_prev_slash < 0) {
        // no previous slashes, dirname is './'
        strcpy(out_dir, "./");
      } else {
        strncpy(out_dir, path, (unsigned long) pos_prev_slash + 1);
        out_dir[pos_prev_slash + 1] = '\0';
      }
    }
  }
  return out_dir;
}
const char *FileUtils_baseName(const char *const path, char *const out_fname) {
  int pos_slash = StringUtils_lastIndexOf(path, '/');
  if (pos_slash < 0) {
    strcpy(out_fname, path);
  } else {
    if (path[pos_slash + 1] != '\0') {
      strcpy(out_fname, &(path[pos_slash + 1]));
    } else {
      // path terminates with '/', hence is a directory
      // find previous slash
      int pos_prev_slash =
          StringUtils_lastIndexOf_atMost(path, '/', pos_slash - 1);
      if (pos_prev_slash < 0) {
        pos_prev_slash = -1;
      }
      strncpy(out_fname, &(path[pos_prev_slash + 1]),
              (unsigned long) (pos_slash - (pos_prev_slash + 1) + 1));
      out_fname[pos_slash - (pos_prev_slash + 1) + 1] = '\0';
    }
  }
  return out_fname;
}

#define CREATE_DIR_FLAGS 0755

int FileUtils_createDir(const char *dir_path) {
  const char *const fname = "FileUtils_createDir";
  Debug_assert(DEBUG_ALWAYS, NULL != dir_path, "%s: NULL == dir_path\n", fname);
  errno = 0;
  int err = mkdir(dir_path, CREATE_DIR_FLAGS);
  Debug_assert(DEBUG_ALWAYS, (0 == err) || (EEXIST == errno),
               "%s: 0 != err (%d) && EEXIST != errno (%d)\n", fname, err,
               errno);
  if ((0 != err) && (EEXIST != errno)) {
    fprintf(stderr, "%s: unable to create dir \"%s\": %s\n", fname, dir_path,
            strerror(errno));
    return -1;
  }
  return 0;
}

#ifdef ZIP_FOUND
#include <zip.h>

#define DEBUG_ZIP_UTILS "Debug_zip_utils"
#define ZIP_BUF_SIZE 256

int FileUtils_unzip(const char *file_name, const char *out_dir) {
  const char *const fname = "FileUtils_unzip";
  Debug_assert(DEBUG_ALWAYS, NULL != file_name, "%s: NULL == file_name\n",
               fname);
  Debug_assert(DEBUG_ALWAYS, NULL != out_dir, "%s: NULL == out_dir\n", fname);
  Debug_assert(DEBUG_ALWAYS, StringUtils_endsWith(out_dir, "/"),
               "%s: out_dir (\"%s\") doesn't end with a '/'\n", fname, out_dir);
  if (!StringUtils_endsWith(out_dir, "/")) {
    fprintf(stderr, "%s: output directory should end with '/'.\n", fname);
    return -1;
  }
  Debug_assert(DEBUG_ALWAYS, FileUtils_isFile(file_name),
               "%s: \"%s\" is not a file\n", fname, file_name);
  if (!FileUtils_isFile(file_name)) {
    fprintf(stderr, "%s: file \"%s\" not found.\n", fname, file_name);
    return -2;
  }
  Debug_assert(DEBUG_ALWAYS, FileUtils_isDir(out_dir),
               "%s: \"%s\" is not a directory\n", fname, out_dir);
  if (!FileUtils_isDir(out_dir)) {
    fprintf(stderr, "%s: output directory \"%s\" not found.\n", fname, out_dir);
    return -3;
  }
  struct zip *archive = NULL;
  struct zip_file *zip_file = NULL;
  struct zip_stat archive_stats = {0};
  char buf[ZIP_BUF_SIZE] = {0};
  char *name = NULL;
  int err = 0;
  int ze = 0;
  archive = zip_open(file_name, 0, &ze);
  Debug_assert(DEBUG_ALWAYS, NULL != archive, "%s: NULL == archive\n", fname);
  Debug_assert(DEBUG_ALWAYS, 0 == ze, "%s: 0 != err.\n", fname);
  if ((NULL == archive) || (0 != ze)) {
    zip_error_t error;
    zip_error_init_with_code(&error, ze);
    fprintf(stderr, "%s: zip_open failed: %s.\n", fname,
            zip_error_strerror(&error));
    zip_error_fini(&error);
    return -3;
  }
  zip_int64_t i = 0;
  zip_int64_t n = 0;
  n = zip_get_num_entries(archive, 0);
  Debug_assert(DEBUG_ALWAYS, 0 <= n, "%s: 0 > n (%lld).\n", fname,
               (long long) n);
  if (0 > n) {
    fprintf(stderr, "%s: zip_get_num_entries failed.\n", fname);
    zip_close(archive);
    return -4;
  }
  for (i = 0; i < n; ++i) {
    err = zip_stat_index(archive, (zip_uint64_t) i, 0, &archive_stats);
    if (0 == err) {
      Debug_out(DEBUG_ZIP_UTILS,
                "%s: { name : \"%s\", size : %llu, mtime : %ld }\n", fname,
                archive_stats.name, archive_stats.size, archive_stats.mtime);
      name = StringUtils_concatenate(2, out_dir, archive_stats.name);
      Debug_assert(DEBUG_ALWAYS, NULL != name, "%s: NULL == name\n", fname);
      if (NULL == name) {
        fprintf(stderr, "%s: StringUtils.concatenate() failed.\n", fname);
        zip_close(archive);
        return -4;
      }
      if (StringUtils_endsWith(name, "/")) {
        Debug_out(DEBUG_ZIP_UTILS, "%s: unzipping folder to '%s'\n", fname,
                  name);
        FileUtils_createDir(name);
      } else {
        unsigned long long sum = 0;
        long int len = 0, len_write = 0;
        int fd = 0;
        Debug_out(DEBUG_ZIP_UTILS, "%s: unzipping file to '%s'\n", fname, name);
        zip_file = zip_fopen_index(archive, (zip_uint64_t) i, 0);
        Debug_assert(DEBUG_ALWAYS, NULL != zip_file, "%s: NULL == zip_file\n",
                     fname);
        if (NULL == zip_file) {
          fprintf(stderr, "%s: zip_fopen_index failed.\n", fname);
          free(name);
          zip_close(archive);
          return -5;
        }
        errno = 0;
        fd = open(name, O_RDWR | O_TRUNC | O_CREAT, 0644);
        Debug_assert(DEBUG_ALWAYS, 0 <= fd, "%s: 0 > fd (%d)\n", fname, fd);
        Debug_assert(DEBUG_ALWAYS, 0 == errno,
                     "%s: 0 != errno (%d) (error: %s)\n", fname, errno,
                     strerror(errno));
        if ((0 > fd) || (0 != errno)) {
          fprintf(stderr, "%s: open failed: %s.\n", fname, strerror(errno));
          zip_fclose(zip_file);
          free(name);
          zip_close(archive);
          return -5;
        }
        while (sum < archive_stats.size) {
          len = zip_fread(zip_file, buf, sizeof(buf));
          Debug_assert(DEBUG_ALWAYS, 0 <= len, "%s: 0 > len (%ld)\n", fname,
                       len);
          if (0 > len) {
            fprintf(stderr, "%s: zip_fread failed.\n", fname);
            close(fd);
            zip_fclose(zip_file);
            free(name);
            zip_close(archive);
            return -5;
          }
          errno = 0;
          len_write = write(fd, buf, (long unsigned int) len);
          Debug_assert(DEBUG_ALWAYS, len == len_write,
                       "%s: len (%ld) != len_write (%ld)\n", fname, len,
                       len_write);
          Debug_assert(DEBUG_ALWAYS, 0 == errno,
                       "%s: 0 != errno (%d) (error: %s).\n", fname, errno,
                       strerror(errno));
          if ((len != len_write) || (0 != errno)) {
            fprintf(stderr, "%s: write failed: %s.\n", fname, strerror(errno));
            close(fd);
            zip_fclose(zip_file);
            free(name);
            zip_close(archive);
            return -5;
          }
          sum += (long unsigned int) len;
        }
        errno = 0;
        err = close(fd);
        Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d).\n", fname,
                     err);
        Debug_assert(DEBUG_ALWAYS, 0 == errno,
                     "%s: 0 != errno (%d) (error: %s).\n", fname, errno,
                     strerror(errno));
        if ((0 != err) || (0 != errno)) {
          fprintf(stderr, "%s: close failed: %s", fname, strerror(errno));
          zip_fclose(zip_file);
          free(name);
          zip_close(archive);
          return -6;
        }
        err = zip_fclose(zip_file);
        Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d).\n", fname,
                     err);
        if (0 != err) {
          fprintf(stderr, "%s: zip_fclose failed.", fname);
          free(name);
          zip_close(archive);
          return -6;
        }
      }
      free(name);
    } else {
      fprintf(stderr, "%s: zip_stat_index failed.\n", fname);
      zip_close(archive);
      return -4;
    }
  }
  err = zip_close(archive);
  Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d)\n", fname, err);
  return 0;
}
#endif /* #ifdef ZIP_FOUND */

int FileUtils_rmdir(const char *dir_path) {
  const char *const fname = "FileUtils_rmdir";

  Debug_assert(DEBUG_ALWAYS, NULL != dir_path, "%s: NULL == dir_path.\n",
               fname);
  if (NULL == dir_path) {
    fprintf(stderr, "%s: NULL pointer argument.\n", fname);
    return -1;
  }

  errno = 0;
  DIR *dir = opendir(dir_path);
  Debug_assert(DEBUG_ALWAYS, NULL != dir, "%s: NULL == dir.\n", fname);
  Debug_assert(DEBUG_ALWAYS, 0 == errno, "%s: 0 != errno (%d) (error: %s).\n",
               fname, errno, strerror(errno));
  if ((NULL == dir) || (0 != errno)) {
    fprintf(stderr, "%s: opendir(\"%s\") failed: %s.\n", fname, dir_path,
            strerror(errno));
    return -1;
  }

  struct dirent *entry = NULL;

  int err = 0;

  Debug_out(DEBUG_FU, "%s: removing directory \"%s\"...\n", fname, dir_path);

  while (1) {
    errno = 0;
    entry = readdir(dir);
    Debug_assert(DEBUG_ALWAYS, (NULL != entry) || (0 == errno),
                 "%s: NULL == entry && 0 != errno (%d) (error: %s).\n", fname,
                 errno, strerror(errno));
    if ((NULL == entry) && (0 != errno)) {
      fprintf(stderr, "%s: readdir failed: %s.\n", fname, strerror(errno));
      return -2;
    }

    if ((NULL == entry) && (0 == errno)) {
      Debug_out(DEBUG_FU, "%s: reached end of directory \"%s\".\n", fname,
                dir_path);
      break;
    }

    if (StringUtils_equals(entry->d_name, ".") ||
        StringUtils_equals(entry->d_name, "..")) {
      Debug_out(DEBUG_FU, "%s: skipping file \"%s\".\n", fname, entry->d_name);
      continue;
    }

    char *entry_path = StringUtils_concatenate(3, dir_path, "/", entry->d_name);
    Debug_assert(DEBUG_ALWAYS, NULL != entry_path, "%s: NULL == entry_path.\n",
                 fname);
    if (NULL == entry_path) {
      fprintf(stderr, "%s: StringUtils_concatenate failed.\n", fname);
      return -3;
    }

    struct stat entry_stat = {0};
    errno = 0;
    err = stat(entry_path, &entry_stat);
    Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d).\n", fname, err);
    Debug_assert(DEBUG_ALWAYS, 0 == errno, "%s: 0 != errno (%d) (error: %s).\n",
                 fname, errno, strerror(errno));
    if ((0 != err) || (0 != errno)) {
      fprintf(stderr, "%s: stat failed: %s.\n", fname, strerror(errno));
      free(entry_path);
      return -3;
    }

    if (S_ISDIR(entry_stat.st_mode)) {
      err = FileUtils_rmdir(entry_path);
      Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d).\n", fname, err);
      if (0 != err) {
        fprintf(stderr, "%s: recursive FileUtils_rmdir failed.\n", fname);
        free(entry_path);
        return -4;
      }
    } else {
      Debug_out(DEBUG_FU, "%s: removing file \"%s\"...\n", fname, entry_path);
      errno = 0;
      err = unlink(entry_path);
      Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d).\n", fname, err);
      Debug_assert(DEBUG_ALWAYS, 0 == errno,
                   "%s: 0 != errno (%d) (error: %s).\n", fname, errno,
                   strerror(errno));
      if ((0 != err) || (0 != errno)) {
        fprintf(stderr, "%s: unlink failed: %s.\n", fname, strerror(errno));
        free(entry_path);
        return -5;
      }
    }

    free(entry_path);
  }

  errno = 0;
  err = closedir(dir);
  Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d).\n", fname, err);
  Debug_assert(DEBUG_ALWAYS, 0 == errno, "%s: 0 != errno (%d) (error: %s).\n",
               fname, errno, strerror(errno));
  if ((0 != err) || (0 != errno)) {
    fprintf(stderr, "%s: closedir failed: %s.\n", fname, strerror(errno));
    return -6;
  }

  errno = 0;
  err = rmdir(dir_path);
  Debug_assert(DEBUG_ALWAYS, 0 == err, "%s: 0 != err (%d).\n", fname, err);
  Debug_assert(DEBUG_ALWAYS, 0 == errno, "%s: 0 != errno (%d) (error: %s).\n",
               fname, errno, strerror(errno));
  if ((0 != err) || (0 != errno)) {
    fprintf(stderr, "%s: rmdir failed: %s.\n", fname, strerror(errno));
    return -7;
  }

  return 0;
}
