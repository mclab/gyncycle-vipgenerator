/*#    This file is part of MCLabUtils
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include <Debug.h>
#include <HashSet.h>
#include <HashTable.h>

/**
Hash-set based on HashTable
**/


struct HashSet {
	HashTable* hTable;
	char* fakeValuePtr;
};

size_t HashSet_sizeof() {
	return sizeof(HashSet);
}

HashSet* HashSet_new() {
	HashTable* hTable = HashTable_new();
	HashSet* new = (HashSet*)calloc(1, sizeof(HashSet));
	Debug_assert(DEBUG_ALWAYS, new != NULL, "new == NULL\n");
	new->hTable = hTable;
	new->fakeValuePtr = (char*)calloc(1, sizeof(char));
	return new;
}

int HashSet_contains(const HashSet* const this, const void* const key, size_t keySize) {
	if (!this) {
		fprintf(stderr, "HashSet_contains(): this cannot be null.\n");
		return 0;
	}
	void* valueP = NULL;
	valueP = HashTable_get(this->hTable, key, keySize, &valueP);
	return (valueP != NULL);
}

int HashSet_add(HashSet* const this, const void* const key, size_t keySize) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	void* old = HashTable_put(this->hTable, key, keySize, this->fakeValuePtr);
	return (old == NULL);
}

void const *HashSet_get(const HashSet* const this, const void* const key, size_t keySize) {
	if (!this) {
		fprintf(stderr, "HashSet_contains(): this cannot be null.\n");
		return 0;
	}
	void* valueP = NULL;
  HashTable_Entry *e = HashTable_entry(this->hTable, key, keySize);
  if (e != NULL) {
    return HashTable_Entry_key(e);
  } else {
    return NULL;
  }
}


void HashSet_remove(HashSet* const this, const void* const key, size_t keySize) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	Debug_assert(DEBUG_ALWAYS, key != NULL, "key == NULL\n");
	HashTable_remove(this->hTable, key, keySize);
}

unsigned long HashSet_size(const HashSet* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return HashTable_size(this->hTable);
}

void HashSet_free(HashSet** const thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	HashSet* this = *thisP;
	if (this == NULL) return;
	HashTable_free(&this->hTable);
	free( this->fakeValuePtr );
	free(this);
	*thisP = NULL;
}



int HashSet_containsAll(const HashSet* this, const HashSet* other) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	if (other == NULL) return 1;

	HashSetIterator* it = HashSetIterator_new(other);
	const void* elem = NULL;
  size_t elemSize = 0;
	while ( (elem = HashSetIterator_next(it, &elemSize)) != NULL ) {
		if (!HashSet_contains(this, elem, elemSize)) return 0;
	}
	return 1;
}
int HashSet_equals(const HashSet* this, const HashSet* other) {
	if (this == NULL) return other == NULL;
	if (other == NULL) return 0;
	if (HashSet_size(this) != HashSet_size(other)) return 0;
	return HashSet_containsAll(this, other);
}


// HashSet Iterator
struct HashSetIterator {
	HashTableIterator* hTableIterator;
};

/** Create an Iterator for this HashSet */
HashSetIterator* HashSetIterator_new(const HashSet* const this) {
	HashSetIterator* it = (HashSetIterator*)calloc(1, sizeof(HashSetIterator));
	Debug_assert(DEBUG_ALWAYS, it != NULL, "it == NULL\n");
	it->hTableIterator = HashTableIterator_new(this->hTable);
	return it;
}

/** Return 1 if a call to HashSetIterator_next(this) will return an entry, 0 otherwise */
int HashSetIterator_hasNext(const HashSetIterator* const this) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	return HashTableIterator_hasNext(this->hTableIterator);
}

/** Returns the next element of the HashSet which 'this' is iterating on.
*/
const void* HashSetIterator_next(HashSetIterator* const this, size_t *elemSizePtr) {
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	HashTable_Entry* e = HashTableIterator_next(this->hTableIterator);
	if (e == NULL) return 0;
  if (elemSizePtr != NULL) {
    *elemSizePtr = HashTable_Entry_keySize(e);
  }
	return HashTable_Entry_key(e);
}


/** Free memory used by this iterator. Also sets *this to NULL. */
void HashSetIterator_free(HashSetIterator** const thisP) {
	Debug_assert(DEBUG_ALWAYS, thisP != NULL, "thisP == NULL\n");
	HashSetIterator* this = *thisP;
	Debug_assert(DEBUG_ALWAYS, this != NULL, "this == NULL\n");
	HashTableIterator_free(& this->hTableIterator );
	free(this);
	*thisP = NULL;
}



