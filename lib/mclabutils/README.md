# MCLabUtils #

MCLabUtils is a general-purpose C library.

The package is versioned using the [semantic versioning policy](http://semver.org).

## Building prerequisites ##

MCLabUtils Library depends on the following:

* [GSL](https://www.gnu.org/software/gsl/)
* [CMake](https://cmake.org)
* [MCLab CMake modules](https://bitbucket.org/mclab/cmake)

Optional dependencies:

* [libzip](https://nih.at/libzip/)

## For users ##

Make sure that your CMake module path contains path to [MCLab CMake modules](https://bitbucket.org/mclab/cmake).

Use the following CMake macro to link your library/tool to MCLabUtils:

```
mclab_link_library(MCLabUtils)
```


## Contributors ##

* [Vadim Alimguzhin](https://bitbucket.org/darth_ru/)
* [Toni Mancini](https://bitbucket.org/tmancini/)
* [Stefano Sinisi](https://bitbucket.org/stefano_sinisi/)

## License

MCLabUtils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published
by the Free Software Foundation.

MCLabUtils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MCLabUtils.
If not, see <https://www.gnu.org/licenses/>.

