# Release v4.0.2

* Fix conditional compiling of `FileUtils_unzip()`.
* Update changelog.

# Release v4.0.1

* Fix bug in `StringUtils_tokenize()`.

# Release v4.0.0

* HashTable now uses variable-length keys.
  - Such modification of `HashTable` allowed to remove need for padding of keys
    in `Properties`, so now `Properties` objects are thread-safe.
  - Variable-length keys in `HashTable` affected also `HashSet`: it stores now
    variable-length objects.
* Dependency on `libzip` now is optional. Libraries/tools that need corresponding
  functionality can request `ZIP` feature when linking to `MCLabUtils`.

# Release v3.1.0

* Switch from HeaderDoc to Doxygen
* ...

# Release v3.0.0

* Implement `TupleGenerator`
* Implement `Array_sort()`
* Implement `Range_Int_equals()`
* Switch to CMake
* Bugfixes *affect backward compatibility* in `Range_Int_Iterator`
