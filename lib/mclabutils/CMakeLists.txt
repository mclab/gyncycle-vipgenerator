#    This file is part of MCLabUtils
#    Copyright (C) 2020 MCLab (http://mclab.di.uniroma1.it)
#
#    MCLabUtils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabUtils is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabUtils.
#    If not, see <https://www.gnu.org/licenses/>.

include(MCLabLibrary)

if(APPLE)
  file(GLOB MEMMAP_SRCS src/memmap/src/*.c)
endif(APPLE)

mclab_library(MCLabUtils 4.0.2 ${MEMMAP_SRCS})

mclab_find_and_link_library(${PROJECT_NAME} GSL REQUIRED)
mclab_find_and_link_library(${PROJECT_NAME} ZIP 1.0.0)
if(ZIP_FOUND)
  mclab_library_feature(ZIP)
endif()

target_link_libraries(${PROJECT_NAME} m)

mclab_generate_config_header()

file(GLOB TESTS src/test/*.c)
foreach(TEST_FILE ${TESTS})
  get_filename_component(TEST_TARGET ${TEST_FILE} NAME_WE)
  add_executable(${TEST_TARGET} ${TEST_FILE})
  target_include_directories(${TEST_TARGET} PRIVATE ${PROJECT_BINARY_DIR})
  target_link_libraries(${TEST_TARGET} MCLabUtils)
  if(${TEST_TARGET} STREQUAL "TestCSV")
    set(TEST_ARGS "csv_test.csv" 0)
  elseif(${TEST_TARGET} STREQUAL "TestCommandLineInterface")
    set(TEST_ARGS --option1 "Ciao" --option2 "42" --option3 "3.14" --flag1)
  elseif(${TEST_TARGET} STREQUAL "TestFileUtils")
    set(TEST_ARGS ../../src)
  elseif(${TEST_TARGET} STREQUAL "TestNumbers")
    set(TEST_ARGS 3.14 3.15)
  elseif(${TEST_TARGET} STREQUAL "TestProperties")
    set(TEST_ARGS "test_properties.txt")
  elseif(${TEST_TARGET} STREQUAL "TestRangeDouble")
    set(TEST_ARGS 0.01 9.99 1)
  elseif(${TEST_TARGET} STREQUAL "TestRangeInt")
    set(TEST_ARGS 0 10 1)
  elseif(${TEST_TARGET} STREQUAL "TestStringUtils")
    set(TEST_ARGS "\tHello World  ")
  else()
    unset(TEST_ARGS)
  endif()
  add_test(NAME ${TEST_TARGET}
    COMMAND ${TEST_TARGET} ${TEST_ARGS}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/src/test)
endforeach(TEST_FILE ${TESTS})
