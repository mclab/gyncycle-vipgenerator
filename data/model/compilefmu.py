#! /usr/bin/env python2

#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.


import pymodelica

import argparse


parser = argparse.ArgumentParser(description='Compile FMU')
parser.add_argument('modelname', type=str, help='Model name')
parser.add_argument('modelfiles', type=str, nargs='+', help='Model files')
args = parser.parse_args()

model_name = args.modelname
model_files = args.modelfiles

pymodelica.compile_fmu(model_name, model_files)
