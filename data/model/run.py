#! /usr/bin/env python2
#    This file is part of GynCycle-ViPGenerator
#    Copyright (C) 2020 Stefano Sinisi, Vadim Alimguzhin
#
#    GynCycle-ViPGenerator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GynCycle-ViPGenerator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GynCycle-ViPGenerator.
#    If not, see <https://www.gnu.org/licenses/>.


import pymodelica
import pyfmi

import argparse
import numpy as np


parser = argparse.ArgumentParser(description='FMU Simulator')
parser.add_argument('fmu', type=str, help='fmu file path')
parser.add_argument('--overridefile', type=str, help='File containing list of variable values to set')
parser.add_argument('--output', type=str, help='Output file')
args = parser.parse_args()

model_name = 'Gyncycle'
fmu_path = args.fmu
override_file=args.overridefile
output_file = args.output if args.output is not None else "out.csv"

model = pyfmi.load_fmu(fmu_path)

var_to_override = {'stopTime' : 30.0, 'stepSize': 0.1}
if override_file is not None:
    with open(override_file, 'r') as f:
        for line in f:
            words = line.split('=')
            varname = words[0]
            varvalue = words[1]
            var_to_override[varname] = float(varvalue)

for var, value in var_to_override.iteritems():
    if var != 'stopTime' and var != 'stepSize':
        model.set(var, value)

outputvars = ['time'] + ['species[{}]'.format(i) for i in range(1,36)]

duration = var_to_override['stopTime']
step_size = var_to_override['stepSize']

opts = model.simulate_options()
opts['result_handling'] = 'memory'
opts['ncp'] = duration/step_size
opts['filter'] = 'species*'
res = model.simulate(final_time=duration, options=opts)
solution = [res[name] for name in outputvars]


np.savetxt(output_file, np.transpose(solution), delimiter=',')


# with open(output_file, 'w') as out:
#     for i in xrange(0, len(res['time'])):
#         t = res['time'][i]
#         out.write('{:.17g}'.format(t));
#         for s in outputvars:
#             out.write(',')
#             out.write('{:.17g}'.format(res[s][i]))
#         out.write('\n')
