within GynCycle;
partial block Interface
  constant Integer SpeciesNum = 35;
  Modelica.Blocks.Interfaces.RealOutput species[SpeciesNum];
end Interface;
