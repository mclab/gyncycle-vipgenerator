within GynCycle;
function HPlus
  input Real s;
  input Real t;
  input Real n;
  output Real result;
algorithm
  result := (s / t) ^ n / (1.0 + (s / t) ^ n);
end HPlus;
