within GynCycle.Administrations;
package Utils
  function InjectionSequence
    import GynCycle.Administrations.*;
    input Integer startDay;
    input Integer stopDay;
    input Real dose;
    output Injections res;
  algorithm
    res.num := stopDay - startDay + 1;
    for i in 1:res.num loop
      res.arr[i].dose := dose;
      res.arr[i].timing := startDay + i - 1;
    end for;
  end InjectionSequence;
end Utils;
