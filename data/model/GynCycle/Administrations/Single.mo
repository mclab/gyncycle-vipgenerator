within GynCycle.Administrations;
block Single
  import RealOutput = Modelica.Blocks.Interfaces.RealOutput;
  constant Drug drug;
  InjectionsInput injections;
  RealOutput v;
equation
  der(v) = DerAdm(injections, drug, v, time);
end Single;
