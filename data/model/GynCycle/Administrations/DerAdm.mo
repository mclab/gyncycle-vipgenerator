within PAEON.GynCycle.Administrations;
function DerAdm
  input Injections injections;
  input Drug drug;
  input Real admSpecies;
  input Real t;
  output Real derAdministered;
algorithm
  derAdministered := 0.0;
  for i in 1:injections.num loop
    if t > injections.arr[i].timing then
      derAdministered := derAdministered + injections.arr[i].dose * drug.doseMultiplier * (drug.pk.beta ^ 2) * (t - injections.arr[i].timing) * exp(-drug.pk.beta * (t - injections.arr[i].timing));
    end if;
  end for;
  derAdministered := derAdministered - drug.pk.clearanceRate * admSpecies;
end DerAdm;
