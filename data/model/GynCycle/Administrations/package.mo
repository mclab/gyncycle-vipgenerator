within GynCycle;
package Administrations
  constant Integer MaxInjectionsNum = 100;
  record Injection "Drug injection"
    Real timing;
    Real dose;
  end Injection;
  record Injections
    Injection[MaxInjectionsNum] arr;
    Integer num;
  end Injections;
  record PK "Pharmacokinetic characteristics"
    Real beta;
    Real clearanceRate;
  end PK;
  record Drug "Drug"
    PK pk;
    Real doseMultiplier;
  end Drug;
  constant Injections NoInjections(arr = {Injection(timing= 0, dose = 0) for i in 1:MaxInjectionsNum}, num = 0);
  connector InjectionsInput = input Injections;
  connector InjectionsOutput = output Injections;
end Administrations;
