#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

# Try to find the MLPACK librairies
# MLPACK_FOUND - system has MLPACK lib
# MLPACK_INCLUDE_DIR - the MLPACK include directory
# MLPACK_LIBRARIES - Libraries needed to use MLPACK

if (MLPACK_INCLUDE_DIR AND MLPACK_LIBRARIES)
		# Already in cache, be silent
		set(MLPACK_FIND_QUIETLY TRUE)
endif (MLPACK_INCLUDE_DIR AND MLPACK_LIBRARIES)

find_path(MLPACK_INCLUDE_DIR NAMES mlpack/core.hpp)
find_library(MLPACK_LIBRARIES NAMES mlpack)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MLPACK DEFAULT_MSG MLPACK_LIBRARIES MLPACK_INCLUDE_DIR)

mark_as_advanced(MLPACK_INCLUDE_DIR MLPACK_LIBRARIES)
