#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_openmp target)
  find_package(OpenMP ${ARGN})
  if(${OpenMP_FOUND})
    target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:C>:${OpenMP_C_FLAGS}>)
    target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:${OpenMP_CXX_FLAGS}>)
    get_target_property(lang ${target} LINKER_LANGUAGE)
    if(${CMAKE_VERSION} VERSION_LESS "3.9.0")
      set_target_properties(${target} PROPERTIES LINK_FLAGS ${OpenMP_${lang}_FLAGS})
    else()
      target_link_libraries(${target} ${OpenMP_${lang}_LIBRARIES})
    endif()
  endif()
endmacro()
