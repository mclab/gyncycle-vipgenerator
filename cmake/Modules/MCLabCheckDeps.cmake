#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_check_deps)
message(STATUS "Checking MCLab dependencies...")
foreach(dep ${MCLAB_DEPS})
  if(NOT TARGET ${dep})
    message(FATAL_ERROR "MCLab library ${dep} is REQUIRED, try checking at https://bitbucket.org/mclab/${dep}")
  else()
    string(TOUPPER ${dep} dep_upcase)
    if (DEFINED MCLAB_REQUESTED_${dep_upcase}_VERSION)
      mclab_version_is_compatible("${MCLAB_${dep_upcase}_VERSION}" "${MCLAB_REQUESTED_${dep_upcase}_VERSION}" compatible)
      if(NOT ${compatible})
        message(FATAL_ERROR "Requested version (${MCLAB_REQUESTED_${dep_upcase}_VERSION}) of the MCLab library ${dep} is not compatible with the actual one (${MCLAB_${dep_upcase}_VERSION})")
      endif()
    endif()
    foreach(feature ${MCLAB_REQUESTED_${dep_upcase}_FEATURES})
      if(NOT (${feature} IN_LIST MCLAB_${dep_upcase}_FEATURES))
        message(FATAL_ERROR "Requested feature ${feature} for the MCLab library ${dep} is not available")
      endif()
    endforeach()
  endif(NOT TARGET ${dep})
endforeach(dep ${MCLAB_DEPS})
endmacro()
