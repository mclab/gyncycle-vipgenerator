#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

set(MCLAB_SUPPRESS_EXT_LIB_WARN ON CACHE BOOL "Suppress compiler warnings from the headers of external libraries")

macro(mclab_find_and_link_library target library)
  cmake_parse_arguments(MCLAB_FIND_AND_LINK_LIBRARY "" "PKGCONFIG" "" ${ARGN})
  if(${MCLAB_SUPPRESS_EXT_LIB_WARN})
    set(SYSTEM_FLAG SYSTEM)
  endif()
  if(MCLAB_FIND_AND_LINK_LIBRARY_PKGCONFIG)
    find_package(PkgConfig REQUIRED)
    if(MCLAB_FIND_AND_LINK_LIBRARY_UNPARSED_ARGUMENTS)
      pkg_search_module(${library} ${MCLAB_FIND_AND_LINK_LIBRARY_PKGCONFIG} ${MCLAB_FIND_AND_LINK_LIBRARY_UNPARSED_ARGUMENTS})
    else()
      pkg_search_module(${library} ${MCLAB_FIND_AND_LINK_LIBRARY_PKGCONFIG})
    endif()
    if(${${library}_FOUND})
      target_include_directories(${target} ${SYSTEM_FLAG} PRIVATE ${${library}_INCLUDE_DIRS})
      target_link_libraries(${target} ${${library}_LDFLAGS})
    endif()
  else()
    if(MCLAB_FIND_AND_LINK_LIBRARY_UNPARSED_ARGUMENTS)
      find_package(${library} ${MCLAB_FIND_AND_LINK_LIBRARY_UNPARSED_ARGUMENTS})
    else()
      find_package(${library})
    endif()
    if(${${library}_FOUND})
      if(${library}_INCLUDE_DIR)
        target_include_directories(${target} ${SYSTEM_FLAG} PRIVATE ${${library}_INCLUDE_DIR})
      elseif(${library}_INCLUDE_DIRS)
        target_include_directories(${target} ${SYSTEM_FLAG} PRIVATE ${${library}_INCLUDE_DIRS})
      endif(${library}_INCLUDE_DIR)
      target_link_libraries(${target} ${${library}_LIBRARIES})
    endif(${${library}_FOUND})
  endif()
endmacro()
