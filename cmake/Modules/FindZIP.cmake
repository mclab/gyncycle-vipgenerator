#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

# Searches for an installation of the zip library. On success, it sets the following variables:
#
#   ZIP_FOUND              Set to true to indicate the zip library was found
#   ZIP_INCLUDE_DIR       The directory containing the header file zip/zip.h
#   ZIP_LIBRARIES          The libraries needed to use the zip library
#

find_path(ZIP_INCLUDE_DIR NAMES zip.h)
find_library(ZIP_LIBRARIES NAMES zip)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ZIP DEFAULT_MSG ZIP_INCLUDE_DIR ZIP_LIBRARIES)

mark_as_advanced(ZIP_INCLUDE_DIR ZIP_LIBRARIES)
