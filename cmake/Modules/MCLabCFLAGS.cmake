#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

# Setup CFLAGS to reasonable defaults

set(MCLAB_WARNINGS ON CACHE BOOL "Enable compiler warnings")

macro(mclab_cflags target)
  target_compile_definitions(${target} PRIVATE "_GNU_SOURCE")

  set(MCLAB_CFLAGS_WARNINGS -fno-builtin -Wall -Wextra -Wfloat-equal -Wconversion -Wno-unused)
  if(${CMAKE_C_COMPILER_ID} STREQUAL "Intel")
    target_compile_options(${target} PRIVATE "$<$<COMPILE_LANGUAGE:C>:-std=c99>")
  else()
    set_property(TARGET ${target} PROPERTY C_STANDARD 99)
    STRING(CONCAT ${MCLAB_CFLAGS_WARNINGS} " --pedantic --pedantic-errors")
  endif()
  target_compile_options(${target} PRIVATE "$<$<OR:$<COMPILE_LANGUAGE:C>,$<COMPILE_LANGUAGE:CXX>>:$<$<BOOL:${MCLAB_WARNINGS}>:${MCLAB_CFLAGS_WARNINGS}>>")

  set(MCLAB_DEBUG_OPTIMIZATION_LEVEL "0" CACHE STRING "Debug optimization level")
  target_compile_options(${target} PRIVATE "$<$<CONFIG:DEBUG>:-O${MCLAB_DEBUG_OPTIMIZATION_LEVEL}>")

  if((${CMAKE_C_COMPILER_ID} STREQUAL "Clang") OR (${CMAKE_C_COMPILER_ID} STREQUAL "GNU"))
    set(MCLAB_SANITIZER "" CACHE STRING "Compiler sanitizer")
    if (NOT("${MCLAB_SANITIZER}" STREQUAL ""))
      set(MCLAB_CFLAGS_SANITIZE "-fsanitize=${MCLAB_SANITIZER}")
      target_compile_options(${target} PRIVATE "$<$<OR:$<COMPILE_LANGUAGE:C>,$<COMPILE_LANGUAGE:CXX>>:${MCLAB_CFLAGS_SANITIZE}>")
      target_link_libraries(${target} "${MCLAB_CFLAGS_SANITIZE}")
    endif()
  endif()

endmacro()
