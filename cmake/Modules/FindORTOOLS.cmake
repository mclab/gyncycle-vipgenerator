#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

if (ORTOOLS_INCLUDE_DIR AND ORTOOLS_LIBRARIES)
        # Already in cache, be silent
        set(ORTOOLS_FIND_QUIETLY TRUE)
endif (ORTOOLS_INCLUDE_DIR AND ORTOOLS_LIBRARIES)
find_path(ORTOOLS_INCLUDE_DIR NAMES ortools/base/basictypes.h)
find_library(ORTOOLS_LIBRARIES NAMES ortools)
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ORTOOLS DEFAULT_MSG ORTOOLS_LIBRARIES ORTOOLS_INCLUDE_DIR)
mark_as_advanced(ORTOOLS_INCLUDE_DIR ORTOOLS_LIBRARIES)
