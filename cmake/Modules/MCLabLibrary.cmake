#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_library library_name library_version)

set(CMAKE_MACOSX_RPATH 1)

project(${library_name} VERSION ${library_version})

string(TOUPPER ${library_name} library_name_upcase)
set(MCLAB_${library_name_upcase}_VERSION ${library_version} PARENT_SCOPE)

include(MCLabCommon)

file(GLOB HEADERS include/*.h)
file(GLOB SOURCES src/*.c)

add_library(${PROJECT_NAME} ${SOURCES} ${ARGN})

mclab_cflags(${PROJECT_NAME})

target_include_directories(${PROJECT_NAME} PRIVATE ${PROJECT_BINARY_DIR})
target_include_directories(${PROJECT_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/include)
target_include_directories(${PROJECT_NAME} PRIVATE ${PROJECT_SOURCE_DIR}/src)

install(TARGETS ${PROJECT_NAME} DESTINATION lib)
install(FILES ${HEADERS} DESTINATION include)

endmacro()

macro(mclab_library_feature feature)
  string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPERCASE)
  list(APPEND MCLAB_${PROJECT_NAME_UPPERCASE}_FEATURES ${feature})
  set(MCLAB_${PROJECT_NAME_UPPERCASE}_FEATURES ${MCLAB_${PROJECT_NAME_UPPERCASE}_FEATURES} PARENT_SCOPE)
endmacro()
