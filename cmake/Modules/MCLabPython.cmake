#    This file is part of MCLabCMakeModules
#    Copyright (C) 2020 Vadim Alimguzhin
#
#    MCLabCMakeModules is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    MCLabCMakeModules is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MCLabCMakeModules.
#    If not, see <https://www.gnu.org/licenses/>.

macro(mclab_python target version)
  if(${CMAKE_VERSION} VERSION_LESS "3.12.0")
    set(Python_ADDITIONAL_VERSIONS ${version})
    find_package(PythonLibs ${ARGN})
    target_include_directories(${target} PRIVATE ${PYTHON_INCLUDE_DIRS})
    target_link_libraries(${target} ${PYTHON_LIBRARIES})
  else()
    # improve getting major version and check that it is 2 or 3
    string(REGEX MATCHALL "[0-9]+" versions "${version}")
    list(GET versions 0 version_major)
    mclab_find_and_link_library(${target} "Python${version_major}" COMPONENTS Development ${ARGN})
  endif()
endmacro()
